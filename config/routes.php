<?php

$app->get('/hello', 'App\Controller\HelloController:sayHello');

$app->group('/user', function () use ($app) {
  $app->get('/send_email', 'App\Controller\UserController:sendEmail');
  $app->get('/', 'App\Controller\UserController:index');
  $app->get('/excel', 'App\Controller\UserController:exportExcel');
  $app->post('/', 'App\Controller\UserController:create');
  $app->post('/invite', 'App\Controller\UserController:invite');
  $app->post('/login', 'App\Controller\UserController:login');
  $app->put('/:userId', 'App\Controller\UserController:update');
  $app->get('/:userId', 'App\Controller\UserController:detail');
  $app->delete('/:userId', 'App\Controller\UserController:delete');
  $app->post('/activate/:userId', 'App\Controller\UserController:activate');
  $app->post('/check/:userId', 'App\Controller\UserController:check');
  $app->post('/reset', 'App\Controller\UserController:reset');
  $app->post('/invitor', 'App\Controller\UserController:invitor');
  $app->post('/invitor_share', 'App\Controller\UserController:invitor_share');
  $app->post('/logged', 'App\Controller\UserController:logged');
});

// $app->get('/share', 'App\Controller\ShareController:index');
$app->get('/share', 'App\Controller\ShareController:share');

$app->group('/family', function () use ($app) {
  $app->get('/:family_id', 'App\Controller\FamilyController:index');
  $app->post('/:family_id', 'App\Controller\FamilyController:create');
});

$app->group('/rt', function () use ($app) {
  $app->get('/', 'App\Controller\RtController:index');
  $app->post('/', 'App\Controller\RtController:create');
  $app->put('/:rtId', 'App\Controller\RtController:update');
  $app->get('/:rtId', 'App\Controller\RtController:detail');
  $app->post('/block/:rtId', 'App\Controller\RtController:block');
  $app->post('/reinvite/:rtId', 'App\Controller\RtController:reinvite');
});

$app->group('/finance', function () use ($app) {
  $app->get('/', 'App\Controller\FinanceController:index');
  $app->get('/start', 'App\Controller\FinanceController:startAmount');
  $app->get('/last', 'App\Controller\FinanceController:lastAmount');
  $app->post('/', 'App\Controller\FinanceController:create');
  $app->get('/:financeId', 'App\Controller\FinanceController:detail');
  $app->put('/:financeId', 'App\Controller\FinanceController:update');
  $app->delete('/:financeId', 'App\Controller\FinanceController:delete');
});


$app->group('/report', function () use ($app) {
  $app->get('/', 'App\Controller\ReportController:index');
  $app->post('/', 'App\Controller\ReportController:create');
  $app->get('/:reportId', 'App\Controller\ReportController:detail');
  $app->put('/:reportId', 'App\Controller\ReportController:update');
  $app->delete('/:reportId', 'App\Controller\ReportController:delete');
});

$app->group('/location', function () use ($app) {
  $app->get('/province', 'App\Controller\LocationController:province');
  $app->get('/province/:provinceId', 'App\Controller\LocationController:getProvince');
  $app->get('/province/:provinceId/city', 'App\Controller\LocationController:city');
  $app->get('/province/:provinceId/city/:cityId', 'App\Controller\LocationController:getCity');
  $app->get('/province/:provinceId/city/:cityId/district', 'App\Controller\LocationController:district');
  $app->get('/province/:provinceId/city/:cityId/district/:districtId', 'App\Controller\LocationController:getDistrict');
});

$app->group('/event', function () use ($app) {
  $app->get('/', 'App\Controller\EventController:index');
  $app->post('/', 'App\Controller\EventController:create');
  $app->get('/:reportId', 'App\Controller\EventController:detail');
  $app->put('/:reportId', 'App\Controller\EventController:update');
  $app->delete('/:reportId', 'App\Controller\EventController:delete');
});

$app->group('/information', function () use ($app) {
  $app->get('/', 'App\Controller\InformationController:index');
  $app->post('/', 'App\Controller\InformationController:create');
  $app->get('/:informationId', 'App\Controller\InformationController:detail');
  $app->delete('/:informationId', 'App\Controller\InformationController:delete');
});

$app->group('/document-type', function () use ($app) {
  $app->get('/', 'App\Controller\DocumentTypeController:index');
  $app->post('/', 'App\Controller\DocumentTypeController:create');
  $app->get('/:id', 'App\Controller\DocumentTypeController:detail');
  $app->put('/:id', 'App\Controller\DocumentTypeController:update');
  $app->delete('/:id', 'App\Controller\DocumentTypeController:delete');
});

$app->group('/document-request', function () use ($app) {
  $app->get('/', 'App\Controller\DocumentRequestController:index');
  $app->post('/', 'App\Controller\DocumentRequestController:create');
  $app->get('/:id', 'App\Controller\DocumentRequestController:detail');
  $app->put('/:id', 'App\Controller\DocumentRequestController:update');
  $app->put('/status/:id', 'App\Controller\DocumentRequestController:update');
  $app->delete('/:id', 'App\Controller\DocumentRequestController:delete');
});

$app->group('/alert', function () use ($app) {
  $app->get('/', 'App\Controller\AlertController:index');
  $app->post('/', 'App\Controller\AlertController:create');
  $app->get('/:alertId', 'App\Controller\AlertController:detail');
});

$app->group('/familytype', function () use ($app) {
  $app->get('/', 'App\Controller\FamilyController:familyType');
});

$app->group('/utilities', function () use ($app) {
  $app->get('/convert_rt_photo', 'App\Controller\UtilitiesController:convertPhoto');
  $app->get('/broadcast', 'App\Controller\UtilitiesController:broadcast');
});

$app->group('/application', function () use ($app) {
  $app->get('/build', 'App\Controller\AppController:detail');
  $app->put('/build', 'App\Controller\AppController:update');
});

$app->group('/activity', function() use ($app) {
  $app->get('/', 'App\Controller\ActivityController:index');
  $app->post('/', 'App\Controller\ActivityController:create');
  $app->get('/:id', 'App\Controller\ActivityController:detail');
  $app->delete('/:id', 'App\Controller\ActivityController:delete');
  $app->get('/:activityId/comments', 'App\Controller\ActivityController:indexComment');
  $app->post('/:activityId/comments', 'App\Controller\ActivityController:createComment');
  $app->delete('/:activityId/comments/:commentId', 'App\Controller\ActivityController:deleteComment');
  $app->put('/:activityId/comments/:commentId', 'App\Controller\ActivityController:updateComment');
});

$app->group('/notification', function() use ($app) {
  $app->get('/', 'App\Controller\NotificationController:index');
  $app->post('/', 'App\Controller\NotificationController:create');
  $app->get('/:id', 'App\Controller\NotificationController:detail');
  $app->delete('/:id', 'App\Controller\NotificationController:delete');
  $app->get('/:notificationId/comments', 'App\Controller\NotificationController:indexComment');
  $app->post('/:notificationId/comments', 'App\Controller\NotificationController:createComment');
  $app->delete('/:notificationId/comments/:commentId', 'App\Controller\NotificationController:deleteComment');
  $app->put('/:notificationId/comments/:commentId', 'App\Controller\NotificationController:updateComment');
});

$app->group('/product', function() use ($app) {
  $app->post('/', 'App\Controller\ProductController:create');
  $app->get('/:id', 'App\Controller\ProductController:detail');
  $app->put('/:id', 'App\Controller\ProductController:update');
  $app->delete('/:id', 'App\Controller\ProductController:delete');
  $app->get('/', 'App\Controller\ProductController:index');

  // Pak rt approval 
  $app->post('/:id/accept', 'App\Controller\ProductController:accept');
  $app->post('/:id/reject', 'App\Controller\ProductController:reject');

  // Product comments 
  // Create
  $app->post('/:productId/comment', 'App\Controller\ProductCommentController:create');
  // detail
  $app->get('/comment/:id', 'App\Controller\ProductCommentController:detail');
  // delete
  $app->delete('/comment/:id', 'App\Controller\ProductCommentController:delete');
  // list 
  $app->get('/:productId/comment', 'App\Controller\ProductCommentController:index');

  // Product image 
  $app->post('/:productId/image', 'App\Controller\ProductImageController:create');
  $app->delete('/image/:id', 'App\Controller\ProductImageController:delete');
  $app->put('/image/:id', 'App\Controller\ProductImageController:update');
});

$app->group('/chat', function() use ($app) {
  $app->post('/', 'App\Controller\ProductChatController:create');
  $app->get('/', 'App\Controller\ProductChatController:index');
  $app->get('/conversation/:productId', 'App\Controller\ProductChatController:conversation');
  $app->delete('/:id', 'App\Controller\ProductChatController:delete');
});