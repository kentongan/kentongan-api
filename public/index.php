<?php
date_default_timezone_set('Asia/Jakarta');

define('APP_PATH', dirname(__DIR__));

require APP_PATH.'/vendor/autoload.php';

$jwtSecret = 'skyshidevKentongan';

$app = require APP_PATH.'/bootstrap/app.php';

// wake up the elephant
$app->run();
