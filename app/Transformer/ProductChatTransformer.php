<?php 
namespace App\Transformer;

use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\NullResource;

class ProductChatTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'from',
    'to',
    // 'product'
  ];

  public function __construct($includes = []) {
    $this->app = \Slim\Slim::getInstance();
    $this->defaultIncludes = array_unique(array_merge($this->defaultIncludes, $includes));
  }

  public function transform($data)
  {
    $data['created'] = date('Y-m-d H:i', $data['created']);

    unset($data['from_pid']);
    unset($data['to_pid']);
    unset($data['product_id']);

    return $data;
  }

  public function includeFrom($data)
  {
    $user = $this->app->user->get($data['from_pid']);

    return $this->item($user, new UserTransformer(true));
  }

  public function includeTo($data)
  {
    $user = $this->app->user->get($data['to_pid']);

    return $this->item($user, new UserTransformer(true));
  }

  public function includeProduct($data)
  {
    $product = $this->app->product->get($data['product_id']);

    return $product ? $this->item($product, new ProductTransformer()) : null;
  }
}
