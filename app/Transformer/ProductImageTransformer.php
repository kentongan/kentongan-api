<?php 
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ProductImageTransformer extends TransformerAbstract {


  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data)
  {
    $req = $this->app->request;
    $base_url = $req->getUrl();
    $data['created'] = date('Y-m-d H:i', $data['created']);
    $data['image'] = $base_url . '/files/product_images/' . $data['file_name'];
    // $data['image_thumb'] = '/files/product_image_thumbs/' . $data['file_name'];

    unset($data['file_name']);

    return $data;
  }
}
