<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class NotificationTransformer extends TransformerAbstract {
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    $base_url = $this->app->request->getUrl();
    $image = (!empty($data['image'])) ? $base_url . "/" . $data['image'] : "" ;
    return array(
      "id" => $data['id'],
      "title" => $data['title'],
      "icon" => $base_url . "/files/icons/kentongan_icon.png",
      "content" => $data['content'],
      "type" => $data['type'],
      "comment_count" => $data['comment_count'],
      "idref" => $data['idref'],
      "image" => $image,
      "created" => date('Y-m-d H:i', $data['created']),
    );
  }
}