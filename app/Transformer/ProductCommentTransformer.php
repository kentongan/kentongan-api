<?php 
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ProductCommentTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'user',
  ];

  public function __construct($includes = []) {
    $this->app = \Slim\Slim::getInstance();
    // $this->setDefaultIncludes($includes);
    $this->defaultIncludes = array_unique(array_merge($includes, $this->defaultIncludes));
  }

  public function transform($data)
  {
    $data['created'] = date('Y-m-d H:i', $data['created']);

    unset($data['people_id']);
    unset($data['product_id']);

    return $data;
  }

  public function includeUser($data)
  {
    $user = $this->app->user->get($data['people_id']);

    return $this->item($user, new UserTransformer(true));
  }

  public function includeProduct($data)
  {
      $product = $this->app->product->get($data['product_id']);

      return $this->item($product, new ProductTransformer());
  }
}
