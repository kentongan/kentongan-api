<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class AppTransformer extends TransformerAbstract {
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      "android" => $data['android'],
      "ios" => $data['ios']
    );
  }
}