<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ReportTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    //'neighbourhood',
    'sender'
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    $base_url = $this->app->request->getUrl();
    $image = (empty($data['image']) ? "" : $base_url . "/" . $data['image']);
    $image_response = (empty($data['image_response']) ? "" : $base_url . "/" . $data['image_response']);
    return array(
      "rid" => $data['rid'],
      "description" => $data['description'],
      "created" => date('Y-m-d H:i', $data['created']),
      "response" => $data['response'],
      "image" => $image,
      "image_response" => $image_response,
 );
  }

  public function includeNeighbourhood($data) {
    $n = $this->app->rt->get($data['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer);
  }
  public function includeSender($data) {
    $n = $this->app->user->get($data['sender_pid']);
    return $this->item($n, new UserTransformer);
  }
}
