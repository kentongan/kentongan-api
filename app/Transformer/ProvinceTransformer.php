<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ProvinceTransformer extends TransformerAbstract {

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      'pvid' => $data['pvid'],
      'name' => $data['name']
    );
  }
}