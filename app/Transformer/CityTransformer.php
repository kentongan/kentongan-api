<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract {

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      'cid' => $data['cid'],
      'name' => $data['name']
    );
  }
}