<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class DocumentRequestTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'sender'
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      "drid" => $data['drid'],
      "dtid" => $data['dtid'],
      "description" => $data['description'],
      "required_date" => date('Y-m-d H:i', $data['required_date']),
      "status" => $data['status'],
      "response" => $data['response'],
      "created" => date('Y-m-d H:i', $data['created']),
      "updated" => date('Y-m-d H:i', $data['updated']),
    );
  }

  public function includeSender($data) {
    $n = $this->app->user->get($data['sender_pid']);
    return $this->item($n, new UserTransformer);
  }
}