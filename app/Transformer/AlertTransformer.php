<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class AlertTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    //'neighbourhood',
    'sender'
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      "aid" => $data['aid'],
      "type" => $data['type'],
      "created" => date('Y-m-d H:i', $data['created']),
      "response" => $data['response'],
      "latitude" => $data['latitude'],
      "longitude" => $data['longitude'],
    );
  }

  public function includeNeighbourhood($data) {
    $n = $this->app->rt->get($data['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer);
  }

  public function includeSender($data) {
    $n = $this->app->user->get($data['sender_pid']);
    return $this->item($n, new UserTransformer);
  }
}