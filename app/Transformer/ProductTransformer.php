<?php 
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'seller',
    // 'neighbourhood',
    'images'
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data)
  {
    $req = $this->app->request;
    $base_url = $req->getUrl();
    $data['created'] = date('Y-m-d H:i', $data['created']);
    $data['thumbnail'] = $base_url . '/files/product_image_thumbs/product-' . $data['id'] . '.jpg';

    unset($data['pid']);

    return $data;
  }

  public function includeSeller($data)
  {
    $user = $this->app->user->get($data['pid']);
    return $this->item($user, new UserTransformer(true));
  }

  public function includeImages($data)
  {
    $images = $this->app->productImage->getByProductId($data['id']);

    return $this->collection($images, new ProductImageTransformer());
  }

  public function includeNeighbourhood($user) {
    $n = $this->app->rt->get($user['nid']);
    return $this->item($n, new NeighbourhoodTransformer);
  }
}
