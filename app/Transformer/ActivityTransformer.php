<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ActivityTransformer extends TransformerAbstract {
  protected $defaultIncludes = [
    // 'neighbourhood'
    'user',
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    $base_url = $this->app->request->getUrl();
    $image = (!empty($data['image'])) ? $base_url . "/" . $data['image'] : "" ;
    $data['created'] = date('Y-m-d H:i', $data['created']);
    // $data['content'] = json_decode($data['content']);
    $data['content'] = $data['content'];
    $data['image'] = $image;

    return $data;
  }

  public function includeNeighbourhood($data) {
    $n = $this->app->rt->get($data['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer);
  }

  public function includeUser($data) {
    $user = $this->app->user->get($data['pid']);
    if ($user != null) {
      return $this->item($user, new UserTransformer);
    }
    return null;
  }
}
