<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class InformationTransformer extends TransformerAbstract {
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    $base_url = $this->app->request->getUrl();
    $image = (!empty($data['image'])) ? $base_url . "/" . $data['image'] : "" ;
    return array(
      "iid" => $data['iid'],
      "icon" => $base_url . "/files/icons/kentongan_icon.png",
      "institution" => $data['institution'],
      "information" => $data['information'],
      "url" => $data['url'],
      "image" => $image,
      "created" => date('Y-m-d H:i', $data['created']),
    );
  }
}