<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class NeighbourhoodTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'province',
    'district',
    'city'
  ];

  public function __construct( $public = false ) {
    $this->app = \Slim\Slim::getInstance();
    $this->public = $public;
  }

  public function transform($neighbourhood) {
    if($this->public){
      return array(
        'nid' => $neighbourhood['nid'],
        'rt' => $neighbourhood['rt'],
        'rw' => $neighbourhood['rw'],
        'village' => $neighbourhood['village'],
      );
    }
    return array(
      'nid' => $neighbourhood['nid'],
      'rt' => $neighbourhood['rt'],
      'rw' => $neighbourhood['rw'],
      'country' => $neighbourhood['country'],
      'village' => $neighbourhood['village'],
      //'validation_doc' => $neighbourhood['validation_doc'],
      'approval_photo' => $neighbourhood['approval_photo'],
      'created' => date('Y-m-d H:i', $neighbourhood['created']),
      'updated' => date('Y-m-d H:i', $neighbourhood['updated']),
    );
  }

  public function includeProvince($neighbourhood) {
    $n = $this->app->location->getLocation('provinces', $neighbourhood['province_id']);

    return $this->item($n, new ProvinceTransformer);
  }
  public function includeCity($neighbourhood) {
    $n = $this->app->location->getLocation('cities', $neighbourhood['city_id']);
    return $this->item($n, new CityTransformer);
  }
  public function includeDistrict($neighbourhood) {
    $n = $this->app->location->getLocation('districts', $neighbourhood['district_id']);
    return $this->item($n, new DistrictTransformer);
  }
}