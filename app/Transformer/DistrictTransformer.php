<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class DistrictTransformer extends TransformerAbstract {

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      'did' => $data['did'],
      'name' => $data['name']
    );
  }
}