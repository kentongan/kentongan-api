<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

  // protected $defaultIncludes = [
  //   'neighbourhood'
  // ];

  // @param public boolean
  // if public is true then show only minimum information about user 
  // eg, name, profile picture etc
  public function __construct($public = false) {
    $this->app = \Slim\Slim::getInstance();
    $this->isPublic = $public;

    // if (!$public) {
      $this->defaultIncludes = [
        'neighbourhood'
      ];
    // }
  }

  public function transform($user) {
    $userData = [
      'pid' => $user['pid'],
      'name' => $user['name'],
      'gender' => $user['gender'],
      'role' => $user['role'],
      'photo' => $user['photo'],
      'phone' => $user['phone'],
      'address' => $user['address'],
      'created' => date("Y-m-d H:i:s",$user['created']),
      'neighbourhood_id' => $user['neighbourhood_id']
    ];

    if (!$this->isPublic) {
      $userData = array_merge($userData, [
        'card_id' => $user['card_id'],
        'family_id' => $user['family_id'],
        'address' => $user['address'],
        'birth_date' => date('Y-m-d', $user['birth_date']),
        'birth_place' => $user['birth_place'],
        'phone' => $user['phone'],
        'email' => $user['email'],
        'password' => $user['password'],
        'education' => $user['education'],
        'family_type' => $user['family_type'],
        'status' => $user['status'],
        'created' => date('Y-m-d H:i', $user['created']),
        'updated' => date('Y-m-d H:i', $user['updated'])
      ]);
    }
    
    return $userData;
  }

  public function includeNeighbourhood($user) {
    $n = $this->app->rt->get($user['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer( $this->isPublic ));
  }
}