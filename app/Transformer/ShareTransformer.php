<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ShareTransformer extends TransformerAbstract {

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
      $this->defaultIncludes = [
        'neighbourhood'
      ];
  }

  public function transform($user) {
    $userData = [
      'pid' => $user['pid'],
      'name' => $user['name'],
      'gender' => $user['gender'],
      'role' => $user['role'],
      'photo' => $user['photo'],
      'phone' => $user['phone'],
      'address' => $user['address'],
      'image_share' => $user['image_share'],
      'created' => date("Y-m-d H:i:s",$user['created']),
      'neighbourhood_id' => $user['neighbourhood_id']
    ];
    
    return $userData;
  }

  public function includeNeighbourhood($user) {
    $n = $this->app->rt->get($user['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer());
  }
}