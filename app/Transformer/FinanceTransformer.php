<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class FinanceTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'neighbourhood'
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      'fid' => $data['fid'],
      'type' => $data['type'],
      'created' => date('Y-m-d H:i', $data['created']),
      'description' => $data['description'],
      'amount' => $data['amount'],
      'total_amount' => $data['total_amount'],
      'tag' => $data['tag'],
    );
  }

  public function includeNeighbourhood($data) {
    $n = $this->app->rt->get($data['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer);
  }
}