<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class DocumentTypeTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'neighbourhood'
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      "dtid" => $data['dtid'],
      "name" => $data['name'],
      "description" => $data['description'],
      "created" => date('Y-m-d H:i', $data['created']),
      "updated" => date('Y-m-d H:i', $data['updated']),
    );
  }

  public function includeNeighbourhood($data) {
    $n = $this->app->rt->get($data['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer);
  }
}