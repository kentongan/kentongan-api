<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class NotificationCommentTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    // 'neighbourhood'
    'user',
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data)
  {
    $base_url = $this->app->request->getUrl();
    $image = (!empty($data['image'])) ? $base_url . "/" . $data['image'] : "" ;
    $data['created'] = date('Y-m-d H:i', $data['created']);
    $data['image'] = $image;
    return $data;
  }

  public function includeUser($data)
  {
    $user = $this->app->user->get($data['people_id']);
    return $this->item($user, new UserTransformer( true ));
  }
}
