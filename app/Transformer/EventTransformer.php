<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class EventTransformer extends TransformerAbstract {

  protected $defaultIncludes = [
    'neighbourhood'
  ];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function transform($data) {
    return array(
      "eid" => $data['eid'],
      "name" => $data['name'],
      "location" => $data['location'],
      "start_date" => date('Y-m-d H:i', $data['start_date']),
      "end_date" => date('Y-m-d H:i', $data['end_date']),
      "created" => date('Y-m-d H:i', $data['created']),
      "type" => $data['type'],
      "description" => $data['description'],
    );
  }

  public function includeNeighbourhood($data) {
    $n = $this->app->rt->get($data['neighbourhood_id']);
    return $this->item($n, new NeighbourhoodTransformer);
  }
}