<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 1/13/16
 * Time: 14:55
 */

namespace App\Middleware;

use Flynsarmy\SlimMonolog\Log\MonologWriter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\SlackHandler;
use Slim\Middleware;
//use App\AmqpWrapper\WorkerSender;


class LogMiddleware extends Middleware {
  public function call() {
    //$sender = new WorkerSender();

    $streamHandler = new StreamHandler(APP_PATH . '/storage/logs/' . date('Y-m-d H') . '.log');
    $streamHandler->setFormatter(new LineFormatter(null, null, true, true));
    //$streamHandler->setLevel(Logger::ERROR);

    $channel = '#kentongan';

    /*$slackHandler = new SlackHandler('xoxp-2999186773-2999610299-18369773650-cb5c7f3413', $channel, 'LogApiBot');
    $slackHandler->setLevel(Logger::ERROR);*/

    // Register slack handler
    $handlers['handlers'][] = $streamHandler;
    //$handlers['handlers'][] = $slackHandler;
    $writer = new MonologWriter($handlers);

    $this->app->log->setWriter($writer);

    $this->next->call();
  }
}