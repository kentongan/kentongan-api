<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/27/15
 * Time: 13:19
 */

namespace App\Middleware;

use \App\Wrapper\Format;

class AccessMiddleware {
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $response = $this->response;
    $response->setForceStop(FALSE);
  }

  public static $publicEndpoint = [
    '/user/login' => 'POST',
    '/user/' => 'POST',
    '/user/invitor' => 'POST',
    '/rt' => 'POST',
    '/location/' => 'GET',
    '/location/*' => 'GET',
    '/location/.*' => 'GET',
    '/api/v1/docs' => 'GET',
    '/share' => 'GET',
  ];

  private function removeBearer() {
    $authHeader = $this->app->request->headers->get('Authorization');
    return preg_replace('/.*\s/', '', $authHeader);
  }

  private function isProtected() {
    $uri = trim($this->app->request()->getResourceUri(), '/');
    $requestType = $this->app->request->getMethod();
    foreach (self::$publicEndpoint as $path => $method) {
      $path = trim($path, '/');
      $path = str_replace('/', '\/', $path);
      if (preg_match('/^'. $path. '$/', $uri) && $requestType == $method) {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function call() {
    $loggedUser = $this->app->loggedUser;
    if ($this->isProtected()) {

    } else {
      $this->next->call();
    }
  }
}