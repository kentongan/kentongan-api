<?php
  namespace App\Middleware;
  use \Firebase\JWT\JWT;
  use \App\Wrapper\Format;

  class AuthMiddleware extends \Slim\Middleware {
    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->response = new Format();
      $response = $this->response;
      $response->setForceStop(FALSE);
    }
    
    public static $publicEndpoint = [
      '/user/login' => 'POST',
      '/user/' => 'POST',
      '/rt' => 'POST',
      '/location/.*' => 'GET',
      '/user/reset' => 'POST',
      '/user/invitor' => 'POST',
      'utilities/convert_rt_photo' => 'GET',
      'application/build' => 'GET',
      '/hello' => 'GET',
      '/share' => 'GET',
    ];

    private function removeBearer() {
      $authHeader = $this->app->request->headers->get('Authorization');
      return preg_replace('/.*\s/', '', $authHeader);
    }

    private function isProtected() {
      $uri = trim($this->app->request()->getResourceUri(), '/');
      $requestType = $this->app->request->getMethod();
      foreach (self::$publicEndpoint as $path => $method) {
        $path = trim($path, '/');
        $path = str_replace('/', '\/', $path);
        if (preg_match('/^'. $path. '$/', $uri) && $requestType == $method) {
          return FALSE;
        }
      }
      return TRUE;
    }

    public function call() {
      $response = $this->response;
      if ($this->isProtected()) {
        // get token and decode it
        $authHeader = $this->removeBearer();
        $authHeaderDecoded = '';
        if($authHeader) {
          try {
            $authHeaderDecoded = JWT::decode($authHeader, $this->app->config('app.key'), array('HS256'));
          } catch (\Exception $e) {
            $response->formatJson(406, $e->getMessage());
          }
        } else {
          $response->formatJson(406, 'Token Needed');
        }

        if ($authHeaderDecoded) {
          $user = $this->app->user->get($authHeaderDecoded->pid);
          //updated user update time if logged
          $this->app->user->update($authHeaderDecoded->pid, array('updated' => time()));

          $this->app->loggedUser = $user;
          // this line is required for the application to proceed
          $this->next->call();

        }
      } else {
        $this->next->call();
      }
    }
  }