<?php
namespace App\Fractal;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;

/**
 * Fractal trait
 *
 * Contain transformer method to implement in base controller
 *
 * @author Wayan Jimmy <jimmy@skyshi.com>
 */

trait FractalTrait
{

  /**
   * Get fractal manager
   */
  public function getManager()
  {
    $manager = new Manager();
    $manager->setSerializer(new ArraySerializer());
    return $manager;
  }

  /**
   * Fractal function get item
   * return transformed array
   * @param  array $data
   * @param  object $transformer
   * @return array
   */
  public function getItem($data = null, $transformer)
  {
    $resource = new Item($data, $transformer);
    return $this->getManager()->createData($resource)->toArray();
  }

  /**
   * Fractal function get collection
   * return transformed array
   * @param  array $data
   * @param  object $transformer
   * @return array
   */
  public function getCollection($data = null, $transformer)
  {
    $resource = new Collection($data, $transformer);
    $collection = $this->getManager()->createData($resource)->toArray();
    return $collection['data'];
  }
}