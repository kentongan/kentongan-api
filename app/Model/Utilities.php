<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 15:43
 */

namespace App\Model;
use Intervention\Image\ImageManagerStatic as Image;

class Utilities {
	public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
  }

  public function convertPhoto() {
  	$config = array(
		  'mode' => 'live',
		  'db' => 'api.kentongan'
		);

		if (file_exists(APP_PATH.'/bootstrap/config.php')) {
		  $config = require APP_PATH.'/bootstrap/config.php';
		}
  	if (!file_exists($this->app->config('app.approval_files'))) {
      @mkdir($this->app->config('app.approval_files'), 0777, true);
    }
    $images = array();
  	$rt = $this->db->get('neighbourhoods', null, array('nid', 'validation_doc', 'approval_photo'));
  	//$mysqli = new mysqli ('localhost', 'kentongan', 'kentongan123%', $config['db']);
  	foreach ($rt as $key => $value) {
  		$photo = $value['validation_doc'];

  		$data = explode(',', $photo);
  		if(count($data) <= 1) continue;
  		//print_r($data);

  		$imgData = base64_decode($data[1]);

      $f = finfo_open();
      $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
      $tmp = explode('/', $mimeType);
      $ext = $tmp[1];
      if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

      $name = uniqid('img-'.time().'-'). '.'. $ext;
      $image = $this->app->config('app.approval_files'). '/'. $name;

      $img = Image::make($data[1]);
      /*$img->resize(263, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $img->crop(263, 263);*/
      $img->save($image);
      $this->db->where ('nid', $value['nid']);
      $rtData = array( 
      	'approval_photo' => $image,
      );
      $images[] = $image;
			$updated = $this->db->update ('neighbourhoods', $rtData);
  	}
		print_r($images);
		exit();
  }
}