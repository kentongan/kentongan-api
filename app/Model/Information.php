<?php
namespace App\Model;
use Intervention\Image\ImageManagerStatic as Image;
use App\Wrapper\Misc;

class Information {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->informationAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->informationAllowedField);
  }

  private function uploadPhoto($userData) {
      $image = FALSE;
      if (!file_exists($this->app->config('app.files'))) {
        @mkdir($this->app->config('app.files'), 0777, true);
      }
      if(isset($_FILES['photo_img'])) {
        $files = $_FILES['photo_img'];
        $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('img-'.time().'-'). '.'. $ext;
        if (move_uploaded_file($files['tmp_name'], $this->app->config('app.files'). '/'. $name) === true) {
          $image = $this->app->config('app.files'). '/'. $name;
          $img = Image::make($image);
          $img->resize(263, null, function ($constraint) {
            $constraint->aspectRatio();
          });
          $img->crop(263, 263);
          $img->save($image);
        }
      }
      if (isset($userData['image']) && $userData['image']) {
        if (strpos($userData['image'], 'data:image') !== FALSE) {
          $data = explode(',', $userData['image']);
          $userData['image'] = $data[1];
        }
        $imgData = base64_decode($userData['image']);

        $f = finfo_open();
        $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
        $tmp = explode('/', $mimeType);
        $ext = $tmp[1];
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('info-'.time().'-'). '.'. $ext;
        $image = $this->app->config('app.files'). '/'. $name;

        $img = Image::make($userData['image']);
        $img->resize(263, null, function ($constraint) {
          $constraint->aspectRatio();
        });
        $img->crop(263, 263);
        $img->save($image);

      }
      return $image;
    }

  public $fields = array(
    "iid" => array('type' => 'number'),
    "institution" => array('type' => 'string', 'format' => 'required'),
    "information" => array('type' => 'string'),
    "url" => array('type' => 'string'),
    "image" => array('type' => 'string'),
  );

  public function total($params = []) {
    unset($params['q']);
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }

      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(information) LIKE ? OR LOWER(institution) LIKE ?)", array('%'.$query. '%', '%'.$query.'%'));
      }

      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    $this->db->orderBy($order, $order_type);
    $this->db->get('informations');
    $count = $this->db->count;

    return $count;
  }

  public function index($params = []) {
    $limit = 10;
    $page = 0;
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }

      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(information) LIKE ? OR LOWER(institution) LIKE ?)", array('%'.$query. '%', '%'.$query.'%'));
      }

      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }
      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
    if (!in_array($order, $this->informationAllowedField)) $order = 'iid';
    $this->db->orderBy($order, $order_type);

    $start = $page * $limit;
    $pager = NULL;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $informations = $this->db->get("informations", $pager);
    return $informations;
  }

  public function get($id) {
    $this->db->where('iid', $id);
    $information = $this->db->getOne("informations");
    return $information;
  }

  public function create($informationData) {
    $image = $this->uploadPhoto($informationData);
    if($image){
      $informationData['image'] = $image;
    }
    $informationData = $this->appMisc->sanitizeDbData($informationData);
    $id = FALSE;
    if(count($informationData)) {
      $informationData['created'] = time();
      $id = $this->db->insert('informations', $informationData);
    }
    return $id;
  }

  public function delete($id) {
    $this->db->where('iid', $id);
    if($this->db->delete('informations')) {
      return TRUE;
    }
    return FALSE;
  }
}
