<?php
  namespace App\Model;
  use Intervention\Image\ImageManagerStatic as Image;
  use App\Wrapper\Misc;

  class User {
    protected $app;
    protected $db;
    protected $userAllowedField;

    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->db = $this->app->mysql;
      $this->appMisc = new Misc();
      $this->userAllowedField = array_keys($this->fields);
      $this->appMisc->setAllowedDbField($this->userAllowedField);
    }

    public $fields = array(
      'email' => array(
        'type' => "email",
        //'format' => "required"
      ),
      'password' => array(
        'type' => "string",
        //'format' => "required"
      ),
      'name' => array(
        'type' => "string",
        //'format' => "required"
      ),
      'pid'  => array(
        'type' => "number",
      ),
      'card_id'  => array(
        // 'type' => "number",
      ),
      'family_id'  => array(
        // 'type' => "number",
      ),
      'address'  => array(
        'type' => "string",
      ),
      'birth_date'  => array(
        'type' => "date",
      ),
      'birth_place'  => array(
        'type' => "string",
      ),
      'gender'  => array(
        'type' => "string",
      ),
      'phone'  => array(
        'type' => "string",
      ),
      'education'  => array(
        'type' => "string",
      ),
      'role'  => array(
        'type' => "string",
      ),
      'photo'  => array(
        'type' => "string",
      ),
      'neighbourhood_id'  => array(
        'type' => "number",
      ),
      'device_token'  => array(
        'type' => "number",
      ),
      'status'  => array(
        'type' => "number",
      ),
      'family_type'  => array(
        'type' => "string",
      ),
      'updated'  => array(
        'type' => "string",
      ),
      'people_reff'  => array(
        'type' => "string",
      ),
    );

    private function uploadPhoto($userData) {
      $image = FALSE;
      if (!file_exists($this->app->config('app.files'))) {
        @mkdir($this->app->config('app.files'), 0777, true);
      }
      if(isset($_FILES['photo_img'])) {

        $files = $_FILES['photo_img'];
        $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('img-'.time().'-'). '.'. $ext;
        if (move_uploaded_file($files['tmp_name'], $this->app->config('app.files'). '/'. $name) === true) {
          $image = $this->app->config('app.files'). '/'. $name;
          $img = Image::make($image);
          $img->resize(263, null, function ($constraint) {
            $constraint->aspectRatio();
          });
          $img->crop(263, 263);
          $img->save($image);
        }
      }
      if (isset($userData['photo']) && $userData['photo']) {
        if (strpos($userData['photo'], 'data:image') !== FALSE) {
          $data = explode(',', $userData['photo']);
          $userData['photo'] = $data[1];
        }
        $imgData = base64_decode($userData['photo']);

        $f = finfo_open();
        $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
        $tmp = explode('/', $mimeType);
        $ext = $tmp[1];
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('img-'.time().'-'). '.'. $ext;
        $image = $this->app->config('app.files'). '/'. $name;

        $img = Image::make($userData['photo']);
        $img->resize(263, null, function ($constraint) {
          $constraint->aspectRatio();
        });
        $img->crop(263, 263);
        $img->save($image);

      }
      return $image;
    }

    public function getFamilyId(){
      $random = substr(md5(microtime()),0,2) . rand(1,10) . substr(md5(microtime()),-2);
      $this->db->where('family_id',$random);
      $this->db->where('family_type','KK');
      $users = $this->db->get('peoples');
      $count = $this->db->count;
      if($count > 0){
        $this->getFamilyId();
      }
      else {
        return $random;
      }
    }

    public function total($params = []) {
      unset($params['q']);
      if(isset($params) && count($params) > 0) {
        if (isset($params['query']) && $params['query']) {
          $query = strtolower($params['query']);
          $this->db->where ("(LOWER(name) LIKE ? OR LOWER(email) LIKE ? OR birth_place = ? OR phone = ?)", array('%'.$query.'%', '%'.$query.'%', $query, $query));
        }

        $params = $this->appMisc->sanitizeFilterParams($params);

        foreach($params as $param => $value) {
          $this->db->where($param, $value);
        }
      }
      $users = $this->db->get('peoples');
      $count = $this->db->count;

      return $count;
    }

    public function index($params = []) {
      $limit = 10;
      $page = 0;
      $order = 'pid';
      $order_type = 'ASC';
      $cols = array ("pid", "name", "gender","role","photo","phone","address",
        "p.created","neighbourhood_id","card_id","family_id","birth_date","birth_place",
        "email","password","education","family_type","status","p.updated");
      $this->db->join("neighbourhoods n", "p.neighbourhood_id=n.nid", "LEFT");
      if(isset($params) && count($params) > 0) {

        if (isset($params['limit'])) {
          $limit = $params['limit'];
        }

        if (isset($params['page'])) {
          $page = $params['page'];
        }

        if (isset($params['query']) && $params['query']) {
          $query = strtolower($params['query']);
          if($query=='kk'){
            $this->db->where ("family_type",$query);
          } else {
            $this->db->where ("(LOWER(name) LIKE ? OR LOWER(email) LIKE ? OR birth_place = ? OR phone = ? OR n.village LIKE ? OR address LIKE ?)", array('%'.$query. '%', '%'.$query.'%', $query, $query, '%'.$query. '%', '%'.$query. '%'));
          }
        }

        if(isset($params['order']) && $params['order']) {
          $tmp = explode(':', $params['order']);
          $order = $tmp[0];
          if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
        }

        $params = $this->appMisc->sanitizeFilterParams($params);

        foreach($params as $param => $value) {
          $this->db->where('p.'. $param, $value);
        }
        //$users = $this->db->get('peoples');
      }

      if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
      foreach($this->app->rt->rtAllowedField as $field) {
        $this->userAllowedField[] = 'n.'. $field;
      }
      if (!in_array($order, $this->userAllowedField)) $order = 'pid';
      $this->db->orderBy($order, $order_type);
      //$users = $this->db->arraybuilder()->paginate("peoples", $page);
      $start = $page * $limit;
      $pager = NULL;
      if ($limit) {
        $pager = array($start, $limit);
      }
      $users = $this->db->get("peoples p", $pager,$cols);
      foreach($users as &$user) {
        $user['role'] = strtolower($user['role']);
      }
      return $users;
    }

    public function login($username, $password) {
      $username = strtolower($username);
      if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
        if(substr($username, 0, 1) == "0"){
          $username = "+62" . substr($username, 1);
        }
      }
      $this->db->where ("(email = ? OR phone = ?)", array($username, $username));
      $this->db->where ("password", MD5($password));
      $user = $this->db->getOne ("peoples");

      return $user;
    }

    public function get($userId) {
      if (!$userId) return FALSE;
      if (filter_var($userId, FILTER_VALIDATE_EMAIL)) {
        $this->db->where('email', $userId);
      } else {
        if (substr($userId, 0, 3) == "+62") {
          $this->db->where('phone', $userId);
        } else if(substr($userId, 0, 1) == "0"){
          $userId = "+62" . substr($userId, 1);
          $this->db->where('phone', $userId);
        } else {
          $this->db->where('pid', $userId);
        }
      }

      $user = $this->db->getOne("peoples");
      if($user['photo']) {
        $req = $this->app->request;
        $base_url = $req->getUrl()."/";
        $user['photo'] = $base_url. $user['photo'];
      }
      if ($user) {
        $user['role'] = strtolower($user['role']);
      }
      return $user;
    }

    public function getByPidAndNid($pid,$nid){
      $this->db->where('neighbourhood_id',$nid);
      $this->db->where('pid',$pid);
      $data = $this->db->getOne("peoples");
      return $data;
    }

    public function create($userData) {
      $image = $this->uploadPhoto($userData);
      if ($image) $userData['photo'] = $image;

      //$this->appMisc->setAllowedDbField($this->userAllowedField);
      $userData = $this->appMisc->sanitizeDbData($userData);
      if(isset($userData['password']) && $userData['password']) {
        $userData['password'] = MD5($userData['password']);
      }

      if(!isset($userData['role']) || empty($userData['role'])) {
        $userData['role'] = 'rt';
      } else {
        $userData['role'] = strtolower($userData['role']);
      }
      if($userData['role'] == 'rt') $userData['status'] = 1;
      if(!isset($userData['family_type']) || empty($userData['family_type'])) {
        $userData['family_type'] = 'KK';
        $userData['family_id'] = $this->getFamilyId();
      }

      if(isset($userData['birth_date']) && !empty($userData['birth_date'])) {
        $userData['birth_date'] = strtotime($userData['birth_date']);
      }

      $id = FALSE;
      if($userData) {
        if (isset($userData['email']) && $userData['email']) {
          $userData['email'] = strtolower($userData['email']);
        }

        $userData['created'] = time();
        $userData['updated'] = time();
        $id = $this->db->insert("peoples", $userData);
      }
      return $id;
    }

    public function update($userId, $userData) {
      $image = $this->uploadPhoto($userData);
      if($this->app->config('app.mode') != 'demo') {
        unset($userData['neighbourhood_id']);
      }
      //unset($userData['status']);
      unset($userData['created']);
      if ($image) {
        $userData['photo'] = $image;
      } else {
        unset($userData['photo']);
      }
      //$this->appMisc->setAllowedDbField($this->userAllowedField);
      $userData = $this->appMisc->sanitizeDbData($userData);

      if(isset($userData['password']) && $userData['password']) {
        $userData['password'] = MD5($userData['password']);
      }

      if(isset($userData['birth_date']) && !empty($userData['birth_date'])) {
        $userData['birth_date'] = strtotime($userData['birth_date']);
      }

      $updated = FALSE;
      $this->db->where ('pid', $userId);
      //$userData = array_filter($userData);
      if($userData) {
        if (isset($userData['email']) && $userData['email']) {
          $userData['email'] = strtolower($userData['email']);
        }
        $userData['updated'] = time();
        $updated = $this->db->update ('peoples', $userData);
      }

      return $updated;
    }

    public function activate($userId) {
      $userData = array('status' => 1);
      $updated = FALSE;
      $this->db->where ('pid', $userId);
      if($userData) {
        $userData['updated'] = time();
        $updated = $this->db->update ('peoples', $userData);
      }

      return $updated;
    }

    public function reset($userId, $password) {
      $userData = array();
      $updated = FALSE;
      if ($password) {
        $userData['password'] = md5($password);
        $this->db->where ('pid', $userId);
        $updated = $this->db->update ('peoples', $userData);
      }

      return $updated;
    }

    public function delete($userId) {
      $this->db->where('pid', $userId);
      if($this->db->delete('peoples')) return TRUE;
      return FALSE;
    }

    public function insertInvitor($dataInvitor){
      if( $this->db->insert("invitors",$dataInvitor) ) return TRUE;
      return FALSE;
    }

    public function findPeopleByNeighborhood($neighbourhoodsId) {
      $this->db->where('neighbourhood_id', $neighbourhoodsId);
      
      return $this->db->get('peoples');
    }
  }
