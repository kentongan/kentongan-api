<?php

namespace App\Model;
use Intervention\Image\ImageManagerStatic as Image;
use App\Wrapper\Misc;

class Activity {
  protected $app;
  protected $db;

  public static $type = ['people', 'finance', 'report', 'danger', 'event', 'status'];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->userAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->userAllowedField);
  }

  public $fields = [
    'title' => [
      'type'=> 'string',
    ],
    'content' => [
      'type'=> 'string',
      'format' => 'required'
    ],
    'type' => [
      'type' => 'string',
      'format' => 'required'
    ],
    "neighbourhood_id" => [
      'type' => 'number',
      'format' => 'required'
    ],
    'pid' => [
      'type' => 'number'
    ],
    'image' => [
      'type' => 'string'
    ]
  ];

    private function uploadPhoto($userData) {
      $image = FALSE;
      if (!file_exists($this->app->config('app.files'))) {
        @mkdir($this->app->config('app.files'), 0777, true);
      }
      if(isset($_FILES['photo_img'])) {
        $files = $_FILES['photo_img'];
        $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('img-'.time().'-'). '.'. $ext;
        if (move_uploaded_file($files['tmp_name'], $this->app->config('app.files'). '/'. $name) === true) {
          $image = $this->app->config('app.files'). '/'. $name;
          $img = Image::make($image);
          $img->resize(263, null, function ($constraint) {
            $constraint->aspectRatio();
          });
          $img->crop(263, 263);
          $img->save($image);
        }
      }
      if (isset($userData['image']) && $userData['image']) {
        if (strpos($userData['image'], 'data:image') !== FALSE) {
          $data = explode(',', $userData['image']);
          $userData['image'] = $data[1];
        }
        $imgData = base64_decode($userData['image']);

        $f = finfo_open();
        $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
        $tmp = explode('/', $mimeType);
        $ext = $tmp[1];
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('act-'.time().'-'). '.'. $ext;
        $image = $this->app->config('app.files'). '/'. $name;

        $img = Image::make($userData['image']);
        $img->resize(263, null, function ($constraint) {
          $constraint->aspectRatio();
        });
        $img->crop(263, 263);
        $img->save($image);

      }
      return $image;
    }

  public function total($nid) {
      $this->db->where('neighbourhood_id',$nid);
      $activities = $this->db->get('activities');
      $count = $this->db->count;

      return $count;
    }

  public function getByNeighbourhoodId($neightbourhoodId, $page = 1){
    $this->db->where('neighbourhood_id', $neightbourhoodId);
    $this->db->orderBy('created', 'desc');
    $this->db->pageLimit = 10;

    $datas = $this->db->arraybuilder()->paginate('activities', $page);
    $data = array();

    foreach($datas as $key => $value){
      if( $this->isJson($value['content'])){
        $parse = json_decode($value['content']);
        if( !is_array($parse) ){
          $data[$key]=$value;
        }
        else {
          $value["content"] = $this->convertJSON($value['content'],$value['type'],$value['idref']);
          $data[$key] = $value;
        }
      }
      else {
        $value["content"] = $this->convertJSON($value['content'],$value['type'],$value['idref']);
        $data[$key] = $value;
      }
      
    }
    return $data;
  }

  public function convertJSON($json,$type,$idref = null){
    $jsonResult = "";
    $build = array();
    switch($type){
      case "people":
        $peoples = $this->app->user->get($idref);
        $build = (object) array(
          'name' => $peoples['name'],
          'address' => $peoples['address'],
          'role' => $peoples['role']
        );
        break;
      case "alert":
        $alerts = $this->app->alert->get($idref);
        $alertType = "";
        switch($alerts['type']){
          case 1:
            $alertType = "Kebakaran";
            break;
          case 2:
            $alertType = "Pencurian/Perampokan";
            break;
          case 3:
            $alertType = "Bencana Alam";
            break;
          case 4:
            $alertType = "Kecelakaan";
            break;
        }
        $build = (object) array(
          'latitude' => $alerts['latitude'],
          'longitude' => $alerts['longitude'],
          'type' => $alertType,
        );
        break;
      case "event":
        $events = $this->app->event->get($idref);
        $build = (object) array(
          'start_date' => $events['start_date'],
          'end_date' => $events['end_date'],
          'location' => $events['location'],
          'type' => $events['type'],
          'description' => $events['description'],
        );
        break;
      case "finance":
        $finances = $this->app->finance->get($idref);
        $build = (object) array(
          'type' => $finances['type'],
          'description' => $finances['description'],
          'amount' => $finances['amount'],
          'total_amount' => $finances['total_amount'],
          'tag' => $finances['tag'],
        );
        break;
      case "report":
        $reports = $this->app->report->get($idref);
        $build = (object) array(
          'description' => $reports['description'],
          'response' => $reports['response'],
        );
        break;
      case "status":
        $build = (object) array(
          'text' => $json,
        );
        break;
    }
    $jsonResult = json_encode($build);
    return $jsonResult;
  }

  public function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }

  public function getActivityComments($activityId){
    $this->db->where('activity_id', $activityId);

    return $this->db->get('activity_comments');
  }

  public function create($activityData){
    //This function remove pid
    $image = $this->uploadPhoto($activityData);
    if($image){
      $activityData['image'] = $image;
    }
    $pid = $activityData['pid'];
    $activityData = $this->appMisc->sanitizeDbData($activityData);
    $activityData['pid'] = $pid;

    $id = FALSE;
    if(count($activityData)){
      $activityData['created'] = time();
      $id = $this->db->insert('activities', $activityData);
    }

    return $id;
  }

  public function get($id){
    $this->db->where('id', $id);

    return $this->db->getOne('activities');
  }

  public function delete($id) {
    $this->db->where('id', $id);
    if ($this->db->delete('activities')) {return true;}
    return false;
  }

  public function getActivityByRefAndType($ref,$type){
    $this->db->where('idref', $ref);
    $this->db->where('type', $type);
    $this->db->orderBy('id', 'DESC');
    return $this->db->getOne('activities');
  }
}
