<?php

namespace App\Model;

use Intervention\Image\ImageManagerStatic as Image;
use App\Wrapper\Misc;

class ActivityComment {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->userAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->userAllowedField);
  }

  public $fields = [
    'content' => [
      'type' => 'string',
      'format' => 'required'
    ],
    'activity_id' => ['type' => 'number'],
    'people_id' => ['type' => 'number'],
    'image' => ['type' => 'string'],
  ];

  private function uploadPhoto($userData) {
    $image = FALSE;
    if (!file_exists($this->app->config('app.files'))) {
      @mkdir($this->app->config('app.files'), 0777, true);
    }
    if(isset($_FILES['photo_img'])) {
      $files = $_FILES['photo_img'];
      $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
      if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

      $name = uniqid('img-'.time().'-'). '.'. $ext;
      if (move_uploaded_file($files['tmp_name'], $this->app->config('app.files'). '/'. $name) === true) {
        $image = $this->app->config('app.files'). '/'. $name;
        $img = Image::make($image);
        $img->resize(263, null, function ($constraint) {
          $constraint->aspectRatio();
        });
        $img->crop(263, 263);
        $img->save($image);
      }
    }
    if (isset($userData['image']) && $userData['image']) {
      if (strpos($userData['image'], 'data:image') !== FALSE) {
        $data = explode(',', $userData['image']);
        $userData['image'] = $data[1];
      }
      $imgData = base64_decode($userData['image']);

      $f = finfo_open();
      $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
      $tmp = explode('/', $mimeType);
      $ext = $tmp[1];
      if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

      $name = uniqid('cact-'.time().'-'). '.'. $ext;
      $image = $this->app->config('app.files'). '/'. $name;

      $img = Image::make($userData['image']);
      $img->resize(263, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $img->crop(263, 263);
      $img->save($image);

    }
    return $image;
  }

  public function create($data) {
    $image = $this->uploadPhoto($data);
    if($image){
      $data['image'] = $image;
    }
    $data = $this->appMisc->sanitizeDbData($data);

    $id = false;
    if (count($data)){
      $data['created'] = time();
      $id = $this->db->insert('activity_comments', $data);
    }

    return $id;
  }

  public function get($id){
    $this->db->where('id', $id);
    return $this->db->getOne('activity_comments');
  }

  public function findByActivity($activityId) {
    $this->db->where('activity_id', $activityId);
    $this->db->orderBy('created', 'asc');

    return $this->db->get('activity_comments');
  }

  public function delete($id) {
    $this->db->where('id', $id);
    if ($this->db->delete('activity_comments')) {return true;}
    return false;
  }

}
