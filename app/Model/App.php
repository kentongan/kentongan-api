<?php
  namespace App\Model;
  class App {
  	public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->db = $this->app->mysql;
    }

    public function get() {
    	$data = $this->db->getOne('app_build_version');
    	return $data;
    }

    public function update($data) {
    	$data = array_filter($data);
    	$allowed = array("android", "ios");
    	$data = array_intersect_key($data, array_flip($allowed));

    	$updated = $this->db->update('app_build_version', $data);
    	return $updated;
    }
  }