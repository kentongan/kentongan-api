<?php

namespace App\Model;

use App\Wrapper\Misc;
use Intervention\Image\ImageManagerStatic as Image;

class ProductImage
{
    protected $app;
    protected $db;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->db = $this->app->mysql;

        $this->appMisc = new Misc();
        $this->allowedField = array_keys($this->fields);
        $this->appMisc->setAllowedDbField($this->allowedField);
    }

    public $fields = [
        'id' => ['type' => 'number'],
        'created' => ['type' => 'number'],
        'file_name' => ['type' => 'string'],
        'product_id' => ['type' => 'number'],
        'image' => ['type' => 'string', 'format' => 'required'],
    ];

    // Make image from base64
    public function makeImage($base64Image)
    {
        $data = explode(',', $base64Image);

        $imgData = base64_decode($data[1]);

        $f = finfo_open();
        $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
        $tmp = explode('/', $mimeType);
        $ext = $tmp[1];
        if (!in_array($ext, $this->app->config('app.imageExt'))) {
            return false;
        }
        $image = Image::make($data[1]);

        return $image;
    }

    // same as create but multiple
    public function createImages($productId, $images = [])
    {
        $imageInserts = [];
        $imagesObjects = [];
        foreach ($images as $image) {
            $img = $this->makeImage($image);
            if (!$img) {
                continue;
            }

            $imageName = uniqid('product-' . time() . '-') . '.' . explode('/', $img->mime())[1];
            $img->save(APP_PATH . '/public/files/product_images/' . $imageName);

            $imagesObjects[] = $img;
            $imageInserts[] = [
                'created' => time(),
                'product_id' => $productId,
                'file_name' => $imageName,
            ];
        }

        // Save to database
        // insertMulti not available on dev server what?
        // $ids = $this->db->insertMulti('product_images', $imageInserts);
        foreach ($imageInserts as $img) {
            $this->db->insert('product_images', $img);
        }

        // Make thumbnail
        $this->saveThumb($imagesObjects, $productId);
    }

    private function saveThumb($images, $productId)
    {
        $width = 640;
        $height = 420;

        $thumb = Image::canvas($width, $height);

        if (count($images) >= 3) {
            if ($images[0]->height() >= $images[0]->width()) {
                // resize the images
                $images[0]->fit($width / 2, $height);
                $images[1]->fit($width / 2, $height / 2);
                $images[2]->fit($width / 2, $height / 2);

                $thumb->insert($images[0]);
                $thumb->line($width / 2, 0, $width / 2, $height, function ($draw) {
                    $draw->color('#ffffff');
                    $draw->width(20);
                });
                $thumb->insert($images[1], null, $width / 2, 0);
                $thumb->line($width / 2, $height / 2, $width, $height / 2, function ($draw) {
                    $draw->color('#ffffff');
                    $draw->width(20);
                });
                $thumb->insert($images[2], null, $width / 2, $height / 2);
            } else {
                $images[0]->fit($width, $height / 2);
                $images[1]->fit($width / 2, $height / 2);
                $images[2]->fit($width / 2, $height / 2);

                $thumb->insert($images[0]);
                $thumb->line(0, $height / 2, $width, $height / 2, function ($draw) {
                    $draw->color('#ffffff');
                    $draw->width(20);
                });
                $thumb->insert($images[1], null, 0, $height / 2);
                $thumb->line($width / 2, $height / 2, $width / 2, $height, function ($draw) {
                    $draw->color('#ffffff');
                    $draw->width(20);
                });
                $thumb->insert($images[2], null, $width / 2, $height / 2);
            }
        } elseif (!empty($images)) {
            $thumb = $images[0]->fit($width, $height);
        } else {
            return false;
        }

        $thumb->encode('jpg')->save(APP_PATH . '/public/files/product_image_thumbs/' . 'product-' . $productId . '.jpg');
    }

    public function create($productId, $imageData)
    {
        $imageData = $this->appMisc->sanitizeDbData($imageData);

        $imageData['created'] = time();
        $imageData['product_id'] = $productId;

        $image = $this->makeImage($imageData['image']);
        $imageName = uniqid('product-' . time() . '-') . '.' . explode('/', $image->mime())[1];

        if (!$image->save(APP_PATH . '/public/files/product_images/' . $imageName)) {
            return false;
        }

        $imageData['file_name'] = $imageName;
        unset($imageData['image']);

        $id = $this->db->insert('product_images', $imageData);

        return $id;
    }

    public function get($id)
    {
        $this->db->where('id', $id);

        return $this->db->getOne('product_images');
    }

    public function update($id, $imageData)
    {
        unset($imageData['product_id']);

        // remove previous image
        $image = $this->get($id);
        unlink(APP_PATH . '/public/files/product_images/' . $image['file_name']);

        $image = $this->makeImage($imageData['image']);
        $imageName = uniqid('product-' . time() . '-') . '.' . explode('/', $image->mime())[1];

        if (!$image->save(APP_PATH . '/public/files/product_images/' . $imageName)) {
            return false;
        }

        $imageData['file_name'] = $imageName;
        unset($imageData['image']);

        $updated = false;

        if (!empty($imageData)) {
            $this->db->where('id', $id);
            $updated = $this->db->update('product_images', $imageData);
        }

        return $updated;
    }

    public function getByProductId($productId)
    {
        $this->db->where('product_id', $productId);

        return $this->db->get('product_images');
    }

    public function delete($id)
    {
        // remove previous image
        $image = $this->get($id);
        unlink(APP_PATH . '/public/files/product_images/' . $image['file_name']);

        $this->db->where('id', $id);

        return $this->db->delete('product_images') ? true : false;
    }
}
