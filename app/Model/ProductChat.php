<?php

namespace App\Model;

use App\Wrapper\Misc;

class ProductChat
{
    protected $app;
    protected $db;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->db = $this->app->mysql;

        $this->appMisc = new Misc();
        $this->allowedField = array_keys($this->fields);
        $this->appMisc->setAllowedDbField($this->allowedField);
    }

    public $fields = [
        'id' => ['type' => 'number'],
        'from_pid' => ['type' => 'number'],
        'to_pid' => ['type' => 'number', 'format' => 'required'],
        'product_id' => ['type' => 'number', 'format' => 'required'],
        'chat' => ['type' => 'string', 'format' => 'required'],
        'created' => ['type' => 'number'],
    ];

    public function create($chatData)
    {
        $chatData = $this->appMisc->sanitizeDbData($chatData);

        $id = false;
        if (count($chatData)) {
            $chatData['created'] = time();
            $id = $this->db->insert('product_chats', $chatData);
        }

        return $id;
    }

    // Get chat for each product (only display one latest chat)
    public function getConversation($pid, $params = [])
    {
        $limit = isset($params['limit']) ? $params['limit'] : 10;
        $page = isset($params['page']) ? $params['page'] + 1 : 1;
        $offset = $limit * ($page - 1);

        // Why limit is required (only on mariadb)
        // https://mariadb.com/kb/en/mariadb/why-is-order-by-in-a-from-subquery-ignored/
        $chats = $this->db->rawQuery(
            'SELECT sub.* FROM'
            . ' (SELECT pc.* FROM `product_chats` as pc'
            . ' WHERE pc.from_pid=? OR pc.to_pid=?'
            . ' ORDER BY pc.created DESC LIMIT 18446744073709551615) as sub'
            . ' GROUP BY sub.product_id ORDER BY sub.created DESC LIMIT ?,?', [$pid, $pid, $offset, $limit]);

        $count = $this->db->rawQuery(
            'SELECT COUNT(*) AS count from'
            . ' (SELECT * FROM product_chats'
            . ' WHERE from_pid=? OR to_pid=?'
            . ' GROUP BY product_id) AS sub', [$pid, $pid]);

        return [
            'data' => $chats,
            'totalCount' => $count[0]['count']
        ];
    }

    public function getConversationByProductId($user1, $user2, $productId, $params = [])
    {
        $limit = isset($params['limit']) ? $params['limit'] : 10;
        $page = isset($params['page']) ? $params['page'] + 1 : 1;
        $offset = $limit * ($page - 1);

        $chats = $this->db->rawQuery(
            'SELECT * FROM product_chats'
            . ' WHERE (from_pid=? OR from_pid=?)'
            . ' AND (to_pid=? OR to_pid=?)'
            . ' ORDER BY created DESC'
            . ' LIMIT ?,?',
            [$user1, $user2, $user1, $user2, $offset, $limit]
        );

        $count = $this->db->rawQuery(
            'SELECT COUNT(*) as count FROM product_chats'
            . ' WHERE (from_pid=? OR from_pid=?)'
            . ' AND (to_pid=? OR to_pid=?)'
            . ' ORDER BY created DESC',
            [$user1, $user2, $user1, $user2]
        );

        return [
            'data' => $chats,
            'totalCount' => $count[0]['count']
        ];
    }

    public function get($id)
    {
        $this->db->where('id', $id);

        return $this->db->getOne('product_chats');
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return $this->db->delete('product_chats') ? true : false;
    }
}
