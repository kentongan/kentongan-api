<?php

namespace App\Model;

use App\Wrapper\Misc;

class Product
{

    const STATUS_ACTIVE = 0;
    const STATUS_SOLD = 1;

    const APPROVAL_WAITING = 0;
    const APPROVAL_ACCEPTED = 1;
    const APPROVAL_REJECTED = 2;

    protected $app;
    protected $db;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->db = $this->app->mysql;

        $this->appMisc = new Misc();
        $this->allowedField = array_keys($this->fields);
        $this->appMisc->setAllowedDbField($this->allowedField);
    }

    public $fields = [
        'id' => ['type' => 'number'],
        'name' => ['type' => 'string', 'format' => 'required'],
        'description' => ['type' => 'string'],
        'view' => ['type' => 'number'],
        'price' => ['type' => 'number', 'format' => 'required'],
        'created' => ['type' => 'string'],
        'approval' => ['type' => 'number'],
        'status' => ['type' => 'number'],
        'pid' => ['type' => 'number'],
        'nid' => ['type' => 'number'],
    ];

    public function create($productData)
    {
        $pid = $productData['pid'];
        $nid = $productData['nid'];

        $productData = $this->appMisc->sanitizeDbData($productData);
        $productData['pid'] = $pid;
        $productData['nid'] = $nid;

        $id = false;
        if (count($productData)) {
            if (!isset($productData['approval'])) {
                $productData['approval'] = self::APPROVAL_WAITING;
            }

            $productData['status'] = self::STATUS_ACTIVE;
            $productData['created'] = time();
            $id = $this->db->insert('products', $productData);
        }

        return $id;
    }

    public function get($productId)
    {
        $this->db->where('id', $productId);

        return $this->db->getOne('products');
    }

    public function update($productId, $productData)
    {
        unset($productData['pid']);
        unset($productData['nid']);
        unset($productData['id']);

        $productData = $this->appMisc->sanitizeDbData($productData);
        $updated = false;

        if (count($productData)) {
            $this->db->where('id', $productId);
            $productData['updated'] = time();
            $updated = $this->db->update('products', $productData);
        }

        return $updated;
    }

    public function delete($productId)
    {
        $this->db->where('id', $productId);

        return $this->db->delete('products') ? true : false;
    }

    public function index($params = [])
    {
        $app_mode = $this->app->config('app.mode');
        $limit = isset($params['limit']) ? $params['limit'] : 10;
        $page = isset($params['page']) ? $params['page'] : 0;

        $loggedUser = $this->app->loggedUser;
        $fields = array(
            "id","name","description","view","price","products.created",
            "approval","status","products.nid","rt","rw","village","pid","products.updated"
        );
        // Get Match Village, District And City From neighborhood data
        $neighborhood = $this->app->rt->get($loggedUser['neighbourhood_id']);
        $this->db->join('neighbourhoods','neighbourhoods.nid=products.nid','LEFT');
        if (isset($params['nid'])) {
            $this->db->where('products.nid', $params['nid']);
        } 

        if (isset($params['pid'])) {
            $this->db->where('products.pid', $params['pid']);
        }

        if(!isset($params['nid']) && !isset($params['pid'])) {
            if($app_mode == "demo"){
                $this->db->where('products.nid', $loggedUser['neighbourhood_id']);
            } else {
                $this->db->where('neighbourhoods.province_id',$neighborhood['province_id']);
                $this->db->where('neighbourhoods.city_id',$neighborhood['city_id']);
                $this->db->where('neighbourhoods.district_id',$neighborhood['district_id']);
                // $this->db->where('neighbourhoods.village',$neighborhood['village']);
                $this->db->where("products.nid != '". $loggedUser['neighbourhood_id']."'");
            }
        }

        // Search Engine
        if (isset($params['query']) && $params['query']) {
          $query = strtolower($params['query']);
          $replace = str_replace(" ", "+", $query);
          $phrases = explode("+",$replace);
          $where = "";
          $arr_where = array();
          foreach($phrases as $phrase){
            $phrase = strtolower($phrase);
            $where .= "(name like ? OR description like ? OR price like ?) AND ";
            $arr_where = array_merge($arr_where,array('%'.$phrase.'%','%'.$phrase.'%','%'.$phrase.'%')); 
          }
          $where = substr($where,0,strlen($where)-5);
          $this->db->where($where,$arr_where);
        }

        if (isset($params['approval']) && is_array($params['approval']) && !empty($params['approval'])) {
            $this->db->where('products.approval', $params['approval'], 'IN');
        } else {
            $this->db->where('products.approval', self::APPROVAL_ACCEPTED);
        }

        if (isset($params['status']) && is_array($params['status']) && !empty($params['status'])) {
            $this->db->where('products.status', $params['status'], 'IN');
        } else {
            $this->db->where('products.status', self::STATUS_ACTIVE);
        }

        $this->db->orderBy('products.created', 'desc');
        $this->db->pageLimit = $limit;

        $products = $this->db->arraybuilder()->paginate('products', $page + 1, $fields);

        return [
            'data' => $products,
            'totalCount' => $this->db->totalCount,
        ];
    }
}
