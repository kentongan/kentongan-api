<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 10:28
 */

namespace App\Model;
use App\Wrapper\Misc;

class DocumentType {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->documentAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->documentAllowedField);
  }

  public $fields = array(
    "dtid" => array('type' => 'number'),
    "name" => array('type' => 'string', 'format' => 'required'),
    "updated" => array('type' => 'date'),
    "created" => array('type' => 'date'),
    "description" => array('type' => 'string'),
    "neighbourhood_id" => array('type' => 'number'),
  );

  public function total($params = []) {
    unset($params['q']);
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }

      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    $this->db->orderBy($order, $order_type);
    $this->db->get('document_types');
    $count = $this->db->count;

    return $count;
  }

  public function index($params = []) {
    $limit = 10;
    $page = 0;
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }

      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }
      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
      //$users = $this->db->get('peoples');
    }
    if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
    if (!in_array($order, $this->eventAllowedField)) $order = 'dtid';
    $this->db->orderBy($order, $order_type);

    //if ($limit) $this->db->pageLimit = $limit;
    //$users = $this->db->arraybuilder()->paginate('document_types', $page);
    $start = $page * $limit;
    $pager = NULL;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $events = $this->db->get('document_types', $pager);
    return $events;
  }

  public function get($id) {
    $this->db->where('dtid', $id);

    $notification = $this->db->getOne('document_types');
    return $notification;
  }

  public function create($data) {
    $data = $this->appMisc->sanitizeDbData($data);

    $id = FALSE;
    if(count($data)) {
      $data['created'] = time();
      $data['updated'] = time();
      $id = $this->db->insert('document_types', $data);
    }
    return $id;
  }

  public function update($id, $data) {
    unset($data['neighbourhood_id']);
    unset($data['created']);
    unset($data['updated']);
    $data = $this->appMisc->sanitizeDbData($data);
    $updated = FALSE;
    $this->db->where ('dtid', $id);
    if($data) {
      $data['updated'] = time();
      $updated = $this->db->update ('document_types', $data);
    }

    return $updated;
  }

  public function delete($id) {
    $this->db->where('dtid', $id);
    if($this->db->delete('document_types')) return TRUE;
    return FALSE;
  }
}