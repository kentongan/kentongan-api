<?php

namespace App\Model;

use App\Wrapper\Misc;

class ProductComment
{
    protected $prefix = 'product_comment_';
    protected $app;
    protected $db;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->db = $this->app->mysql;

        $this->appMisc = new Misc();
        $this->allowedField = array_keys($this->fields);
        $this->appMisc->setAllowedDbField($this->allowedField);
    }

    public $fields = [
        'id' => ['type' => 'number'],
        'comment' => ['type' => 'string', 'format' => 'required'],
        'created' => ['type' => 'int'],
        'people_id' => ['type' => 'number'],
        'product_id' => ['type' => 'number'],
    ];

    public function create($commentData)
    {
        $commentData = $this->appMisc->sanitizeDbData($commentData);

        $id = false;
        if (count($commentData)) {
            $commentData['created'] = time();
            $id = $this->db->insert('product_comments', $commentData);
        }

        return $id;
    }

    public function get($id)
    {
        $this->db->where('id', $id);

        return $this->db->getOne('product_comments');
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return $this->db->delete('product_comments') ? true : false;
    }

    public function getCommentsByProductId($productId, $params = [])
    {
        $limit = isset($params['limit']) ? $params['limit'] : 10;
        $page = isset($params['page']) ? $params['page'] : 0;

        $this->db->where('product_id', $productId);
        $this->db->orderBy('created', 'DESC');
        $this->db->pageLimit = $limit;

        $comments = $this->db->arraybuilder()->paginate('product_comments', $page + 1);

        return [
            'data' => $comments,
            'totalCount' => $this->db->totalCount
        ];

    }

}
