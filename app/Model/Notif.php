<?php

namespace App\Model;
use Intervention\Image\ImageManagerStatic as Image;
use App\Wrapper\Misc;

class Notif {
  protected $app;
  protected $db;

  public static $type = ['ads', 'info', 'notif'];

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->userAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->userAllowedField);
  }

  public $fields = [
    'title' => [
      'type'=> 'string',
    ],
    'content' => [
      'type'=> 'string',
      'format' => 'required'
    ],
    'type' => [
      'type' => 'string',
      'format' => 'required'
    ],
    'idref' => [
      'type' => 'number',
    ],
    'image' => [
      'type' => 'string'
    ]
  ];

  private function uploadPhoto($userData) {
    $image = FALSE;
    if (!file_exists($this->app->config('app.files'))) {
      @mkdir($this->app->config('app.files'), 0777, true);
    }
    if(isset($_FILES['photo_img'])) {
      $files = $_FILES['photo_img'];
      $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
      if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

      $name = uniqid('img-'.time().'-'). '.'. $ext;
      if (move_uploaded_file($files['tmp_name'], $this->app->config('app.files'). '/'. $name) === true) {
        $image = $this->app->config('app.files'). '/'. $name;
        $img = Image::make($image);
        $img->resize(263, null, function ($constraint) {
          $constraint->aspectRatio();
        });
        $img->crop(263, 263);
        $img->save($image);
      }
    }
    if (isset($userData['image']) && $userData['image']) {
      if (strpos($userData['image'], 'data:image') !== FALSE) {
        $data = explode(',', $userData['image']);
        $userData['image'] = $data[1];
      }
      $imgData = base64_decode($userData['image']);

      $f = finfo_open();
      $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
      $tmp = explode('/', $mimeType);
      $ext = $tmp[1];
      if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

      $name = uniqid('not-'.time().'-'). '.'. $ext;
      $image = $this->app->config('app.files'). '/'. $name;

      $img = Image::make($userData['image']);
      $img->resize(263, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $img->crop(263, 263);
      $img->save($image);

    }
    return $image;
  }

  public function index($page = 1) {
    $this->db->orderBy('created', 'desc');
    $this->db->pageLimit = 10;
    $data = $this->db->arraybuilder()->paginate('notifications', $page);
    return $data;
  }

  public function total() {
    $notifications = $this->db->get('notifications');
    $count = $this->db->count;
    return $count;
  }

  public function convertJSON($json,$type,$idref = null){
    $jsonResult = "";
    $build = array();
    switch($type){
      case "ads":
        // For ads information
        break;
      case "info":
        $info = $this->app->information->get($idref);
        $build = (object) array(
          'institution' => $info['institution'],
          'text' => $info['information'],
          'url' => $info['url'],
          'image' => $info['image'],
        );
        break;
    }
    $jsonResult = json_encode($build);
    return $jsonResult;
  }

  public function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }

  public function getActivityComments($activityId){
    $this->db->where('notification_id', $activityId);
    return $this->db->get('notification_comments');
  }

  public function create($notificationData){
    $image = $this->uploadPhoto($notificationData);
    if($image){
      $notificationData['image'] = $image;
    }
    $notificationData = $this->appMisc->sanitizeDbData($notificationData);
    $id = FALSE;
    if(count($notificationData)){
      $notificationData['created'] = time();
      $id = $this->db->insert('notifications', $notificationData);
    }
    return $id;
  }

  public function get($id){
    $this->db->where('id', $id);
    return $this->db->getOne('notifications');
  }

  public function delete($id) {
    $this->db->where('id', $id);
    if ($this->db->delete('notifications')) {return true;}
    return false;
  }

  public function getNotificationByRefAndType($ref,$type){
    $this->db->where('idref', $ref);
    $this->db->where('type', $type);
    $this->db->orderBy('id', 'DESC');
    return $this->db->getOne('notifications');
  }
}
