<?php
namespace App\Model;
use App\Wrapper\Misc;
use Intervention\Image\ImageManagerStatic as Image;

class Report {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->reportAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->reportAllowedField);
  }

  public $fields = array(
    "rid" => array('type' => 'number'),
    "sender_pid" => array('type' => 'number'),
    "description" => array('type' => 'string', 'format' => 'required'),
    "created" => array('type' => 'number'),
    "response" => array('type' => 'string'),
    "neighbourhood_id" => array('type' => 'number'),
    "image" => array('type' => 'string'),
    "image_response" => array('type' => 'string'),
  );

  private function uploadPhoto($reportData, $field = "image") {
    $image = FALSE;
    if (!file_exists($this->app->config('app.files'))) {
      @mkdir($this->app->config('app.files'), 0777, true);
    }
    if(isset($_FILES['photo_img'])) {
      $files = $_FILES['photo_img'];
      $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
      if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

      $name = uniqid('img-'.time().'-'). '.'. $ext;
      if (move_uploaded_file($files['tmp_name'], $this->app->config('app.files'). '/'. $name) === true) {
        $image = $this->app->config('app.files'). '/'. $name;
        $img = Image::make($image);
        $img->resize(263, null, function ($constraint) {
          $constraint->aspectRatio();
        });
        $img->crop(263, 263);
        $img->save($image);
      }
    }
    if (isset($reportData[$field]) && $reportData[$field]) {
      if (strpos($reportData[$field], 'data:image') !== FALSE) {
        $data = explode(',', $reportData[$field]);
        $reportData[$field] = $data[1];
      }
      $imgData = base64_decode($reportData[$field]);

      $f = finfo_open();
      $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
      $tmp = explode('/', $mimeType);
      $ext = $tmp[1];
      if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

      $name = uniqid('report-'.time().'-'). '.'. $ext;
      $image = $this->app->config('app.files'). '/'. $name;

      $img = Image::make($reportData[$field]);
      $img->resize(263, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $img->crop(263, 263);
      $img->save($image);

    }
    return $image;
  }

  public function total($params = []) {
    unset($params['q']);
    if(isset($params) && count($params) > 0) {
      $params = $this->appMisc->sanitizeFilterParams($params);
      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("LOWER(description) = ?)", array('%'.$query.'%'));
      }

      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    $this->db->get('reports');
    $count = $this->db->count;

    return $count;
  }

  public function index($params = []) {
    $limit = 10;
    $page = 0;
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }

      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("LOWER(description) = ?)", array('%'.$query.'%'));
      }
      $params = $this->appMisc->sanitizeFilterParams($params);

      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
      //$users = $this->db->get('peoples');
    }

    if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
    if (!in_array($order, $this->reportAllowedField)) $order = 'rid';
    $this->db->orderBy($order, $order_type);
    //if ($limit) $this->db->pageLimit = $limit;

    $start = $page * $limit;
    $pager = NULL;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $report = $this->db->get("reports", $pager);
    //$report = $this->db->arraybuilder()->paginate("reports", $page);

    return $report;
  }

  public function get($reportId) {
    $this->db->where('rid', $reportId);

    $notification = $this->db->getOne("reports");
    return $notification;
  }

  public function create($reportData) {
    $image = $this->uploadPhoto($reportData, 'image');
    if($image){
      $reportData['image'] = $image;
    }
    $reportData = $this->appMisc->sanitizeDbData($reportData);

    $id = FALSE;
    if(count($reportData)) {
      $reportData['created'] = time();
      $id = $this->db->insert('reports', $reportData);
    }
    return $id;
  }

  public function update($reportId, $reportData) {
    $image = $this->uploadPhoto($reportData, 'image_response');
    if($image){
      $reportData['image_response'] = $image;
    }
    unset($reportData['neighbourhood_id']);
    unset($reportData['created']);
    unset($reportData['sender_pid']);
    unset($reportData['description']);
    $reportData = $this->appMisc->sanitizeDbData($reportData);
    $updated = FALSE;
    $this->db->where ('rid', $reportId);
    if($reportData) {
      $updated = $this->db->update ('reports', $reportData);
    }

    return $updated;
  }

  public function delete($reportId) {
    $this->db->where('rid', $reportId);
    if($this->db->delete('reports')) return TRUE;
    return FALSE;
  }
}