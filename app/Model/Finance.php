<?php
  namespace App\Model;
  use App\Wrapper\Misc;

  class Finance {
    protected $app;
    protected $db;

    public $fields = array(
      'fid' => array(
        'type' => "primarykey"
      ),
      'type' => array(
        'type' => "string",
        'format' => "maxlength:1,required"
      ),
      'created' => array(
        'type' => "date",
        //'format' => "required"
      ),
      'description' => array(
        'type' => "textarea",
        'format' => "required"
      ),
      'amount' => array(
        'type' => "number",
        'format' => "required,maxlength:11"
      ),
      'total_amount' => array(
        'type' => "number",
        'format' => "maxlength:11"
      ),
      'neighbourhood_id' => array(
        //'format' => "required",
        'type' => "number"
      ),
      'people_id' => array(
        //'format' => "required",
        'type' => "number"
      ),
      'tag' => array(
        'type' => "text",
        'format' => "required,maxlength:50"
      ),
    );

    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->db = $this->app->mysql;
      $this->appMisc = new Misc();
      $this->allowedField = array_keys($this->fields);
      $this->appMisc->setAllowedDbField($this->allowedField);
      //$this->getrtid();
    }

    /*private function getrtid(){
      $user = $this->app->loggedUser;
      $this->rtid = $user['neighbourhood_id'];
    }*/

    public function total($params = []) {
      unset($params['q']);
      $order = 'fid';
      $order_type = 'ASC';

      if(isset($params) && count($params) > 0) {

        $filterDateEnd = false;
        $filterDateStart = false;
        if (isset($params['month'])) {
          $dateObj   = \DateTime::createFromFormat('!m', $params['month']);
          $monthName = $dateObj->format('F');
          $filterDateEnd = strtotime('last day of '. $monthName)+86400;
          $filterDateStart = strtotime('first day of '. $monthName);
        }
        if (isset($params['year'])) {
          $filterDateEnd = strtotime($params['year']. '-12-31');
          $filterDateStart = strtotime($params['year']. '-01-01');
        }
        if (isset($params['month']) && isset($params['year'])){
          $dateObj   = \DateTime::createFromFormat('!m', $params['month']);
          $monthName = $dateObj->format('F');
          $filterDateEnd = strtotime('last day of '. $monthName. ' '. $params['year'])+86400;
          $filterDateStart = strtotime('first day of '. $monthName. ' '. $params['year']);
        }
        if($filterDateEnd || $filterDateStart) {
          $this->db->where ("created >= ? AND created <= ?", array($filterDateStart, $filterDateEnd));
        }
        if(isset($params['order']) && $params['order']) {
          $tmp = explode(':', $params['order']);
          $order = $tmp[0];
          if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
        }

        $params = $this->appMisc->sanitizeFilterParams($params);

        foreach($params as $param => $value) {
          $this->db->where($param, $value);
        }
      }
      $this->db->orderBy($order, $order_type);
      $this->db->get('finances');
      $count = $this->db->count;

      return $count;
    }

    public function startAmount( $params = array() ) {
      $page = 0;
      $limit = 1;
      $order = 'fid';
      $order_type = 'DESC';
      if(isset($params) && count($params) > 0) {
        $filterDateEnd = false;
        $filterDateStart = false;
        if (isset($params['month']) && isset($params['year'])){
          $dateObj   = \DateTime::createFromFormat('!m', $params['month']);
          $monthName = $dateObj->format('F');

          $filterDateEnd = strtotime('last day of '. $monthName. ' '. $params['year'])+86400;
          $filterDateStart = strtotime('first day of '. $monthName. ' '. $params['year']);
        }
        if($filterDateEnd || $filterDateStart) {
          $this->db->where ("created < ?", array($filterDateStart));
        }
        if(isset($params['order']) && $params['order']) {
          $tmp = explode(':', $params['order']);
          $order = $tmp[0];
          if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
        }

        $params = $this->appMisc->sanitizeFilterParams($params);
        foreach($params as $param => $value) {
          $this->db->where($param, $value);
        }
      }
      if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
      if (!in_array($order, $this->allowedField)) $order = 'fid';

      $this->db->orderBy($order, $order_type);

      $start = $page * $limit;
      $pager = NULL;
      if ($limit) {
        $pager = array($start, $limit);
      }
      $finances = $this->db->get("finances", $pager);
      return $finances;
    }

    public function lastAmount( $params = array() ) {
      $page = 0;
      $limit = 1;
      $order = 'fid';
      $order_type = 'DESC';
      if(isset($params) && count($params) > 0) {
        $filterDateEnd = false;
        $filterDateStart = false;
        if (isset($params['month']) && isset($params['year'])){
          $dateObj   = \DateTime::createFromFormat('!m', $params['month']);
          $monthName = $dateObj->format('F');

          $filterDateEnd = strtotime('last day of '. $monthName. ' '. $params['year'])+86400;
          $filterDateStart = strtotime('first day of '. $monthName. ' '. $params['year']);
        }
        if($filterDateEnd || $filterDateStart) {
          $this->db->where ("created < ?", array($filterDateEnd));
        }
        if(isset($params['order']) && $params['order']) {
          $tmp = explode(':', $params['order']);
          $order = $tmp[0];
          if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
        }

        $params = $this->appMisc->sanitizeFilterParams($params);
        foreach($params as $param => $value) {
          $this->db->where($param, $value);
        }
      }
      if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
      if (!in_array($order, $this->allowedField)) $order = 'fid';

      $this->db->orderBy($order, $order_type);

      $start = $page * $limit;
      $pager = NULL;
      if ($limit) {
        $pager = array($start, $limit);
      }
      $finances = $this->db->get("finances", $pager);
      return $finances;
    }

    public function index( $params = array() ) {
      $limit = 10;
      $page = 0;

      $order = 'fid';
      $order_type = 'ASC';
      if(isset($params) && count($params) > 0) {
        if (isset($params['limit'])) {
          $limit = $params['limit'];
        }
        if (isset($params['page'])) {
          $page = $params['page'];
        }

        $filterDateEnd = false;
        $filterDateStart = false;
        if (isset($params['month'])) {
          $dateObj   = \DateTime::createFromFormat('!m', $params['month']);
          $monthName = $dateObj->format('F');
          $filterDateEnd = strtotime('last day of '. $monthName)+86400;
          $filterDateStart = strtotime('first day of '. $monthName);
        }
        if (isset($params['year'])) {
          $filterDateEnd = strtotime($params['year']. '-12-31');
          $filterDateStart = strtotime($params['year']. '-01-01');
        }
        if (isset($params['month']) && isset($params['year'])){
          $dateObj   = \DateTime::createFromFormat('!m', $params['month']);
          $monthName = $dateObj->format('F');
          $filterDateEnd = strtotime('last day of '. $monthName. ' '. $params['year'])+86400;
          $filterDateStart = strtotime('first day of '. $monthName. ' '. $params['year']);
        }
        if($filterDateEnd || $filterDateStart) {
          $this->db->where ("created >= ? AND created <= ?", array($filterDateStart, $filterDateEnd));
        }
        if(isset($params['order']) && $params['order']) {
          $tmp = explode(':', $params['order']);
          $order = $tmp[0];
          if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
        }

        $params = $this->appMisc->sanitizeFilterParams($params);
        foreach($params as $param => $value) {
          $this->db->where($param, $value);
        }
      }
      if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
      if (!in_array($order, $this->allowedField)) $order = 'fid';

      $this->db->orderBy($order, $order_type);
      //if($limit) $this->db->pageLimit = $limit;

      $start = $page * $limit;
      $pager = NULL;
      if ($limit) {
        $pager = array($start, $limit);
      }
      $finances = $this->db->get("finances", $pager);
      //$finances = $this->db->arraybuilder()->paginate("finances", $page);
      return $finances;
    }

    public function get($fid){
      $this->db->where("fid",$fid);
      $financeData = $this->db->getOne("finances");
      return $financeData;
    }

    public function delete($fid){
      $this->db->where("fid",$fid);
      $financeData = $this->db->delete("finances");
      if(!$financeData){
        return FALSE;
      }
      return TRUE;
    }

    public function create($data) {
      $fields = $this->fields;
      unset($fields['total_amount']);
      $this->allowedField = array_keys($fields);
      $this->appMisc->setAllowedDbField($this->allowedField);
      $data = $this->appMisc->sanitizeDbData($data);
      $data['created'] = time();
      $id = false;
      if($data) {
        $id = $this->db->insert("finances", $data);
      }
      return $id;
    }

    public function update($fid, $data) {
      unset($data['created']);
      $fields = $this->fields;
      $this->allowedField = array_keys($fields);
      $this->appMisc->setAllowedDbField($this->allowedField);
      $data = $this->appMisc->sanitizeDbData($data);
      $updated = FALSE;
      $this->db->where('fid', $fid);
      if($data) {
        $updated = $this->db->update('finances', $data);
      }

      return $updated;
    }

    function updateLastAmount($fid, $financeData, $oldData, $mode){
      if($mode == "update"){
        if($financeData['type']=="I" && $oldData['type']=="O"){
          $query = "UPDATE finances SET total_amount = total_amount + ".$oldData['amount']." + ".$financeData['amount']." WHERE neighbourhood_id='".$financeData['neighbourhood_id']."' AND fid >= '".$fid."'";
        }
        elseif($financeData['type']=="O" && $oldData['type']=="I"){
          $query = "UPDATE finances SET total_amount = total_amount - ".$oldData['amount']." - ".$financeData['amount']." WHERE neighbourhood_id='".$financeData['neighbourhood_id']."' AND fid >= '".$fid."'";
        }
        elseif($financeData['type']=="O" && $oldData['type']=="O"){
          $query = "UPDATE finances SET total_amount = (total_amount + ".$oldData['amount'].") - ".$financeData['amount']." WHERE neighbourhood_id='".$financeData['neighbourhood_id']."' AND fid >= '".$fid."'";
        }
        elseif($financeData['type']=="I" && $oldData['type']=="I"){
          $query = "UPDATE finances SET total_amount = (total_amount - ".$oldData['amount'].") + ".$financeData['amount']." WHERE neighbourhood_id='".$financeData['neighbourhood_id']."' AND fid >= '".$fid."'";
        }
      } else {
        if($oldData['type']=="I"){
          $query = "UPDATE finances SET total_amount = total_amount - ".$oldData['amount']." WHERE neighbourhood_id='".$oldData['neighbourhood_id']."' AND fid >= '".$fid."'";
        } else {
          $query = "UPDATE finances SET total_amount = total_amount + ".$oldData['amount']." WHERE neighbourhood_id='".$oldData['neighbourhood_id']."' AND fid >= '".$fid."'";
        }
      }
      $this->db->query($query);
    }
  }