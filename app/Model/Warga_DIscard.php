<?php
  namespace App\Model;

  use PHPMailer;
  // use Twilio;

  class Warga {
    protected $app;
    protected $db;

    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->db = $this->app->mysql;
    }

    public function total($params = []) {
      unset($params['q']);
      $users = $this->db->get('peoples');
      $count = $this->db->count;

      return $count;
    }

    public function index( $params = array() ) {
      unset($params['q']);

      $limit = 10;
      $page = 1;
      if( count($params) > 0 ) {
        if (isset($params['limit'])) {
          $limit = $params['limit'];
        }
        if (isset($params['page'])) {
          $page = $params['page']+1;
        }
        
//        foreach($params as $param => $value) {
//          if($value != 'limit' && $value != 'page'){
//            $this->db->where($param, $value);
//          }
//        }
        
      }
      $users = $this->db->get('peoples');

      $this->db->pageLimit = $limit;
      $users = $this->db->arraybuilder()->paginate("peoples", $page);

      return $users;
    }

    public function delete( $peopleid = null ){
      $this->db->where('peopleid',$peopleid);
      if( $this->db->delete("peoples") ){
        return true;
      }
      return false;
    }

    public function update( $peopleid, $params ){
      $fields = array(
        'card_id',
        'family_id',
        'name',
        'address',
        'birth_date',
        'birth_place',
        'gender',
        'phone',
        'email',
        'password',
        'educationlevel',
        'role',
        'photo',
        'registertoken',
        'neighbourhood',
        'devicetoken',
        'familyrelationshiptype',
        'status'
      );
      $dataupdate = array();
      foreach($fields as $field){
        $dataupdate[$field] = @$params[$field];
      }
      $this->db->where('peopleid',$peopleid);
      if( $this->db->update("peoples") ){
        return true;
      }
      return false;

    }

    public function insertinvite($table,$data){
      if( $this->db->insert($table,$data) ){
        return true;
      }
      else {
        return false;
      }
    }

    public function get($nid){
      $this->db->where("neighbourhoodid",$nid);
      $datart = $this->db->get("neighbourhoods");
      return $datart[0];
    }
    
    public function sendemail($params){
        if(!isset($params)){
            return false;
        }
        else {
            $mail = new PHPMailer;
            $mail->isSMTP();
            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            $mail->Host = "smtp.gmail.com";
            //Set the SMTP port number - likely to be 25, 465 or 587
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "saddamrajif@gmail.com";
            $mail->Password = "mejikuhibiniu";
            $mail->setFrom($params['from'], 'Kentongan');
            //Set an alternative reply-to address
            // $mail->addReplyTo('replyto@example.com', 'First Last');
            //Set who the message is to be sent to
            $mail->addAddress($params['to']['email'], $params['to']['name']);
            //Set the subject line
            $mail->Subject = $params['subject'];
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $mail->msgHTML($params['message']);
            //Replace the plain text body with one created manually
            // $mail->AltBody = 'This is a plain-text message body';
            //Attach an image file
            // $mail->addAttachment('images/phpmailer_mini.png');
            if ( !$mail->send() ) {
                // $result['message'] = "Mailer Error: " . $mail->ErrorInfo;
                // $result['status'] = 406;
                return false;
            } else {
                // $result['message'] = "Message sent!";
                // $result['status'] = 200;
                return true;
            }
        }
        // return $result;
    }
}