<?php
namespace App\Model;
use App\Wrapper\Misc;

class Familytype {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    // $this->reportAllowedField = array_keys($this->fields);
    // $this->appMisc->setAllowedDbField($this->reportAllowedField);
  }

  public function total() {
    $this->db->get('familytypes');
    $count = $this->db->count;
    return $count;
  }

  public function index() {
    $familytype = $this->db->get("familytypes");
    return $familytype;
  }
}