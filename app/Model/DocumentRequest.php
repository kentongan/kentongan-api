<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 10:28
 */

namespace App\Model;
use App\Wrapper\Misc;

class DocumentRequest {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->documentAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->documentAllowedField);
  }

  public $fields = array(
    "drid" => array('type' => 'number'),
    "dtid" => array('type' => 'number', 'format' => 'required'),
    "sender_pid" => array('type' => 'number'),
    "description" => array('type' => 'string', 'format' => 'required'),
    "required_date" => array('type' => 'date', 'format' => 'required'),
    "status" => array('type' => 'number'),
    "response" => array('type' => 'string'),
    "created" => array('type' => 'date'),
    "updated" => array('type' => 'date'),
    "neighbourhood_id" => array('type' => 'number'),
  );

  public function total($params = []) {
    unset($params['q']);
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }

      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    $this->db->orderBy($order, $order_type);
    $this->db->get('document_request');
    $count = $this->db->count;

    return $count;
  }

  public function index($params = []) {
    $limit = 10;
    $page = 0;
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }

      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }
      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
      //$users = $this->db->get('peoples');
    }
    if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
    if (!in_array($order, $this->eventAllowedField)) $order = 'drid';
    $this->db->orderBy($order, $order_type);

    //if ($limit) $this->db->pageLimit = $limit;
    //$users = $this->db->arraybuilder()->paginate('document_request', $page);
    $start = $page * $limit;
    $pager = NULL;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $events = $this->db->get('document_request', $pager);
    return $events;
  }

  public function get($id) {
    $this->db->where('drid', $id);

    $notification = $this->db->getOne('document_request');
    return $notification;
  }

  public function create($data) {
    $data = $this->appMisc->sanitizeDbData($data);
    $id = FALSE;
    if(count($data)) {
      $data['created'] = time();
      $data['updated'] = time();
      $id = $this->db->insert('document_request', $data);
    }
    return $id;
  }

  public function update($id, $data) {
    unset($data['created']);
    unset($data['updated']);
    $data = $this->appMisc->sanitizeDbData($data);
    $updated = FALSE;
    $this->db->where ('drid', $id);
    if($data) {
      $data['updated'] = time();
      $updated = $this->db->update ('document_request', $data);
    }

    return $updated;
  }

  public function delete($id) {
    $this->db->where('drid', $id);
    if($this->db->delete('document_request')) return TRUE;
    return FALSE;
  }
}