<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 15:43
 */

namespace App\Model;
use App\Wrapper\Misc;

class Alert {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->allowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->allowedField);
  }

  public $fields = array(
    "aid" => array('type' => 'number', 'format' => 'uneditable'),
    "sender_pid" => array('type' => 'number'),
    "type" => array('type' => 'number', 'format' => 'required'),
    "created" => array('type' => 'number'),
    "response" => array('type' => 'string'),
    "latitude" => array('type' => 'number'),
    "longitude" => array('type' => 'number'),
    "neighbourhood_id" => array('type' => 'number'),
  );

  public function total($params = []) {
    unset($params['q']);
    if(isset($params) && count($params) > 0) {
      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    $this->db->get('alerts');
    $count = $this->db->count;

    return $count;
  }

  public function index($params = []) {
    $limit = 10;
    $page = 0;
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }

      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }

      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
    if (!in_array($order, $this->allowedField)) $order = 'aid';
    $this->db->orderBy($order, $order_type);
    //if ($limit) $this->db->pageLimit = $limit;
    //$users = $this->db->arraybuilder()->paginate('alerts', $page);

    $start = $page * $limit;
    $pager = NULL;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $alerts = $this->db->get("alerts", $pager);

    return $alerts;
  }

  public function get($alertId) {
    $this->db->where('aid', $alertId);

    $notification = $this->db->getOne('alerts');
    return $notification;
  }

  public function create($alertData) {
    $alertData = $this->appMisc->sanitizeDbData($alertData);

    $id = FALSE;
    if(count($alertData)) {
      $alertData['created'] = time();
      $id = $this->db->insert('alerts', $alertData);
    }
    return $id;
  }
}