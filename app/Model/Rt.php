<?php
namespace App\Model;
use App\Wrapper\Misc;
use Intervention\Image\ImageManagerStatic as Image;

class Rt {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->rtAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->rtAllowedField);
  }

  public $fields = array(
    "nid" => array('type' => 'number'),
    "rt" => array('type' => 'number', 'format' => 'required'),
    "rw" => array('type' => 'number', 'format' => 'required'),
    "village" => array('type' => 'string', 'format' => 'required'),
    "district_id" => array('type' => 'number', 'format' => 'required'),
    "city_id" => array('type' => 'number', 'format' => 'required'),
    "province_id" => array('type' => 'number', 'format' => 'required'),
    "country" => array('type' => 'string', 'format' => 'required'),
    "created" => array('type' => 'string'),
    "updated" => array('type' => 'string'),
    "province" => array('type' => 'string'),
    "city" => array('type' => 'string'),
    "district" => array('type' => 'string'),
    "city_tmp" => array('type' => 'string'),
    "district_tmp" => array('type' => 'string'),
    "approval_photo" => array('type' => 'string')
  );

  private function uploadPhoto($userData) {
      $image = FALSE;
      if (!file_exists($this->app->config('app.files'))) {
        @mkdir($this->app->config('app.files'), 0777, true);
      }
      if(isset($_FILES['photo_img'])) {

        $files = $_FILES['photo_img'];
        $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('img-'.time().'-'). '.'. $ext;
        if (move_uploaded_file($files['tmp_name'], $this->app->config('app.files'). '/'. $name) === true) {
          $image = $this->app->config('app.files'). '/'. $name;
          $img = Image::make($image);
          /*$img->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
          });
          $img->crop(500, 500);*/
          $img->save($image);
        }
      }
      if (isset($userData['approval_photo']) && $userData['approval_photo']) {
        if (strpos($userData['approval_photo'], 'data:image') !== FALSE) {
          $data = explode(',', $userData['approval_photo']);
          $userData['approval_photo'] = $data[1];
        }
        $imgData = base64_decode($userData['approval_photo']);

        $f = finfo_open();
        $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
        $tmp = explode('/', $mimeType);
        $ext = $tmp[1];
        if (!in_array($ext, $this->app->config('app.imageExt'))) return FALSE;

        $name = uniqid('img-'.time().'-'). '.'. $ext;
        $image = $this->app->config('app.files'). '/'. $name;

        $img = Image::make($userData['approval_photo']);
        /*$img->resize(263, null, function ($constraint) {
          $constraint->aspectRatio();
        });
        $img->crop(263, 263);*/
        $img->save($image);

      }
      return $image;
    }

  public function create($rtData) {
    unset($rtData['province']);
    unset($rtData['city']);
    unset($rtData['district']);

    $image = $this->uploadPhoto($rtData);
    if ($image) $rtData['approval_photo'] = $image;

    $rtData = $this->appMisc->sanitizeDbData($rtData);

    $cityTmp = "";
    $districtTmp = "";
    if(isset($rtData['city_tmp'])){
      $cityTmp = $rtData['city_tmp'];
      unset($rtData['city_tmp']);
    }
    
    if(isset($rtData['district_tmp'])){
      $districtTmp = $rtData['district_tmp'];
      unset($rtData['district_tmp']);
    }

    $id = FALSE;
    if(count($rtData)) {
      $rtData['created'] = time();
      $rtData['updated'] = time();
      $id = $this->db->insert('neighbourhoods', $rtData);
    }

    if($cityTmp != ""){
      // Insert City TMP
      $datainsert = array(
        'nid' => $id,
        'city' => $cityTmp
      );
      $city = $this->db->insert("cities_tmp",$datainsert);
    }
    if($districtTmp != ""){
      // Insert Disctrict TMP
      $datainsert = array(
        'nid' => $id,
        'district' => $districtTmp
      );
      $district = $this->db->insert("districts_tmp",$datainsert);
    }
    return $id;
  }

  public function get($nid) {
    if (!$nid) return FALSE;
    //$this->db->join("cities c", "c.cid=n.city_id", "LEFT");
    //$this->db->join("districts d", "d.did=n.district_id", "LEFT");
    //$this->db->join("provinces p", "p.pvid=n.province_id", "LEFT");

    $this->db->where('nid', $nid);

    //$this->db->join("provinces p", "n.nid, n.rt, n.rw, n.village, p.name as `province`, n.created", "LEFT");
    //$rt = $this->db->rawQuery('SELECT n.nid, n.rt, n.rw, n.village, p.name as `province`, d.name as `district`, p.name as `province`, c.name as `city`, n.created FROM neighbourhoods n JOIN provinces p ON p.pvid = n.province_id JOIN cities c ON c.cid = n.city_id JOIN districts d ON d.did = n.district_id WHERE nid = ?', array($nid));
    //$rt = reset($rt);
    //$rt = reset($this->db->get("neighbourhoods n", null, 'n.nid, n.rt, n.rw, n.village, p.name as `province`, d.name as `district`, c.name as `city`, n.created'));
    $rt = $this->db->getOne('neighbourhoods');
    //$province = $this->app->location->getLocation('provinces', $rt['province_id']);
    /*$rt['province'] = array(
      'province_id' => (is_numeric($rt['province_id'])) ? $rt['province_id'] : '',
      'name' => ($province) ? $province['name'] : '',
    );
    $city = $this->app->location->getLocation('cities', $rt['city_id']);
    $rt['city'] = array(
      'city_id' => (is_numeric($rt['city_id'])) ? $rt['city_id'] : '',
      'name' => ($city) ? $city['name'] : '',
    );

    $district = $this->app->location->getLocation('districts', $rt['district_id']);
    $rt['district'] = array(
      'district_id' => (is_numeric($rt['district_id'])) ? $rt['district_id'] : '',
      'name' => ($district) ? $district['name'] : '',
    );*/

    /*unset($rt['province_id']);
    unset($rt['city_id']);
    unset($rt['district_id']);*/
    if(isset($rt['approval_photo']) && $rt['approval_photo']) {
      $req = $this->app->request;
      $base_url = $req->getUrl()."/";
      $rt['approval_photo'] = $base_url. $rt['approval_photo'];
    }

    return $rt;
  }

  public function total($params = []) {
    unset($params['q']);
    if(isset($params) && count($params) > 0) {
      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(village) LIKE ? OR district_id = ? OR city_id = ? OR province_id = ?)", array('%'.$query.'%', $query, $query, $query));
      }

      $params = $this->appMisc->sanitizeFilterParams($params);
      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }

    $this->db->get('neighbourhoods n');
    $count = $this->db->count;

    return $count;
  }

  public function index($params = []) {
    unset($params['q']);
    $limit = 10;
    $page = 0;
    $order = 'nid';
    $order_type = 'ASC';
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }

      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }

      $params = $this->appMisc->sanitizeFilterParams($params);
      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(village) LIKE ? OR district_id = ? OR city_id = ? OR province_id = ?)", array('%'.$query.'%', $query, $query, $query));
      }
      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
    if (!in_array($order, $this->rtAllowedField)) $order = 'nid';
    $this->db->orderBy($order, $order_type);

    //if ($limit) $this->db->pageLimit = $limit;
    $start = $page * $limit;
    $pager = NULL;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $rt = $this->db->get("neighbourhoods", $pager);
    //$rt = $this->db->arraybuilder()->paginate("neighbourhoods", $page);
    foreach ($rt as &$v) {
      $province = $this->app->location->getLocation('provinces', $v['province_id']);
      $v['province'] = array(
        'province_id' => (is_numeric($v['province_id'])) ? $v['province_id'] : '',
        'name' => ($province) ? $province['name'] : '',
      );
      $city = $this->app->location->getLocation('cities', $v['city_id']);
      $v['city'] = array(
        'city_id' => (is_numeric($v['city_id'])) ? $v['city_id'] : '',
        'name' => ($city) ? $city['name'] : '',
      );

      $district = $this->app->location->getLocation('districts', $v['district_id']);
      $v['district'] = array(
        'district_id' => (is_numeric($v['district_id'])) ? $v['district_id'] : '',
        'name' => ($district) ? $district['name'] : '',
      );
      unset($v['province_id']);
      unset($v['city_id']);
      unset($v['district_id']);
    }
    return $rt;
  }

  public function update($nid, $rtData) {
    $image = $this->uploadPhoto($rtData);
    if ($image) {
      $rtData['approval_photo'] = $image;
    } else {
      unset($rtData['approval_photo']);
    }
    unset($rtData['created']);
    unset($rtData['province']);
    unset($rtData['city']);
    unset($rtData['district']);
    $rtData = $this->appMisc->sanitizeDbData($rtData);
    $rtData = array_filter($rtData);

    $updated = FALSE;
    $this->db->where ('nid', $nid);
    if($rtData) {
      $rtData['updated'] = time();
      $updated = $this->db->update ('neighbourhoods', $rtData);
    }

    return $updated;
  }

  public function block($nid) {
    $rtData = array();
    $rtData['approval_photo'] = '';
    $this->db->where ('nid', $nid);

    $rtData['updated'] = time();
    $updated = $this->db->update ('neighbourhoods', $rtData);
    return $updated;
  }
}