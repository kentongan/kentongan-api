<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 10:28
 */

namespace App\Model;
use App\Wrapper\Misc;

class Event {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->eventAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->eventAllowedField);
  }

  public $fields = array(
    "eid" => array('type' => 'number'),
    "name" => array('type' => 'string', 'format' => 'required'),
    "location" => array('type' => 'string', 'format' => 'required'),
    "start_date" => array('type' => 'datetime'),
    "end_date" => array('type' => 'datetime'),
    "created" => array('type' => 'date'),
    "type" => array('type' => 'string'),
    "description" => array('type' => 'string'),
    "neighbourhood_id" => array('type' => 'number'),
    "createdby" => array('type' => 'number'),
  );

  public function total($params = []) {
    unset($params['q']);
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }

      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(name) LIKE ? OR LOWER(location) LIKE ? OR LOWER(description) = ?)", array('%'.$query. '%', '%'.$query.'%', '%'.$query.'%'));
      }

      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }
    $this->db->orderBy($order, $order_type);
    $this->db->get('events');
    $count = $this->db->count;

    return $count;
  }

  public function index($params = []) {
    $limit = 10;
    $page = 0;
    $order = 'created';
    $order_type = 'DESC';
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }

      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(name) LIKE ? OR LOWER(location) LIKE ? OR LOWER(description) = ?)", array('%'.$query. '%', '%'.$query.'%', '%'.$query.'%'));
      }

      if(isset($params['order']) && $params['order']) {
        $tmp = explode(':', $params['order']);
        $order = $tmp[0];
        if (count($tmp) > 1) $order_type = strtoupper($tmp[1]);
      }
      $params = $this->appMisc->sanitizeFilterParams($params);

      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
      //$users = $this->db->get('peoples');
    }
    if (!in_array($order_type, array('ASC', 'DESC'))) $order_type = 'ASC';
    if (!in_array($order, $this->eventAllowedField)) $order = 'eid';
    $this->db->orderBy($order, $order_type);

    //if ($limit) $this->db->pageLimit = $limit;
    //$users = $this->db->arraybuilder()->paginate("events", $page);
    $start = $page * $limit;
    $pager = NULL;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $events = $this->db->get("events", $pager);
    return $events;
  }

  public function get($id) {
    $this->db->where('eid', $id);

    $notification = $this->db->getOne("events");
    return $notification;
  }

  public function create($reportData) {
    $reportData = $this->appMisc->sanitizeDbData($reportData);

    $id = FALSE;
    if(count($reportData)) {
      $reportData['created'] = time();
      $id = $this->db->insert('events', $reportData);
    }
    return $id;
  }

  public function update($id, $reportData) {
    unset($reportData['neighbourhood_id']);
    unset($reportData['created']);
    $reportData = $this->appMisc->sanitizeDbData($reportData);
    $updated = FALSE;
    $this->db->where ('eid', $id);
    if($reportData) {
      $updated = $this->db->update ('events', $reportData);
    }
    return $updated;
  }

  public function delete($id) {
    $this->db->where('eid', $id);
    if($this->db->delete('events')) {
      return TRUE;
    }
    return FALSE;
  }
}
