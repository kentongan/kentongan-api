<?php
namespace App\Model;
use App\Wrapper\Misc;

class Location {
  protected $app;
  protected $db;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->db = $this->app->mysql;
    $this->appMisc = new Misc();
    $this->userAllowedField = array_keys($this->fields);
    $this->appMisc->setAllowedDbField($this->userAllowedField);
  }

  public $fields = array(
    "cid" => array('type' => 'number'),
    "did" => array('type' => 'number'),
    "pvid" => array('type' => 'number'),
    "province_id" => array('type' => 'number'),
    "district_id" => array('type' => 'number'),
    "city_id" => array('type' => 'number'),
    "name" => array('type' => 'string'),
  );

  public function getProvince(){
    $this->db->orderBy("name","asc");
    $provinces = $this->db->get("provinces");
    foreach ($provinces as $key => &$value) {
    	$value['name'] = $value['name'];
    }
  	return $provinces;
  }

  public function getTotalProvince(){
    $provinces = $this->db->get("provinces");
    $count = $this->db->count;
    return $count;
  }
  public function getCity( $provinceid ){
    $this->db->where("province_id",$provinceid);
    $this->db->orWhere("ISNULL(province_id)");
    $this->db->orderBy("name","asc");
    return $this->db->get("cities");
  }

  public function getTotalCity(){
    $cities = $this->db->get("cities");
    $count = $this->db->count;
    return $count;
  }

  public function getDistrict( $cityid ){
    $this->db->where("city_id",$cityid);
    //$this->db->orWhere("ISNULL(city_id)");
    $this->db->orderBy("name", 'ASC');
    $district = $this->db->get("districts");
    //return $district;
    $getOther = $this->db->where("ISNULL(city_id)");
  	$district2 = $this->db->get("districts");
  	
  	$district[] = $district2[0];
  	return $district;
  }

  public function getTotalDistrict(){
    $cities = $this->db->get("districts");
    $count = $this->db->count;
    return $count;
  }




  public function getLocation($type, $id) {

    if (!in_array($type, array('provinces', 'cities', 'districts'))) return FALSE;
    if(!$id) return FALSE;
    $idKey = '';
    if ($type == 'provinces') {
      $idKey = 'pvid';
    } else if ($type == 'cities') {
      $idKey = 'cid';
    } else {
      $idKey = 'did';
    }
    $this->db->where($idKey, $id);
    return $this->db->getOne($type);
  }

  public function total($type, $params = []) {
    unset($params['q']);
    if(isset($params) && count($params) > 0) {
      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(name) LIKE ?)", array('%'.$query.'%'));
      }

      $params = $this->appMisc->sanitizeFilterParams($params);
      foreach($params as $param => $value) {
        $this->db->where($param, $value);
      }
    }

    $this->db->get($type);
    $count = $this->db->count;

    return $count;
  }

  public function index($type, $params) {
    if (!in_array($type, array('provinces', 'cities', 'districts'))) return FALSE;
    unset($params['q']);
    $limit = 10;
    $page = 0;
    if(isset($params) && count($params) > 0) {
      if (isset($params['limit'])) {
        $limit = $params['limit'];
      }
      if (isset($params['page'])) {
        $page = $params['page'];
      }
      $params = $this->appMisc->sanitizeFilterParams($params);
      if (isset($params['query']) && $params['query']) {
        $query = strtolower($params['query']);
        $this->db->where ("(LOWER(name) LIKE ?)", array('%'.$query.'%'));
      }
    }

    $this->db->pageLimit = $limit;
    $pager = NULL;
    $start = $page * $limit;
    if ($limit) {
      $pager = array($start, $limit);
    }
    $location = $this->db->get($type, $pager);
    return $location;
  }
}