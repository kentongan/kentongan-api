<?php
namespace App\Notification;

use App\Wrapper\Misc;

//use PHPMailer;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
use Postmark\Models\PostmarkAttachment;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Services_Twilio;
//use Twilio;
use NexmoMessage;

define('SMTPUSER', 'developer@skyshi.com');
define('SMTPPASS', 'skysh1123%');

define('NEXMOFROM', '+6285574719200');
define('NEXMOACCOUNTKEY', '56d525f3');
define('NEXMOACCOUNTSECRET', 'd6ab199b');

/*define('TWILIOSID', 'ACae15702652ed419e5724074fd5012593');
define('TWILIOTOKEN', '8287486b4294cf6a30c315d2793c1692');
define('TWILIOFROM', '+16097724622');*/

define('PMASERVERTOKEN', '3bb6c98e-cfb6-41c5-8b80-fb96c4ea3fbc');
define('PMASIGNATURE', 'mail@kentongan.org');

class Notification {
  public function __construct() {
    $this->appMisc = new Misc();
  }

  public function alertNotification($channel_name, $message, $data, $mode) {
    $NEW_PARSE_SERVER = [
      'URL' => 'https://pg-app-4xcls0uay9pc8fu4ips8742vcxzkre.scalabl.cloud',
      'PATH' => '1'
    ];

    if(is_string($channel_name)) {
      $channel_name = array($channel_name);
    }
    $data['alert'] = $message;

    $config = array(
      'app_id' => 'Go7tNnzooiqdv5kiSE8QxwksB4ZTQV7S7GA6P7Ad',
      'master_key' => 'Mh4JRMN7G8nP070OZPQzTeITk1mdxNLqqX7D6Gwz',
      'rest_key' => 'BfAQKHp0VOXEN8WXsDMaaHIkCe4NTqXTLOGTI7C8'
    );

    if ($mode == 'live' || $mode == 'dev') {  
      ParseClient::initialize($config['app_id'], $config['rest_key'], $config['master_key'] );
      $query = ParseInstallation::query();
      $query->notContainedIn("channels", array($channel_name['pid']));
      $channel = array();
      // Filter By App Version
      if(isset($channel_name['appVersion'])){
        $query->startsWith("appVersion",$channel_name['appVersion']);
      }
      // Filter By Name
      if(isset($channel_name['name'])){
        $channel = array_merge($channel,array($channel_name['name']));
        // $query->containedIn("channels", array($channel_name['name']));
      }
      // Filter By Device Type
      if(isset($channel_name['deviceType'])){
        $query->equalTo("deviceType",$channel_name['deviceType']);
      }
      // Filter By Channel Name
      if (isset($channel_name['target_pid'])) {
        if(is_array($channel_name['target_pid'])){
          $channel = array_merge($channel, $channel_name['target_pid']);
          // $query->containedIn("channels", $channel_name['target_pid']);
        }
        else {
          $channel = array_merge($channel, array($channel_name['target_pid']));
          // $query->containedIn("channels", array($channel_name['target_pid']));
        }
      } elseif(isset($channel_name['rt'])) {
          $channel = array_merge($channel, array($channel_name['rt']));
          // $query->containedIn("channels", array($channel_name['rt']));
      } else {
        // $query->containedIn("channels", array($channel_name['rt']));
      }
      $query->containedIn("channels", $channel);
      ParseClient::setServerURL('https://api.parse.com', '1');
      $send = ParsePush::send(array(
        "where" => $query,
        "data" => $data
      ));
      // Send to sashido.io 
      ParseClient::setServerURL($NEW_PARSE_SERVER['URL'], $NEW_PARSE_SERVER['PATH']);
      ParsePush::send(array(
        "where" => $query,
        "data" => $data
      ));
      print "\nquery: \n";
      print_r($query);
    } else {
      /*$config = array(
        'app_id' => 'ii6GRIV1a7F4zWVg3Nzvoamx0ioEySAvvuQG0xRL',
        'master_key' => 'arQ69xJQpoDY1j5OHYbnziUizkYxDD1KeSCZJZAB',
        'rest_key' => 'duNPqNULjHYiU0LTYP75en0UpfDfbjNp9LGPg8gz'
      );*/
      ParseClient::initialize($config['app_id'], $config['rest_key'], $config['master_key'] );
      $channel_name = array($channel_name['rt']);
      if (isset($channel_name['target_pid'])) {
        if(is_array($channel_name['target_pid'])){
          $channel_name = $channel_name['target_pid'];
        } else {
          $channel_name = array($channel_name['target_pid']);
        }
      }
      ParseClient::setServerURL('https://api.parse.com', '1');
      $send = ParsePush::send(array(
        "channels" => $channel_name,
        "data" => $data
      ));
      // Send to sashido.io 
      ParseClient::setServerURL($NEW_PARSE_SERVER['URL'], $NEW_PARSE_SERVER['PATH']);
      ParsePush::send(array(
        "channels" => $channel_name,
        "data" => $data
      ));
    }
    print "\nmode: ";
    print_r($mode);
    
    print_r($send);
    return $send;
  }

  public function userNotification($type, $template, $params) {
    $template = $this->{$template}($params);
    if ($type == 'mail') {
      $subject = $template['subject'];
      $email = $params['email'];
      if(strpos($email, '@kentongan.org') !== FALSE) {
        print 'send email to demo, skip it!';
        return;
      }
      $body = $template['body'];
      $emailParams = array(
        'to' => array("email" => $email, 'name' => ""),
        'subject' => $subject,
        'message' => $body
      );
      print 'send mail';
      $this->sendMail($emailParams);
    }
    if ($type == 'mobile') {
      $phone = $params['phone'];
      if(strpos($phone, '0000000') !== FALSE) {
        print 'send sms to demo, skip it!';
        return;
      }
      print 'template'. $template;
      $mobileParams = array(
        'to' => $phone,
        'message' => $template,
      );
      print 'send sms';
      $this->sendSMS($mobileParams);
    }
    //call_user_func(array($this, $type), $params);
  }

  public function errorNotification($message) {
    $level = $message['level'];
    unset($message['level']);
    $channel = '@andreas';
    $ch = curl_init("https://slack.com/api/chat.postMessage");
    $attachment = array(
      "fallback" => print_r($message, true),
      "color" => "danger",
      "fields" => [[
        "title" => "Level",
        //"value" => $level,
        "value" => "ERROR",
        "short" => true
      ]],
      "title" => "Message",
      "text" => print_r($message, true)
    );
    $slackData = [
      "token" => "xoxp-2999186773-2999610299-18369773650-cb5c7f3413",
      "channel" => $channel,
      "text" => 'bek...ada error nih...jangan lupa dicatet ya :huahaha:',
      "username" => "LogApiBot",
      "attachments" => json_encode(array($attachment))
    ];
    $data = http_build_query($slackData);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    print_r($result);
  }

  public function newUserNotification($param) {
    $level = $param['level'];
    unset($param['level']);
    $channel = 'kentongan-marketing';
    $ch = curl_init("https://slack.com/api/chat.postMessage");
    $attachment = array(
      "fallback" => print_r($param, true),
      "color" => "danger",
      /*"fields" => [[
        "title" => "Level",
        //"value" => $level,
        "value" => "ERROR",
        "short" => true
      ]],*/
      "title" => "Message",
      "text" => print_r($param, true)
    );
    $slackData = [
      "token" => "xoxb-78287854770-Yz0O9FMvca5LKFyrtVAPIOGs",
      "channel" => $channel,
      "text" => $param['type'] . ' ' . $param['phone'],
      "username" => "kentongan-bot",
      //"attachments" => json_encode(array($attachment))
      "as_user" => "true",
    ];
    $data = http_build_query($slackData);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    print_r($result);
  }

  public function sendMail($params, $attachment = array() ){
    if(!isset($params)){
      return FALSE;
    }
    else {
      try{
        $client = new PostmarkClient(PMASERVERTOKEN);
        if( count($attachment) == 0 ){
          $sendResult = $client->sendEmail(PMASIGNATURE,
            $params['to']['email'],
            $params['subject'],
            $params['message']
          );
        }
        else {
          $attachment = PostmarkAttachment::fromFile($attachment['source'], $attachment['fileName'], $attachment['appType']);
          $sendResult = $client->sendEmail(PMASIGNATURE,
            $params['to']['email'],
            $params['subject'],
            $params['message'],
            null,null,true,null,null,null,null,[$attachment]
          );
        }
        print_r($sendResult);
      }catch(PostmarkException $ex){
        // If client is able to communicate with the API in a timely fashion,
        // but the message data is invalid, or there's a server error,
        // a PostmarkException can be thrown.
        echo $ex->httpStatusCode;
        echo $ex->message;
        echo $ex->postmarkApiErrorCode;

      }catch(Exception $generalException){
        // A general exception is thown if the API
        // was unreachable or times out.
      }
      return TRUE;
    }
  }

  public function sendSMS($params) {
    $to = $params['to'];
    $message = $params['message'];
    $sms = new \NexmoMessage(NEXMOACCOUNTKEY, NEXMOACCOUNTSECRET);
    $send = $sms->sendText($to, NEXMOFROM, $message);
    print_r($send);
    return $send;
  }

  public function registerMailNotification($params) {
    $subject = 'Selamat Bergabung di Kentongan';
    $password = $params['password'];
    $email = $params['email'];
    $message = <<<EOL
      Selamat, Anda telah terdaftar dalam aplikasi Kentongan. Kentongan adalah aplikasi berbasis iOS dan Android yang membantu meningkatkan keamanan lingkungan RT.<br/><br/>
      Informasi lebih lanjut mengenai kentongan bisa lihat pada http://kentongan.org <br /><br />
      Informasi login : <br /><br />
      Email: $email<br />
      Password: $password<br /><br />
      Kentongan, Cara smart bertetangga.
EOL;

    $body = $this->mainMailTemplate($message);
    $data = array(
      'subject' => $subject,
      'body' => $body,
    );
    return $data;
  }

  public function registerMobileNotification($params) {
    $password = $params['password'];
    $phone = $params['phone'];
    $message = "Info masuk Kentongan.
No.HP: ".$phone."
Password: ".$password.".
Daftarkan warga lain untuk tetap terhubung dengan lingkungan RT Anda.";
    $this->newUserNotification(array('phone' => $phone, 'type' => 'newRT'));
    return $message;
  }

  public function changeMobileNotification($params) {
    $password = $params['password'];
    $phone = $params['phone'];
    $message = "Password Kentongan Anda telah diubah, login dengan :
No.HP: ".$phone."
Password: ".$password;
    return $message;
  }

  public function inviteMailNotification($params) {
    $password = $params['password'];
    $email = $params['email'];
    $rt = $params['rt'];
    $rw = $params['rw'];
    $village = $params['village'];
    $rt_name = ucwords($params['rt_name']);

    $subject = "Ketua RT ($rt_name) mengajak Anda bergabung di aplikasi Kentongan";
    $message = <<<EOL
      Ketua RT ($rt_name) mengundang Anda untuk ikut bergabung dalam aplikasi Kentongan untuk RT $rt / RW $rw $village.<br/><br/>
      Laporkan keadaan bahaya Anda melalui Kentongan agar tetangga dapat membantu.<br/><br/>
      Silakan download aplikasinya di http://kentongan.org.<br /><br />
      Login dengan: <br />
      Email: $email<br />
      Password: $password<br /><br />
      Kentongan, Cara smart bertetangga.
EOL;

    $body = $this->mainMailTemplate($message);

    $data = array(
      'subject' => $subject,
      'body' => $body,
    );
    return $data;
  }
  public function inviteMobileNotification($params) {
    $password = $params['password'];
    $phone = $params['phone'];
    $rt = $params['rt'];
    $rw = $params['rw'];
    if(!isset($params['role'])){
      $params['role'] = "rt";
    }
    $role = ($params['role'] == "rt") ? "Pengurus RT" : "Bpk/Ibu" ;
    $rt_name = ucwords($params['rt_name']);
    $village = $params['village'];
    $message = $role . " " . $rt_name." mengundang semua Warga RT di aplikasi Kentongan. Unduh gratis di https://goo.gl/oy3njH masuk dengan 
No.HP :".$phone."
Password: ".$password;
    $this->newUserNotification(array('phone' => $phone, 'type' => 'newWarga'));
    return $message;
  }

  public function resetMobileNotification($params) {
    $password = $params['password'];
    $phone = $params['phone'];
    $message = "Password Kentongan Anda berhasil diubah. Silakan login dengan:
No HP : ".$phone."
Password : ".$password."";
    return $message;
  }

  public function resetMailNotification($params) {
    $subject = 'Password Kentongan Anda berhasil diubah';
    $password = $params['password'];
    $email = $params['email'];
    $message = <<<EOL
      Password Anda di aplikasi Kentongan berhasil diubah. Silakan login dengan data berikut:<br/>
      Email: $email<br />
      Password : $password<br /><br />
      Kentongan, Cara smart bertetangga.
EOL;

    $body = $this->mainMailTemplate($message);
    $data = array(
      'subject' => $subject,
      'body' => $body,
    );
    return $data;
  }

  public function invitorMobileNotification($params) {
    $name = ucwords($params['name']);
    $rt_name = ucwords($params['rt_name']);
    $message = $name . " mengajak Ketua RT mendaftarkan lingkungan RT Anda di aplikasi Kentongan. Unduh gratis di https://goo.gl/oy3njH";
    $this->newUserNotification(array('phone' => $params['phone'], 'type' => 'inviteRT'));
    return $message;
  }

  public function updateMobileNotification() {
    $message = '';
    return $message;
  }

  public function reinviteMobileNotification($params) {
    $phone = $params['phone'];
    $password = $params['password'];
    $message = "Bergabunglah di aplikasi Kentongan bersama Ketua RT Anda. Unduh gratis di https://goo.gl/oy3njH masuk dengan No. HP ".$phone." dan password ".$password;
    return $message;
  }

  public function mainMailTemplate($message){
    $content = '<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="Content-type" content="text/html; charset=utf-8">
          <meta content="telephone=no" name="format-detection">
          <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
          <!-- Facebook sharing information tags -->
          <title>Email Template</title>
          <link href="https://fonts.googleapis.com/css?family=Lato:400,700,300" rel="stylesheet" type="text/css">

          <style type="text/css">
              body{
                padding:0 !important;
                margin:0 !important;
                display:block !important;
                -webkit-text-size-adjust:none;
              }
              a{
                color:#7ba436;
                text-decoration:underline;
              }
              img{
                max-width:100% !important;
              }
              p{
                padding:0 !important;
                margin:0 !important;
              }
              .col{
                padding:0 15px !important;
              }
          </style>
        </head>
        <body>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #f7f7f7;">
          <tr>
            <td align="center" valign="top">
              <table bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" style="width: 100%; max-width: 530px;">
                <tr>
                  <td>
                    <!-- Header -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center">
                          <table class="col" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="width: 100%; max-width: 460px;">
                            <tr>
                              <td style="font-size:0pt; line-height:0pt; height:48px">&nbsp;</td>
                            </tr>
                            <tr>
                              <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="http://kentongan.org" target="_blank"><img src="https://gallery.mailchimp.com/958b211384b8d5c3188f0ccfe/images/3e1271f3-4d59-421d-9454-9f2a72eeedc1.png" alt="" border="0"></a>
                              </td>
                            </tr>
                            <tr>
                              <td style="font-size:0pt; line-height:0pt; height:24px">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!-- END Header -->


                    <!-- content -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center">
                          <table class="col" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="width: 100%; max-width: 460px;">
                            <tr>
                              <td style="font-size:0pt; line-height:0pt; height:24px">&nbsp;</td>
                            </tr>
                            <tr>
                              <td style="font-size:18px; line-height:30px; text-align:center; color: #402816; font-family: \'Lato\', sans-serif; text-align:left;">
                                '.$message.'
                              </td>
                            </tr>
                            <tr>
                              <td style="font-size:0pt; line-height:0pt; height:24px">&nbsp;</td>
                            </tr>
                            <tr>
                              <td style="font-size:14px; line-height:24px; text-align:left; color: #ffffff; font-family: \'Lato\', sans-serif;">
                                Kentongan Team
                              </td>
                            </tr>
                            <tr>
                              <td style="font-size:0pt; line-height:0pt; height:24px">&nbsp;</td>
                            </tr>
                            <tr>
                              <td style=" text-align: center; padding-bottom: 24px;">
                                <img src="https://gallery.mailchimp.com/958b211384b8d5c3188f0ccfe/images/a77fcd1c-c2fd-40ec-853a-61c1cb04962c.png" alt="kentongan">
                              </td>
                            </tr>
                            <tr>
                              <td style="text-align: center; padding-bottom: 24px;">
                                <table style="width:100%">
                                  <tr>
                                    <td style="text-align:right;width:50%;">
                                      <table style="border-radius:50%;width:36px;height:36px;background:#FE5722;margin-left:80%;">
                                        <tbody>
                                          <tr>
                                            <td style="text-align:center;vertical-align:middle;"><a href="https://facebook.com/kentongan" style="color:#fff;"><img alt="facebook" src="https://d2q0qd5iz04n9u.cloudfront.net/_ssl/proxy.php/http/kentongan.org/img/fb.png" style="padding-top: 5px;" /></a></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                    <td style="text-align:left;width:50%;">
                                      <table style="border-radius:50%;width:36px;height:36px;background:#FE5722;margin-right:80%;">
                                        <tbody>
                                          <tr>
                                            <td style="text-align:center;vertical-align:middle;"><a href="https://twitter.com/kentongan_id" style="color:#fff;"><img alt="twitter" src="https://d2q0qd5iz04n9u.cloudfront.net/_ssl/proxy.php/http/kentongan.org/img/twitter.png" style="padding-top: 5px;" /></a></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!-- END content -->
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        </body>
        </html>';
    return $content;
  }
}
