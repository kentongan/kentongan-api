<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/20/15
 * Time: 09:04
 */

namespace App\Wrapper;


class Misc {
  private $allowedDbField;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }

  public function getMailTemplate($type, $params) {
    if ($type == 'mailInvitation') return $this->getMailInvitationTemplate($params);
    if ($type == 'mobileInvitation') return $this->getMobileInvitationTemplate($params);
  }

  public function inputValidate($datainput,$arrayfield){
    $errors = array();
    /*if( count($datainput) != count($arrayfield) ){
      $errors['general'] = "Jumlah Field tidak sesuai";
      return $errors;
    }*/

    foreach($arrayfield as $_key => $_value){
      $required = false;
      $maxlength = null;
      $type = (isset($_value['type'])) ? $_value['type'] : "text" ;

      /*if(!isset($datainput[$_key])){
        unset($datainput[$_key]);
      }*/

      $lang = $this->lang();
      // parsing format parameters
      if(isset($_value['format'])) {
        $format = @$_value['format'];
        $exformat = explode(",",$format);

        foreach($exformat as $formats){
          $subformats = explode(":",$formats);
          if($subformats[0] == "required"){
            $required = true;
            if(!isset($datainput[$_key]) || $datainput[$_key] == ""){
              $errors[$_key]['required'] = "Kolom " . (isset($lang[$_key]) ? $lang[$_key] : $_key) . " tidak boleh kosong";
            }
          }
          if($subformats[0] == "maxlength"){
            $maxlength = $subformats[1];

            if(isset($datainput[$_key]) && strlen($datainput[$_key]) > $maxlength ){
              $errors[$_key]['maxlength'] = "Kolom " . (isset($lang[$_key]) ? $lang[$_key] : $_key) . " hanya boleh terisi " . $maxlength . " karakter";
            }
          }
        }
      }
      if(!isset($datainput[$_key])) continue;
      // Validating Type
      if( $type == "number" ){
        $validate = $this->validateNumber($datainput[$_key]);
        if( (string)$validate == "wrong" ){
          $errors[$_key]['validate'] = "Kolom " . (isset($lang[$_key]) ? $lang[$_key] : $_key) . " hanya boleh berisi angka";
        }
      }
      elseif( $type == "date" ){
        $validate = $this->validateDate($datainput[$_key]);
        if(!$validate){
          $errors[$_key]['validate'] = "Kolom " . (isset($lang[$_key]) ? $lang[$_key] : $_key) . " format tanggal tidak sesuai yyyy-mm-dd";
        }
      }
      elseif( $type == "datetime" ) {
        $validate = $this->validateDateTime($datainput[$_key]);
        if(!$validate){
          $errors[$_key]['validate'] = "Kolom " . (isset($lang[$_key]) ? $lang[$_key] : $_key) . " format tanggal dan waktu tidak sesuai yyyy-mm-dd hh:ii";
        }
      }
      elseif( $type == "email" ) {
        $validate = $this->validateEmail($datainput[$_key]);
        if(!$validate){
          $errors[$_key]['validate'] = "Kolom " . (isset($lang[$_key]) ? $lang[$_key] : $_key) . " format email tidak valid";
        }
      }
    }
    return $errors;
  }

  public function validateEmail( $data ){
    if( filter_var($data, FILTER_VALIDATE_EMAIL) ){
      return $data;
    }
    else {
      return 0;
    }
  }

  public function validateNumber( $numberInput ){
    if( is_numeric($numberInput)){
      return $numberInput;
    }
    else {
      return "wrong";
    }
  }

  public function validateDate( $dateInput ){
    if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $dateInput )) {
      return false;
    }
    return true;
  }

  public function validateDateTime( $dateTimeInput ){
    if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([0-1][0-9]|2[0-3]):([0-5][0-9])$/", $dateTimeInput )) {
      return false;
    }
    return true;
  }

  public function setAllowedDbField($allowedDbField) {
    $this->allowedDbField = $allowedDbField;
  }

  public function sanitizeFilterParams($params) {
    unset($params['limit']);
    unset($params['page']);

    unset($params['order']);
    unset($params['order_type']);

    $params = $this->sanitizeDbData($params);
    return $params;
  }

  public function sanitizeDbData($data) {
    $allowed = $this->allowedDbField;
    $data = array_change_key_case($data);
    $data = array_intersect_key($data, array_flip($allowed));
    $data = array_map('trim', $data);
    $idKeys = array('pid', 'nid', 'eid', 'aid', 'rid');
    foreach ($idKeys as $id) {
      unset($data[$id]);
    }
    return $data;
  }

  public function lang(){
    $lang['name'] = "Nama";
    $lang['birth_date'] = "Tanggal lahir";
    $lang['birth_place'] = "Tempat lahir";
    $lang['phone'] = "No. HP";
    $lang['email'] = "Email";
    $lang['card_id'] = "No. KTP / NIK";
    $lang['family_id'] = "Kode keluarga";
    $lang['address'] = "Alamat";
    $lang['gender'] = "Jenis Kelamin";
    $lang['email'] = "Email";
    $lang['password'] = "Password";
    $lang['role'] = "Hak Akses";
    $lang['photo'] = "Foto";
    $lang['family_type'] = "Status di Keluarga";
    $lang['description'] = "Deskripsi";
    $lang['response'] = "Tanggapan";
    return $lang;
  }

}