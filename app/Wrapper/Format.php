<?php
  namespace App\Wrapper;

  class Format {
    protected $total;
    protected $forceStop;

    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->total = 0;
      $this->count = 0;
      $this->forceStop = TRUE;
    }

    public function setTotal($total) {
      $this->total = $total;
    }

    public function setCount($count) {
      $this->count = $count;
    }

    public function setForceStop($forceStop) {
      $this->forceStop = $forceStop;
    }

    public function formatJson($code, $message, $data = []) {
      $status = FALSE;
      if ($code == 200) $status = TRUE;
      if (array_values($data) === $data) {
        $count = count($data);
      } else {
        $count = $this->count;
      }
      if ($code == 406) {
        $count = 0;
        $this->total = 0;
      }
      $output = array(
        'code' => $code,
        'status' => $status,
        'message' => $message,
        'data' => array(
          'total' => $this->total,
          'count' => $count,
          'rows' => $data
        ),
      );
      $this->app->render($code, $output);
      if ($this->forceStop) {
        $this->app->stop();
      }
      //$this->app->stop();
      //$this->app->halt(403, 'You shall not pass!');
      /*$this->app->response->status($code);
      $this->app->response()->header('Content-Type', 'application/json');
      $this->jsonp_callback = $this->app->request->get('callback', null);
      $this->app->view->appendData($output);
      $this->app->view->display($code);*/
      //return 0;
      //$this->app->halt($code, $message);
    }

    public function formatDataOutput($fields, $data) {
      $data = array_intersect_key($data, array_flip(array_keys($fields)));
      $data = array_map(function($v) {
        return (!is_array($v)) ? trim ($v) : $v;
      }, $data);

      $data = array_map(function($v){
        return (is_null($v)) ? "" : $v;
      }, $data);

      if (isset($data['created']) && $data['created']) {
        $data['created'] = date('Y-m-d H:i', $data['created']);
      }

      foreach($data as $k => &$v) {
        if($k != 'birth_date' && strpos($k, '_date') !== FALSE && !empty($v)) {
          $v = date('Y-m-d H:i', $v);
        }
        if($k == 'birth_date' && !empty($v)) $v = date('Y-m-d', $v);
      }

      return $data;
    }
  }