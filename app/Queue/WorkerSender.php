<?php

namespace App\AmqpWrapper;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class WorkerSender {

  /**
   * Sends an invoice generation task to the workers
   *
   * @param string $queueName
   * @param string $jsonData
   */
  public function execute($queueName, $jsonData)
  {
    $exchange = 'router';
    $queue = $queueName;
    $conn = new AMQPStreamConnection(AMQHOST, AMQPORT, AMQUSER, AMQPASS, AMQVHOST);
    $ch = $conn->channel();
    $ch->queue_declare($queue, false, true, false, false);
    $ch->exchange_declare($exchange, 'direct', false, true, false);
    $ch->queue_bind($queue, $exchange);
    $msg = new AMQPMessage($jsonData, array('content_type' => 'text/plain', 'delivery_mode' => 2));
    $ch->basic_publish($msg, $exchange, $queueName);
    $ch->close();
    $conn->close();
  }
}