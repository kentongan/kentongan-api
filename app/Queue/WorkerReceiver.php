<?php
namespace App\AmqpWrapper;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Notification\Notification;

define('AMQHOST', 'localhost');
define('AMQPORT', 5672);
define('AMQUSER', 'guest');
define('AMQPASS', 'guest');
define('AMQVHOST', '/');

class WorkerReceiver {
  protected $queueName;
  public function listen($queueName) {
    print $queueName;

    $exchange = 'router';
    $queue = $queueName;
    $this->queueName = $queueName;
    $consumer_tag = $queueName;
    $conn = new AMQPStreamConnection(AMQHOST, AMQPORT, AMQUSER, AMQPASS, AMQVHOST);
    $ch = $conn->channel();

    $ch->queue_declare($queue, false, true, false, false);
    $ch->exchange_declare($exchange, 'direct', false, true, false);
    $ch->queue_bind($queue, $exchange, $queueName);
    $ch->basic_consume($queue, $consumer_tag, false, false, false, false, array($this, $queueName));
    register_shutdown_function(array($this, 'shutdown'), $ch, $conn);

    // Loop as long as the channel has callbacks registered
    while (count($ch->callbacks)) {
      $ch->wait();
    }
  }
  /**
   * @param \PhpAmqpLib\Channel\AMQPChannel $ch
   * @param \PhpAmqpLib\Connection\AbstractConnection $conn
   */
  public function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
  }

  public function userNotification($msg) {
    if ($this->queueName == 'userNotification') {
      $notification = new Notification();

      $data = json_decode($msg->body, true);
      print "====\n";
      print_r($data);
      print "====\n";
      $type = $data['type'];
      $template = $data['template'];
      $params = $data['params'];

      $notification->userNotification($type, $template, $params);
      $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
    /*
    // Send a message with the string "quit" to cancel the consumer.
    if ($msg->body === 'quit') {
      $msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
    }*/
  }
  public function alertNotification($msg) {
    //print $this->queueName;
    if ($this->queueName == 'alertNotification') {
      $notification = new Notification();

      $messgeData = json_decode($msg->body, true);
      print "====\n";
      print_r($messgeData);
      print "====\n";
      $channel_name = $messgeData['channel_name'];
      $message = $messgeData['message'];
      $mode = $messgeData['mode'];
      $data = $messgeData['data'];

      $notification->alertNotification($channel_name, $message, $data, $mode);
      $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
  }
  public function errorNotification($msg) {
    //print $this->queueName;
    print_r($this->queueName);
    if ($this->queueName == 'errorNotification') {
      $notification = new Notification();

      $messgeData = json_decode($msg->body, true);
      print "====\n";
      print_r($messgeData);
      print "====\n";
      $message = $messgeData['message'];

      $notification->errorNotification($message);
      $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
  }
}