<?php

namespace App\Controller;

use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;
use App\Transformer\NotificationTransformer;
use App\Transformer\NotificationCommentTransformer;

class NotificationController {

  use FractalTrait;

  protected $app;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $fields = $this->app->notif->fields;
    $this->fields = $fields;
  }

  public function index(){
    $loggedUser = $this->app->loggedUser;
    $page = $this->app->request->get('page');
    if (!$page){ $page = 1; }
    $total = $this->app->notif->total();
    $notifications = $this->app->notif->index($page);
    $notifications = $this->getCollection($notifications, new NotificationTransformer());
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Notifications', $notifications);
  }

  public function create(){
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $this->response = $this->response;
    if($loggedUser['role'] != 'admin') {
      $this->response->formatJson(406, 'Pengisian harus dilakukan oleh hak akses admin');
    }
    $notificationData = $this->app->request->post();
    $code = 406;
    $notification = [];
    if ($notificationData){
      $notificationData['type'] = 'notif';
      $errors = $this->appMisc->inputValidate($notificationData, $this->fields);
      if($errors){
        $this->response->formatJson(406, 'Validation Error', $errors);
      }
      $notificationId = $this->app->notif->create($notificationData);
      if($notificationId){
        $base_url = $this->app->request->getUrl();
        $notification = $this->app->notif->get($notificationId);
        $notification['image'] = ($notification['image'] != "") ? $base_url . "/" . $notification['image'] : "";
        $content = json_decode($notification['content']);
        $alertMessage = $notification['title'] . " : " . $notification['content'];
        $queueParams = array(
          'channel_name' => array(
            'pid' => "pid-". $loggedUser['pid'],
            'name' => "kentongan",
            'appVersion' => "2.2.6",
            'deviceType' => "android",
            // 'rt' => 'rt-1'
          ),
          'message' => $alertMessage,
          'data' => array(
            'notification' => $notification['id'],
            'activity_type' => 'notif',
            'pid' => $loggedUser['pid'],
            'sound' => 'default'
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));

        $code = 200;
        $message = 'Status berhasil dibuat';
        $this->response->setTotal(1);
        $notification = $this->getCollection(array($notification), new NotificationTransformer());
      } else {
        $message = 'Status gagal dibuat';
      }
      $this->response->formatJson($code, $message, $notification);
    }
  }

  public function detail($id){
    $this->response = $this->response;
    $loggedUser = $this->app->loggedUser;
    if(!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'User belum terdaftar disuatu RT');

    $notification = $this->app->notif->get($id);
    if($notification) {
      $notification = [$this->getItem($notification, new NotificationTransformer())];
      $code = 200;
      $message = 'Notification ditemukan';
      $this->response->setTotal(1);
    } else {
      $notification = [];
      $this->response->setTotal(0);
      $code = 406;
      $message = 'Notification tidak ditemukan';
    }

    $this->response->formatJson($code, $message, $notification);
  }

  public function delete($id) {
    $loggedUser = $this->app->loggedUser;
    $this->response = $this->response;
    $code = 406;
    $notification = $this->app->notif->get($id);
    if (!$notification) {$this->response->formatJson($code, 'Notification tidak ditemukan');}
    if($loggedUser['role'] != 'admin') {
      $this->response->formatJson(406, 'Penghapusan harus dilakukan oleh hak akses admin');
    }

    $message = 'Notifikasi gagal dihapus';
    if($this->app->notif->delete($id)){
       $code = 200;
       $message = 'Notifikasi berhasil dihapus';
    }

    $this->response->formatJson($code, $message, [$this->getItem($notification, new NotificationTransformer())]);
  }

  public function createComment($notificationId) {
    $sender = new WorkerSender();
    $notification = $this->app->notif->get($notificationId);
    if (!$notification) {$this->response->formatJson(406, 'Notification tidak ditemukan');}
    $loggedUser = $this->app->loggedUser;
    $commentData = $this->app->request->post();
    if ($commentData) {
      $commentData['notification_id'] = $notificationId;
      $commentData['people_id'] = $loggedUser['pid'];
      $errors = $this->appMisc->inputValidate(
        $commentData,
        $this->app->notifComment->fields
      );

      if ($errors) {
        $this->response->formatJson(406, 'Validation error', $errors);
      }

      $commentId = $this->app->notifComment->create($commentData);
      if ($commentId) {
        $mycomment = $this->app->notifComment->get($commentId);
        $people = $this->app->user->get($mycomment['people_id']);
        $alertMessage = $people['name'] . " mengomentari sebuah pemberitahuan : " . $mycomment['content'];
        $commenters = $this->app->notifComment->getByNotification($notification['id']);
        $target_pid = array();
        foreach($commenters as $commenter){
          if($commenter['people_id'] == $loggedUser['pid']){
            continue;
          }
          $target_pid[] = "pid-" . $commenter['people_id'];
        }
        $queueParams = array(
          'channel_name' => array(
            'target_pid' => array_unique($target_pid),
            'pid' => "pid-". $loggedUser['pid'],
          ),
          'message' => $alertMessage,
          'data' => array(
            'pid' => $loggedUser['pid'],
            'sound' => 'default',
            'activity_type' => 'notif',
            'notification' => $mycomment['notification_id']
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));

        $comment = $this->app->notifComment->get($commentId);
        $comment = $this->getItem($comment, new NotificationCommentTransformer());
        $this->response->formatJson(200, 'Komentar berhasil ditambahkan', [$comment]);
        
      }
    }

    $this->response->formatJson(406, 'Komentar gagal ditambahkan');
  }

  public function indexComment($notificationId){
    $params = $this->app->request->params();
    $notification = $this->app->notif->get($notificationId);
    if (!$notification) { $this->response->formatJson(406, 'Notification tidak ditemukan'); }
    $comments = $this->app->notifComment->getByNotification($notification['id'], $params);
    $total = $this->app->notifComment->total($notificationId, $params);
    $comments = $this->getCollection($comments, new NotificationCommentTransformer());
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Notification comments', $comments);
  }

  public function deleteComment($notificationId, $commentId)
  {
    $notification = $this->app->notif->get($notificationId);
    if (!$notification) { $this->response->formatJson(406, 'Notification tidak ditemukan'); }
    $loggedUser = $this->app->loggedUser;
    $comment = $this->app->notifComment->get($commentId);
    if (!$comment) {
      $this->response->formatJson(404, 'Komentar tidak ditemukan');
    }
    if ($loggedUser['pid'] != $comment['people_id']) {
      $this->response->formatJson(404, 'Komentar tidak ditemukan');
    }
    if ($this->app->notifComment->delete($comment['id'])) {
      $this->response->formatJson(200, 'Komentar berhasil dihapus',$this->getCollection(array($comment), new NotificationCommentTransformer()));
    } else {
      $this->response->formatJson(200, 'Komentar gagal dihapus');
    }
  }

  public function getNotificationByRefAndType($ref,$type){
    $notification = $this->app->notification->getNotificationByRefAndType($ref,$type);
    $total = count($notification);
    $notification = $this->getCollection($notification, new NotificationTransformer());
    $this->response->setTotal($total);

    $this->response->formatJson(200, 'Activities', $activities);
  }

  private function getNotificationOr404($id) {
    $notification = $this->app->notification->get($id);
    if (!$notification) {
      return $this->response->formatJson(404, 'Notification tidak ditemukan');
    }
    return $notification;
  }


}
