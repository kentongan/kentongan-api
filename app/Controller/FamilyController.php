<?php

namespace App\Controller;

use App\Wrapper\Format;
use App\Wrapper\Misc;

class FamilyController {

  protected $app;
  private $format;
  private $params;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $fields = $this->app->user->fields;
    unset($fields['password']);
    $this->fields = $fields;
  }

  public function index($family_id) {
    $response = $this->response;
    if(!$family_id) $response->formatJson(200, 'Family id harus diisi');

    $params = $this->app->request->params();
    $params['family_id'] = $family_id;

    $total = $this->app->user->total($params);
    $users =  $this->app->user->index($params);

    $response->setTotal($total);
    $response->formatJson(200, 'Daftar Keluarga', $users);
  }

  public function familyType(){
    $total = $this->app->familytype->total();
    $familytype =  $this->app->familytype->index();
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Daftar Status Keluarga', $familytype);
  }

  public function create($family_id) {
    $response = $this->response;
    $params = $this->app->request->post();
    /*$requiredfield = array(
      'card_id',
      'family_id',
      'name',
      'address',
      'birth_date',
      'birth_place',
      'gender',
      'type',
      'status'
    );*/
    $fields = $this->fields;
    $fields['card_id']['format'] = 'required';
    $fields['family_id']['format'] = 'required';
    $fields['name']['format'] = 'required';
    $fields['address']['format'] = 'required';
    $fields['birth_date']['format'] = 'required';
    $fields['birth_place']['format'] = 'required';
    $fields['gender']['format'] = 'required';
    $fields['type']['format'] = 'required';
    $fields['status']['format'] = 'required';

    $errors = $this->appMisc->inputValidate($params, $fields);
    
    if(isset($userData['email'])) {
      if($this->app->user->get($userData['email'])) {
        $errors['email_exist']['validate'] = 'Email sudah terdaftar';
      }
    }

    if(count($errors)) {
      $message = array();
      foreach($errors as $key => $error) {
        foreach ($error as $m) {
          $message[$key][] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }
    $userData['role'] = 'wr';

    $code = 406;
    $user = array();


    if ($userData) {
      $userData['family_id'] = $family_id;
      $userId = $this->app->user->create($userData);
      if ($userId) {
        $user = $this->app->user->get($userId);
        $code = 200;
        $message = 'Anggota keluarga berhasil dibuat';
        $response->setTotal(1);
      } else {
        $response->setTotal(0);
        $message = 'Anggota keluarga gagal dibuat';
      }
    } else {
      $message = 'No Data Posted';
    }
    $response->formatJson($code, $message, $user);
  }
}
