<?php

namespace App\Controller;

use App\Fractal\FractalTrait;
use App\Model\Product;
use App\Transformer\ProductImageTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;

class ProductImageController
{
    use FractalTrait;

    protected $app;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->response = new Format();
        $this->appMisc = new Misc();
        $this->fields = $this->app->productImage->fields;
    }

    public function create($productId)
    {
        $user = $this->app->loggedUser;
        if (!$user) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $product = $this->app->product->get($productId);
        if (!$product) {
            return $this->response->formatJson(404, 'Produk tidak ditemukan');
        }
        if ($product['pid'] != $user['pid']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $imageParams = $this->app->request->post();
        if ($errors = $this->appMisc->inputValidate($imageParams, $this->fields)) {
            return $this->response->formatJson(406, 'Validation Error', $errors);
        }

        if ($imageId = $this->app->productImage->create($productId, $imageParams)) {
            $productImage = $this->app->productImage->get($imageId);
            $productImage = $this->getItem($productImage, new ProductImageTransformer());

            return $this->response->formatJson(200, 'Gambar berhasil ditambahkan', [$productImage]);
        }

        return $this->response->formatJson(400, 'Gambar gagal ditambahkan');
    }

    public function delete($id)
    {
        $user = $this->app->loggedUser;
        if (!$user) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $productImage = $this->app->productImage->get($id);
        if (!$productImage) {
            return $this->response->formatJson(404, 'Gambar tidak ditemukan');
        }

        $product = $this->app->product->get($productImage['product_id']);
        if (!$product) {
            return $this->response->formatJson(404, 'Gambar tidak ditemukan');
        }
        if ($product['pid'] != $user['pid']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        if ($this->app->productImage->delete($id)) {
            return $this->response->formatJson(200, 'Gambar berhasil dihapus', []);
        }

        return $this->response->formatJson(500, 'Gambar gagal dihapus');
    }

    public function update($id)
    {
        $user = $this->app->loggedUser;
        if (!$user) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $productImage = $this->app->productImage->get($id);
        if (!$productImage) {
            return $this->response->formatJson(404, 'Gambar tidak ditemukan');
        }

        $product = $this->app->product->get($productImage['product_id']);
        if (!$product) {
            return $this->response->formatJson(404, 'Gambar tidak ditemukan');
        }
        if ($product['pid'] != $user['pid']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $imageParams = $this->app->request->post();
        if ($errors = $this->appMisc->inputValidate($imageParams, $this->fields)) {
            return $this->response->formatJson(406, 'Validation Error', $errors);
        }

        if ($this->app->productImage->update($id, $imageParams)){
            $image = $this->app->productImage->get($id);
            $image = $this->getItem($image, new ProductImageTransformer());

            return $this->response->formatJson(200, 'Gambar berhasil diubah', [$image]);
        }

        return $this->response->formatJson(200, 'Gambar gagal diubah'); 
    }
}
