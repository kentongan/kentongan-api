<?php
  namespace App\Controller;
  use App\Wrapper\Format;
  use App\Fractal\FractalTrait;
  use App\Transformer\AppTransformer;

  class AppController {
  	use FractalTrait;
    protected $app;

    public function __construct() {
       $this->app = \Slim\Slim::getInstance();
       $this->response = new Format();
    }

    public function detail() {
      $response = $this->response;
      $appBuild = $this->app->application->get();
      $appBuild = array($this->getItem($appBuild, new AppTransformer()));
      $response->setTotal(1);

      $response->formatJson(200, 'Application build version', $appBuild);
    }

    public function update() {
    	$loggedUser = $this->app->loggedUser;
      $response = $this->response;
		  if ($loggedUser['role'] == 'admin') {
		    $data = $this->app->request->post();
		    $data = array_filter($data);
		    $allowed = array("android", "ios");
	    	$data = array_intersect_key($data, array_flip($allowed));
		    $appBuild = $this->app->application->get();
		    $error = FALSE;
		    $build = array();
		    foreach ($data as $name => $version) {
		    	if($version < $appBuild[$name]) {
		    		$error = TRUE;
		    		$build[$name] = array(
		    			'current' => (float) $appBuild[$name],
		    			'new' => (float) $version,
		    		);
		    	}
		    }
		    if ($error) {
		  		//$response->formatJson(406, 'Application build version is lower than current version', $build);
		    }
		    $updated = $this->app->application->update($data);
        
        if ($updated) {
        	$appBuild = $this->app->application->get();
        	$appBuild = array($this->getItem($appBuild, new AppTransformer()));
		  		$response->formatJson(200, 'Application build version updated', $appBuild);
        } else {
		  		$response->formatJson(406, 'Application build version failed to updated');
        }
		  } else {
		  	$response->formatJson(406, 'Access denied');
		  }
    }
	}