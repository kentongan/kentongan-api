<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 10:10
 */

namespace App\Controller;

use App\Transformer\DocumentRequestTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;

class DocumentRequestController {
  use FractalTrait;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $this->fields = $this->app->documentrequest->fields;
  }

  public function detail($id) {
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $documentrequest = $this->app->documentrequest->get($id);
    if ($documentrequest) {
      if($documentrequest['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
        $response->formatJson(406, 'Access denied');
      }
      if($loggedUser['role'] != 'rt' && $documentrequest['pid'] != $loggedUser['pid']) {
        $response->formatJson(406, 'Access denied');
      }
      $documentrequest = $this->getItem($documentrequest, new DocumentRequestTransformer());
      //$documentrequest = $response->formatDataOutput($this->fields, $documentrequest);
      $documentrequest = array($documentrequest);
      $response->setTotal(1);
      $code = 200;
      $message = 'Permintaan Dokumen ditemukan';
    } else {
      $documentrequest = array();
      $response->setTotal(0);
      $code = 406;
      $message = 'Permintaan Dokumen tidak ditemukan';
    }
    $response->formatJson($code, $message, $documentrequest);
  }

  public function index() {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $params = $this->app->request->params();
    $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
    if($loggedUser['role'] != 'rt') {
      $params['pid'] = $loggedUser['pid'];
    }

    $total = $this->app->documentrequest->total($params);
    $documentrequests =  $this->app->documentrequest->index($params);
    $documentrequests = $this->getCollection($documentrequests, new DocumentRequestTransformer());

    $response->setTotal($total);

    $response->formatJson(200, 'Daftar Permintaan Dokumen', $documentrequests);
  }

  public function create() {
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
    if($loggedUser['role'] != 'wr') $response->formatJson(406, 'Access denied');
    $data = $this->app->request->post();

    $fields = $this->fields;

    $code = 406;
    $documentrequest = array();

    if ($data) {
      $errors = $this->appMisc->inputValidate($data, $fields);
      if(strtotime($data['required_date']) < time()) {
        $errors['end_date']['validate'] = 'Kolom required_date tidak boleh lebih kecil daripada tanggal sekarang';
      }
      if(count($errors)) {
        $message = array();
        foreach($errors as $key => $error) {
          foreach ($error as $m) {
            $message[$key][] = $m;
          }
        }
        $response->formatJson(406, 'Validation Error', $message);
      }

      $data['sender_pid'] = $loggedUser['pid'];
      $data['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      $getdocumentrequest = $this->app->documenttype->get($data['dtid']);
      if(!$getdocumentrequest) {
        $response->formatJson(406, 'Tipe dokumen tidak ditemukan');
      } elseif ($getdocumentrequest['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
        $response->formatJson(406, 'Access Denied');
      }

      $date = $data['required_date'];
      $data['required_date'] = strtotime($data['required_date']);
      $drid = $this->app->documentrequest->create($data);
      if ($drid) {
        $get_ketua_rt = $this->app->user->index(array(
          'neighbourhood_id' => $loggedUser['neighbourhood_id'],
          'role' => 'rt',
        ));
        $ketua_rt = $get_ketua_rt[0];

        $documentName = $getdocumentrequest['name'];
        $alertMessage = $loggedUser['name']. " mengajukan permohonan ". $documentName. " untuk dipakai tanggal ". $date;
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $loggedUser['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid'],
            'target_pid' => 'pid-'. $ketua_rt['pid'],
          ),
          'message' => $alertMessage,
          'data' => array(
            'link' => "app/info_kegiatan.php?page=view&id=".$drid,
            'pid' => $ketua_rt['pid'],
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));

        $documentrequest = $this->app->documentrequest->get($drid);
        $documentrequest = array($this->getItem($documentrequest, new DocumentRequestTransformer()));
        $code = 200;
        $message = 'Permintaan Dokumen berhasil dibuat';
        $response->setTotal(1);
      } else {
        $response->setTotal(0);
        $message = 'Permintaan Dokumen gagal dibuat';
      }
    } else {
      $message = 'Tidak ada data yang dikirimkan';
    }
    $response->formatJson($code, $message, $documentrequest);
  }

  public function update($id) {
    $sender = new WorkerSender();
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;

    $data = $this->app->request->post();

    $fields = $this->fields;
    unset($fields['dtid']['format']);
    unset($fields['description']['format']);
    unset($fields['required_date']['format']);
    unset($fields['created']);
    $fields['status']['format'] = 'required';
    $errors = $this->appMisc->inputValidate($data, $fields);
    if(count($errors)) {
      $message = array();
      foreach($errors as $key => $error) {
        foreach ($error as $m) {
          $message[$key][] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }

    $code = 406;
    $documentrequest = array();
    $message = '';
    $currentData = $this->app->documentrequest->get($id);
    if (!$currentData) $response->formatJson(406, 'Permintaan Dokumen tidak ditemukan');
    if($loggedUser['role'] == 'wr') {
      $data = array(
        'description' => $data['description'],
        'required_date' => $data['required_date'],
      );
      unset($data['response']);
      if($currentData['sender_pid'] != $loggedUser['pid']) $response->formatJson(406, 'Access denied');
    }
    if($loggedUser['role'] == 'rt') {
      $data = array(
        'status' => $data['status'],
        'response' => $data['response'],
      );
      $getdocumentrequest = $this->app->documenttype->get($currentData['dtid']);
      $documentName = $getdocumentrequest['name'];
      if ($data['status'] == 1) {
        $alertMessage = 'Pengajuan '. $documentName. ' Anda telah selesai, mohon diambil di Ketua RT';
      }
      if ($data['status'] == 0) {
        $alertMessage = 'Pengajuan '. $documentName. ' Anda ditolak karena '. $data['response'];
      }
      if($currentData['neighbourhood_id'] != $loggedUser['neighbourhood_id']) $response->formatJson(406, 'Access denied');
    }

    if ($data) {
      unset($data['sender_pid']);
      $updated = $this->app->documentrequest->update($id, $data);
      if ($updated) {
        if($loggedUser['role'] == 'rt') {
          $queueParams = array(
            'channel_name' => array(
              'rt' => "rt-". $loggedUser['neighbourhood_id'],
              'pid' => 'pid-'. $loggedUser['pid'],
              'target_pid' => 'pid-'. $ketua_rt['pid'],
            ),
            'message' => $alertMessage,
            'data' => array(
              'link' => "app/info_kegiatan.php?page=view&id=".$drid,
              'pid' => $ketua_rt['pid'],
            )
          );
          $queueParams['mode'] = $this->app->config('app.mode');
          $sender->execute('alertNotification', json_encode($queueParams));
        }

        $response->setTotal(1);
        $documentrequest = $this->app->documentrequest->get($id);
        $documentrequest = array($this->getItem($documentrequest, new DocumentRequestTransformer()));
        $code = 200;
        $message = 'Permintaan Dokumen berhasil diubah';
      } else {
        $response->setTotal(0);
        $message = 'Gagal mengubah Permintaan Dokumen';
      }
    } else {
      $message = 'No Data Posted';
    }

    $response->formatJson($code, $message, $documentrequest);
  }

  public function delete($id) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    $code = 406;
    $documentrequest = array();
    $currentData = $this->app->documentrequest->get($id);
    if (!$currentData) $response->formatJson(406, 'Permintaan Dokumen tidak ditemukan');

    if($currentData['pid'] != $loggedUser['pid']) $response->formatJson(406, 'Access denied');

    $message = 'Permintaan Dokumen gagal dihapus';
    $deleted = $this->app->documentrequest->delete($id);
    if ($deleted) {
      $code = 200;
      $message = 'Permintaan Dokumen berhasil dihapus';
    }
    $response->formatJson($code, $message, $documentrequest);
  }
}