<?php
namespace App\Controller;
use App\Wrapper\Format;

class HelloController
{
    protected $app;

    public function __construct() {
        $this->response = new Format();
        $this->app = \Slim\Slim::getInstance();
    }

    public function sayHello() {
        $response = $this->response;
        $response->formatJson(200, 'Hello Kentongan!');
    }
}