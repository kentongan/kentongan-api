<?php
  namespace App\Controller;

  use App\Transformer\UserTransformer;
  use App\Wrapper\Format;
  use App\Wrapper\Misc;
  use App\Notification\Notification;
  use \Firebase\JWT\JWT;
  use App\Fractal\FractalTrait;
  use App\AmqpWrapper\WorkerSender;

  class UserController {
    use FractalTrait;
    protected $app;
    private $format;


    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->response = new Format();
      $this->appMisc = new Misc();
      $this->notif = new Notification();
      $fields = $this->app->user->fields;
      unset($fields['password']);
      $this->fields = $fields;
    }

    public function index() {
      $loggedUser = $this->app->loggedUser;
      $response = $this->response;
      $params = $this->app->request->params();
      if($loggedUser['role'] != 'admin') {
        if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
        $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      }

      $total = $this->app->user->total($params);
      $users =  $this->app->user->index($params);
      $users = $this->getCollection($users, new UserTransformer());

      $response->setTotal($total);

      $response->formatJson(200, 'Daftar user', $users);
    }

    public function login() {
      $response = $this->response;
      $username = $this->app->request->post('username');
      $password = $this->app->request->post('password');
      $user = $this->app->user->login($username, $password);
      if ($user && !$user['status']) {
        $response->formatJson(406, 'User belum aktif');
      }
      $code = 200;
      if (!count($user)) {
        $code = 406;
        $user = array();
        $response->setTotal(0);
        $message = 'Login gagal';
      } else {
        $response->setTotal(1);
        //$jwt = JWT::encode($user, $this->app->config('app.key'));
        $data = array(
          'pid' => $user['pid'],
          'email' => $user['email'],
        );
        $jwt = JWT::encode($data, $this->app->config('app.key'));
        $this->app->user->update($user['pid'], array('updated' => time()));
        $user = $this->app->user->get($user['pid']);
        $user = $this->getItem($user, new UserTransformer());
        //$user = $response->formatDataOutput($this->fields, $user);
        $user['token'] = $jwt;
        $user = array($user);
        $message = 'Login sukses';
      }
      $response->formatJson($code, $message, $user);
    }

    public function detail($userId) {
      $loggedUser = $this->app->loggedUser;
      $response = $this->response;
      $user = $this->app->user->get($userId);
      if(strtolower($loggedUser['role']) != 'rt' && $loggedUser['family_id'] != $user['family_id']) {
        $response->formatJson(406, 'Access denied');
      }
      if ($user) {
        if ($user['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
          $response->formatJson(406, 'Access denied');
        }
        $user = $this->getItem($user, new UserTransformer());
        //$user = $response->formatDataOutput($this->fields, $user);

        $user = array($user);
        $response->setTotal(1);
        $code = 200;
        $message = 'User ditemukan';
      } else {
        $user = array();
        $response->setTotal(0);
        $code = 406;
        $message = 'User tidak ditemukan';
      }
      $response->formatJson($code, $message, $user);
    }

    public function update($userId) {
      $sender = new WorkerSender();
      $loggedUser = $this->app->loggedUser;
      $response = $this->response;

      $code = 406;
      $user = array();

      if ($loggedUser['pid'] == $userId
        || strtolower($loggedUser['role']) == 'rt'
        || $loggedUser['family_type'] == 'KK')
      {
        $userData = $this->app->request->post();
        if( empty($userData['password']) ){
          unset($userData['password']);
        }
        $fields = $this->fields;
        unset($fields['created']);
        unset($fields['status']);
        unset($fields['neighbourhood_id']);

        if(isset($userData['email']) && $userData['email'] == ''){
          unset($userData['email']);
        }
        if(isset($userData['phone']) && $userData['phone'] == ''){
          unset($userData['phone']);
        }

        if(isset($userData['phone']) && ($userData['phone']) && substr($userData['phone'], 0, 1) == "0" ){
          $userData['phone'] = "+62" . substr($userData['phone'],1);
        }

        $errors = $this->appMisc->inputValidate($userData, $fields);
        if(isset($userData['email']) && $userData['email']) {
          $checkUser = $this->app->user->get($userData['email']);
          if ($checkUser && $checkUser['pid'] != $userId) {
            $errors['email_exist']['validate'] = 'Email sudah terdaftar di user lain';
          }
        }
        if(isset($userData['phone']) && $userData['phone']) {
          $checkUser = $this->app->user->get($userData['phone']);
          if ($checkUser && $checkUser['pid'] != $userId) {
            $errors['phone_exist']['validate'] = 'Nomor telepon sudah terdaftar di user lain';
          }
        }

        if(count($errors)) {
          $message = array();
          foreach($errors as $key => $error) {
            foreach ($error as $m) {
              $message[$key][] = $m;
            }
          }
          $response->formatJson(406, 'Validation Error', $message);
        }
        $currentUser = $this->app->user->get($userId);
        if (!$currentUser) $response->formatJson(406, 'User tidak ditemukan');


        if ($userData || $_FILES) {
          if(isset($userData['password']) && $userData['password']) {
            if($this->app->config('app.mode') == 'demo') $userData['password'] = 'demo_'. $userData['password'];
          }
          //if user is not active and updated data contain phone or email, then send invitation
          $sendMail = FALSE;
          $sendPhone = FALSE;

          // If new status is KK then give the new KK number
          if($currentUser['family_type'] != "KK"){
            if($userData['family_type'] == "KK"){
              $userData['family_id'] = $this->app->user->getFamilyId();
            }
          }

          if (!$currentUser['status']) {

            if (!empty($userData['phone']) && !empty($userData['email'])) {
              $userData['status'] = 1;
              $sendMail = TRUE;
              $sendPhone = TRUE;

              $phone = $userData['phone'];
              $password = substr(md5($phone), 5, 6);

              $inviteParams['phone'] = $phone;
              $inviteParams['password'] = $password;

              $email = $userData['email'];
              $inviteParams['email'] = $email;
              $inviteParams['phone'] = $phone;
              $userData['password'] = $password;
            } else {
              if (!empty($userData['phone'])) {
                $userData['status'] = 1;

                $sendPhone = TRUE;
                $phone = $userData['phone'];
                $password = substr(md5($phone), 5, 6);
                $inviteParams['password'] = $password;
                $inviteParams['phone'] = $phone;
                $userData['password'] = $password;
              } else if (!empty($userData['email'])) {
                $userData['status'] = 1;
                $sendMail = TRUE;
                $email = $userData['email'];
                $password = substr(md5($email), 5, 6);
                $inviteParams['password'] = $password;
                $inviteParams['email'] = $email;
                $userData['password'] = $password;
              }
            }
          }

          if( empty($userData['phone']) && empty($currentUser['phone']) && empty($userData['email']) && empty($currentUser['email']) ) {
            $userData['status'] = 0;
            $sendMail = FALSE;
            $sendPhone = FALSE;
          }
          $updated = $this->app->user->update($userId, $userData);
          if ($updated) {
            $message = 'Data warga berhasil diubah';
            if($sendMail == TRUE) {
              $queueParams = array(
                'type' => 'mail',
                'template' => 'registerMailNotification',
                'params' => $inviteParams,
              );
              $sender->execute('userNotification', json_encode($queueParams));
              $message = 'Data warga berhasil diubah, undangan dikirim melalui email';
            }
            if ($sendPhone == TRUE) {
              $queueParams = array(
                'type' => 'mobile',
                'template' => 'registerMobileNotification',
                'params' => $inviteParams,
              );
              $sender->execute('userNotification', json_encode($queueParams));
              $message = 'Data warga berhasil diubah, undangan dikirim melalui SMS';
            }
            if ($sendPhone && $sendMail) {
              $message = 'Data warga berhasil diubah, undangan dikirim melalui email dan SMS';
            }
            if( empty($userData['phone']) && empty($currentUser['phone']) && empty($userData['email']) && empty($currentUser['email']) ) {
              $message = 'Data warga berhasil diubah, tetapi status menjadi tidak aktif karena tidak ada data email atau telepon';
            }

            if(isset($userData['password']) && md5($userData['password']) != $currentUser['password'] ){
              $updateParams = array(
                'phone' => $currentUser['phone'],
                'password' => $userData['password']
              );
              $message = 'Password Anda berhasil diubah';
              $queueParams = array(
                'type' => 'mobile',
                'template' => 'changeMobileNotification',
                'params' => $updateParams,
              );
              $sender->execute('userNotification', json_encode($queueParams));
            }

            $response->setTotal(1);
            $user = $this->app->user->get($userId);
            //$user = array($response->formatDataOutput($this->fields, $user));
            $user = array($this->getItem($user, new UserTransformer()));
            $code = 200;

          } else {
            $response->setTotal(0);
            $message = 'Data warga gagal diubah';
          }
        } else {
          $message = 'Tidak ada data yang dikirimkan';
        }
      } else {
        $message = 'Access denied';
      }

      $response->formatJson($code, $message, $user);
    }

    public function create() {
      $sender = new WorkerSender();
      $response = $this->response;
      $userData = $this->app->request->post();
      $loggedUser = $this->app->loggedUser;

      $code = 406;
      $user = array();
      $inviteParams = array();
      $userActive = TRUE;
      if ($userData) {
        $fields = $this->fields;
        $fields['name']['format'] = 'required';
        if((isset($userData['role']) && $userData['role'] == 'rt') || (!isset($userData['role']) || empty($userData['role']))) {
          $fields['neighbourhood_id']['format'] = 'required';
        }
        if(isset($userData['role']) && $userData['role'] == 'wr') {
          unset($fields['email']['format']);
          if(!isset($userData['email']) || !$userData['email']) {
            unset($fields['email']);
          }
        }

        if(!isset($userData['family_id'])){
          $userData['family_id'] = $this->app->user->getFamilyId();
        }

        $errors = $this->appMisc->inputValidate($userData, $fields);
        if(isset($userData['email'])) {
          if($this->app->user->get($userData['email'])) {
            $errors['email_exist']['validate'] = 'Email sudah terdaftar';
          }
        }

        if(isset($userData['phone'])) {
          if($this->app->user->get($userData['phone'])) {
            $errors['phone_exist']['validate'] = 'Nomor telepon sudah terdaftar';
          }
        }

        if(count($errors)) {
          $message = array();
          foreach($errors as $key => $error) {
            foreach ($error as $m) {
              $message[$key][] = $m;
            }
          }
          $response->formatJson(406, 'Validation Error', $message);
        }
        if (!isset($userData['phone'])) {
          $userData['phone'] = '';
        }
        if (!isset($userData['email'])) {
          $userData['email'] = '';
        }
        if(isset($userData['phone']) && ($userData['phone']) && substr($userData['phone'], 0, 1) == "0" ){
          $userData['phone'] = "+62" . substr($userData['phone'],1);
        }
        if((!isset($userData['phone']) || empty($userData['phone'])) && (!isset($userData['email']) || empty($userData['email']))) {
          $userData['status'] = 0;
          $userActive = FALSE;
        }
        $sendMail = FALSE;
        $sendPhone = FALSE;
        if (!empty($userData['phone']) && !empty($userData['email']) && $userActive) {
          $sendMail = TRUE;
          $sendPhone = TRUE;

          $phone = $userData['phone'];
          $password = substr(md5($phone), 5, 6);

          $inviteParams['phone'] = $phone;
          $inviteParams['password'] = $password;
          // $inviteParams['rt_name'] = $loggedUser['name'];

          $email = $userData['email'];
          $inviteParams['email'] = $email;
          $inviteParams['phone'] = $phone;

          $code = 200;
          $message = 'Warga berhasil ditambahkan, undangan dikirim melalui email dan SMS';
        } else {
          if (!empty($userData['phone'])) {
            $sendPhone = TRUE;
            $phone = $userData['phone'];
            $password = substr(md5($phone), 5, 6);
            $inviteParams['password'] = $password;
            $inviteParams['phone'] = $phone;

            $code = 200;
            $message = 'Warga berhasil ditambahkan, undangan dikirim melalui SMS';
          } else if (!empty($userData['email'])) {
            $sendMail = TRUE;
            $email = $userData['email'];
            $password = substr(md5($email), 5, 6);
            $inviteParams['password'] = $password;
            $inviteParams['email'] = $email;

            $code = 200;
            $message = 'Warga berhasil ditambahkan, undangan dikirim melalui email';
          }
        }

        if ($userActive) {
          if($this->app->config('app.mode') == 'demo') $password = 'demo_'. $password;
          $inviteParams['password'] = $password;

          if($sendMail == TRUE) {
            $queueParams = array(
              'type' => 'mail',
              'template' => 'registerMailNotification',
              'params' => $inviteParams,
            );
            $sender->execute('userNotification', json_encode($queueParams));
          }
          if ($sendPhone == TRUE) {
            $queueParams = array(
              'type' => 'mobile',
              'template' => 'registerMobileNotification',
              'params' => $inviteParams,
            );
            $sender->execute('userNotification', json_encode($queueParams));
          }

          $userData['password'] = $password;
        } else {
          $message = 'User tidak aktif';
        }


        $userId = $this->app->user->create($userData);
        if ($userId) {
          $activity = $this->app->activity->getActivityByRefAndType($userId,'people');
          if(!$userActive) {
            $code = 200;
            $message = 'Warga berhasil ditambahkan, tetapi status tidak aktif. Silakan update email atau nomor telepon user untuk mengaktifkan';
          }
          // $user = $this->getItem($user, new UserTransformer());
          //$user = $response->formatDataOutput($this->fields, $user);

          /*
          $message = $loggedUser['name'] . " menambah warga baru : " . $userData['name'];
          $queueParams = array(
            'channel_name' => array(
              'rt' => "rt-". $alertData['neighbourhood_id'],
              'pid' => 'pid-'. $loggedUser['pid']
            ),
            'message' => $message,
            'data' => array(
              'link' => "app/detail_rt.php",
              'pid' => $loggedUser['pid'],
              'activity' => $activity['id'],
              'activity_type' => 'people',
              'sound' => 'kentongan.caf'
            )
          );
          $queueParams['mode'] = $this->app->config('app.mode');
          $sender->execute('alertNotification', json_encode($queueParams));
          */

          $inviteParams = array();
          $user = $this->app->user->get($userId);
          $data = array(
            'pid' => $userId,
            'email' => $userData['email'],
          );
          $user = $this->getItem($user, new UserTransformer());
          $jwt = JWT::encode($data, $this->app->config('app.key'));
          $user['token'] = $jwt;

          $user = array($user);
          //$message = 'Warga berhasil ditambahkan';
          $response->setTotal(1);
        } else {
          $response->setTotal(0);
          $message = 'User gagal dibuat';
        }
      } else {
        $message = 'Tidak ada data yang dikirimkan';
      }
      $response->formatJson($code, $message, $user);
    }

    public function invite() {
      $sender = new WorkerSender();
      $response = $this->response;
      $loggedUser = $this->app->loggedUser;

      // if ($loggedUser['role'] != 'rt') {
      //   $response->formatJson(406, 'Access denied');
      // }

      $nid = $loggedUser['neighbourhood_id'];
      if (!$nid) {
        $response->formatJson(406, 'Anda belum terdaftar di suatu RT');;
      }

      $rtData = $this->app->rt->get($nid);
      $inviteParams = array(
        'rt' => $rtData['rt'],
        'rw' => $rtData['rw'],
        'village' => $rtData['village'],
        'rt_name' => $loggedUser['name'],
        'role' => $loggedUser['role'],
      );
      $email = $this->app->request->post('email');
      $output = array();
      if ($email) {
        $email = str_replace(" ", "", $email);
        $emails = explode(",",$email);

        foreach ($emails as $email) {
          $invitationMessage = '';
          $valid = TRUE;
          $email = trim($email);
          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $invitationMessage = 'Email tidak valid';
            $valid = FALSE;
          } else {
            $getUserData = $this->app->user->get($email);
            if ($getUserData && $this->app->config('app.mode') != 'demo') {
              if ($getUserData['neighbourhood_id'] != $nid) {
                $invitationMessage = 'Email sudah terdaftar di RT lain';
                $valid = FALSE;
              } else {
                //$valid = TRUE;
              }
            }
          }
          if ($valid) {
            $password = substr(md5($email), 5, 6);
            if($this->app->config('app.mode') == 'demo') $password = 'demo_'. $password;
            //$password = md5($token);
            $inviteParams['email'] = $email;
            $inviteParams['password'] = $password;
            $inviteParams['rt_name'] = $loggedUser['name'];
            $inviteParams['role'] = $loggedUser['role'];

            $queueParams = array(
              'type' => 'mail',
              'template' => 'inviteMailNotification',
              'params' => $inviteParams,
            );
            $sender->execute('userNotification', json_encode($queueParams));
            //if($emailSend){
              $loggedUser = $this->app->loggedUser;
              $invitationMessage = 'Undangan sukses terkirim';
              $userData = array(
                'email' => $email,
                'password' => $password,
                'neighbourhood_id' => $rtData['nid'],
                'role' => "wr",
                'family_type' => 'KK',
                'status' => '1',
                'family_id' => $this->app->user->getFamilyId(),
                'people_reff' => $loggedUser['pid']
              );
            if($getUserData) {
              $userId = $this->app->user->update($getUserData['pid'], array('password' => $password));
            } else {
              $userId = $this->app->user->create($userData);
            }
            //}
          }

          $output[] = array(
            'email' => $email,
            'status' => $valid,
            'message' => $invitationMessage,
          );
        }
      }

      $phone = $this->app->request->post('phone');
      $storedPhone = array();
      if ($phone) {
        $phones = explode(",",$phone);
        foreach($phones as $phone){
          $phoneOrigin = $phone;
          $valid = TRUE;
          // if(!trim($phone)){
          //   continue;
          // }
          $phone = trim($phone);
          if( substr($phone,0,3) == "+62" ){
            $phone = "0" . substr($phone,3);
            $phone = str_replace(" ", "", $phone);
          }
          $phone = preg_replace('/(\+62\ ?)/', '0', $phone);
          $phone = str_replace("-", "", $phone);
          $phone = str_replace("'", "", $phone);
          $phone = str_replace('"', "", $phone);
          
          if(is_numeric($phone)) {
            $name = '';
            $valid = TRUE;
          } else {
            preg_match('/([a-zA-Z0-9 ]+) \((\ *\+?[-0-9]*)\ *\)/', $phone, $matches);
            if (count($matches) < 3) {
              $valid = FALSE;
            } else {
              $valid = TRUE;
              $phone = $matches[2];
              $name = $matches[1];
            }
          }
          $phone = trim($phone);
          $phone = str_replace(" ", "", $phone);
          $phone = str_replace("-", "", $phone);
          if (!isset($storedPhone[$phone])) {
            $storedPhone[$phone] = $phone;
          } else {
            continue;
          }

          $invitationMessage = '';

          if( substr($phone,0,1) == "0" ){
            $phone = "+62" . substr($phone,1);
          }
          if(!preg_match("/^(\+\d{1,3}[- ]?)?\d{9,13}$/", $phone)) {
            $invitationMessage = 'Nomor telepon tidak valid';
            $valid = FALSE;
          } else {
            if( substr($phone,0,1) == "0" ){
              $phone = "+62" . substr($phone,1);
            }
            $getUserData = $this->app->user->get($phone);
            if ($getUserData && $this->app->config('app.mode') != 'demo') {
              if ($getUserData['neighbourhood_id'] != $nid) {
                $invitationMessage = 'No. telepon sudah terdaftar di RT lain';
                $valid = FALSE;
              } else {
                //$valid = TRUE;
              }
            }
          }

          if(substr($phone,0,1) == "0"){
            $phone = "+62" . substr($phone,1);
          }

          if ($valid) {
            $password = substr(md5($phone), 5, 6);
            if($this->app->config('app.mode') == 'demo') $password = 'demo_'. $password;
            $inviteParams['phone'] = $phone;
            $inviteParams['password'] = $password;

            $queueParams = array(
              'type' => 'mobile',
              'template' => 'inviteMobileNotification',
              'params' => $inviteParams,
            );
            $sender->execute('userNotification', json_encode($queueParams));
            //if ($phoneSend->sid) {
              $invitationMessage = 'Undangan sukses terkirim';
            if ($getUserData) {
              $userData = array('name' => $name, 'password' => $password);
              if($this->app->config('app.mode') == 'demo') {
                $userData['neighbourhood_id'] = $rtData['nid'];
                $userData['role'] = 'wr';
              }
              $userId = $this->app->user->update($getUserData['pid'], $userData);
            } else {
              $loggedUser = $this->app->loggedUser;
              $userId = $this->app->user->create(array(
                'name' => $name,
                'phone' => $phone,
                'password' => $password,
                'neighbourhood_id' => $rtData['nid'],
                'role' => "wr",
                'family_type' => 'KK',
                'status' => '1',
                'family_id' => $this->app->user->getFamilyId(),
                'people_reff' => $loggedUser['pid']
              ));
              
              /*
              $activity = $this->app->activity->getActivityByRefAndType($userId,'people');
              $message = $loggedUser['name'] . " menambah warga baru : " . $name;
              $queueParams = array(
                'channel_name' => array(
                  'rt' => "rt-". $alertData['neighbourhood_id'],
                  'pid' => 'pid-'. $loggedUser['pid']
                ),
                'message' => $message,
                'data' => array(
                  'link' => "app/detail_rt.php",
                  'pid' => $loggedUser['pid'],
                  'activity' => $activity['id'],
                  'activity_type' => 'people',
                  'sound' => 'default'
                )
              );
              $queueParams['mode'] = $this->app->config('app.mode');
              $sender->execute('alertNotification', json_encode($queueParams));
              */

            }
          }
          //}
          $output[] = array(
            'phone' => $phoneOrigin,
            'status' => $valid,
            'message' => $invitationMessage,
          );
        }
      }

      $response->setTotal(count($output));
      $response->formatJson(200, '', $output);
    }

    public function activate($userId) {
      $sender = new WorkerSender();

      $response = $this->response;
      $loggedUser = $this->app->loggedUser;
      if($loggedUser['role'] != 'rt') $response->formatJson(406, 'Access denied');
      $code = 406;
      $message = 'Access denied';
      $user = $this->app->user->get($userId);
      if ($user) {
        if ($user['status']) {
          $response->formatJson(406, 'User sudah diaktifkan');
        }
        if($loggedUser['role'] == 'rt' && $loggedUser['neighbourhood_id'] == $user['neighbourhood_id']) {
          if (empty($user['email']) && empty($user['phone'])) {
            $code = 406;
            $message = 'Email atau nomor telepon user tersebut harus diisi';
          } else {
            $updated = $this->app->user->activate($userId);
            $sendMail = FALSE;
            $sendPhone = FALSE;
            if ($updated) {
              $nid = $loggedUser['neighbourhood_id'];
              $rtData = $this->app->rt->get($nid);
              $inviteParams = array(
                'rt' => $rtData['rt'],
                'rw' => $rtData['rw'],
                'village' => $rtData['village'],
              );
              if (!empty($user['phone']) && !empty($user['email'])) {
                $sendPhone = TRUE;
                $sendMail = TRUE;

                $phone = $user['phone'];
                $password = substr(md5($phone), 5, 6);
                $inviteParams['phone'] = $phone;
                $inviteParams['password'] = $password;

                $email = $user['email'];
                $inviteParams['phone'] = $phone;
                $inviteParams['email'] = $email;

                $code = 200;
                $response->setTotal(1);
                $message = 'User berhasil diaktifkan, undangan dikirim melalui email dan SMS';
              } else {
                if (!empty($user['phone'])) {
                  $sendPhone = TRUE;

                  $phone = $user['phone'];
                  $password = substr(md5($phone), 5, 6);
                  $inviteParams['password'] = $password;

                  $code = 200;
                  $response->setTotal(1);
                  $message = 'User berhasil diaktifkan, undangan dikirim melalui SMS';
                } else if (!empty($user['email'])) {
                  $sendMail = TRUE;
                  $email = $user['email'];
                  $password = substr(md5($email), 5, 6);
                  $inviteParams['password'] = $password;
                  $inviteParams['email'] = $email;

                  $code = 200;
                  $response->setTotal(1);
                  $message = 'User berhasil diaktifkan, undangan dikirim melalui email';
                }
              }
              if($this->app->config('app.mode') == 'demo') $password = 'demo_'. $password;
              $inviteParams['password'] = $password;
              $userData['password'] = $password;
              $this->app->user->update($userId, $userData);

              if($sendMail == TRUE) {
                $queueParams = array(
                  'type' => 'mail',
                  'template' => 'inviteMailNotification',
                  'params' => $inviteParams,
                );
                $sender->execute('userNotification', json_encode($queueParams));
              }
              if ($sendPhone == TRUE) {
                $queueParams = array(
                  'type' => 'mobile',
                  'template' => 'inviteMobileNotification',
                  'params' => $inviteParams,
                );
                $sender->execute('userNotification', json_encode($queueParams));
              }

              $user = $this->app->user->get($userId);
            }
          }
        } else {
          $message = 'Access denied';
        }
        //$user = $response->formatDataOutput($this->fields, $user);
        $user = $this->getItem($user, new UserTransformer());
        $user = array($user);
      } else {
        $message = 'User tidak ditemukan';
      }

      $response->formatJson($code, $message, $user);
    }

    public function delete($userId){
      $loggedUser = $this->app->loggedUser;
      $response = $this->response;

      $currentData = $this->app->user->get($userId);
      if (!$currentData) $response->formatJson(406, 'User tidak ditemukan');

      if ($loggedUser['pid'] == $userId) {
        if($loggedUser['neighbourhood_id']) {
          $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
          //$params['role'] = 'rt';
          $neighbourhoodPeople = $this->app->user->index($params);
          $otherRTFound = FALSE;
          foreach ($neighbourhoodPeople as $key => $people) {
            if ($people['pid'] == $loggedUser['pid']) {
              unset($neighbourhoodPeople[$key]);
              continue;
            }
            if ($people['role'] == 'rt') $otherRTFound = TRUE;
          }
          if (count($neighbourhoodPeople) > 0 && !$otherRTFound) $response->formatJson(406, 'Masih ada warga lain di RT Anda, silakan angkat RT baru');
        }
      } else {
        if ($currentData['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
          $response->formatJson(406, 'Access denied');
        }
        if (!$loggedUser['role'] == 'rt') $response->formatJson(406, 'Access denied');
      }
      $code = 406;
      $report = array();
      $message = 'User gagal dihapus';
      $deleted = $this->app->user->delete($userId);
      if ($deleted) {
        $code = 200;
        $message = 'User berhasil dihapus';
      }
      $response->formatJson($code, $message, $report);
    }

    public function check($userId){
      $check = $this->app->user->check($userId);
      $this->response->formatJson(200, "Check User", $check);
    }

    public function reset() {
      $sender = new WorkerSender();
      $userData = $this->app->request->post();
      if(!isset($userData['username']) || !$userData['username']) $this->response->formatJson(406, 'Tidak ada data email atau nomor telepon yang dikirim');

      $username = $userData['username'];
      $user = $this->app->user->get($username);

      $code = 406;
      if ($user) {
        if(!$user['status']) $this->response->formatJson(406, "User belum aktif");
        //$reset = $this->app->user->reset($user['pid']);


        if (empty($user['email']) && empty($user['phone'])) {
          $code = 406;
          $message = 'Email atau nomor telepon user dimaksud belum diisi';
        } else {
          $nid = $user['neighbourhood_id'];
          $rtData = $this->app->rt->get($nid);
          $params = array(
            'rt' => $rtData['rt'],
            'rw' => $rtData['rw'],
            'village' => $rtData['village'],
          );
          $sendMail = FALSE;
          $sendPhone = FALSE;

          if (!empty($user['phone']) && !empty($user['email'])) {
            $sendMail = TRUE;
            $sendPhone = TRUE;

            $phone = $user['phone'];
            $password = substr(md5($phone), 5, 6);
            $params['phone'] = $phone;
            $params['password'] = $password;

            $email = $user['email'];
            $params['phone'] = $phone;
            $params['email'] = $email;

            $code = 200;
            $this->response->setTotal(1);
            $message = 'User berhasil direset, info login dikirim melalui email dan SMS';
          } else {
            if (!empty($user['phone'])) {
              $sendPhone = TRUE;
              $phone = $user['phone'];
              $password = substr(md5($phone), 5, 6);
              $params['password'] = $password;
              $params['phone'] = $phone;

              $code = 200;
              $this->response->setTotal(1);
              $message = 'User berhasil direset, info login dikirim melalui SMS';
            } else if (!empty($user['email'])) {
              $sendMail = TRUE;
              $email = $user['email'];
              $password = substr(md5($email), 5, 6);
              $params['password'] = $password;
              $params['email'] = $email;

              $code = 200;
              $this->response->setTotal(1);
              $message = 'User berhasil direset, info login dikirim melalui email';
            }
          }
          if($this->app->config('app.mode') == 'demo') $password = 'demo_'. $password;
          $params['password'] = $password;

          $reset = $this->app->user->reset($user['pid'], $password);
          if (!$reset) $this->response->formatJson(200, "Reset user gagal");
          if ($reset) {
            if($sendMail == TRUE) {
              $queueParams = array(
                'type' => 'mail',
                'template' => 'resetMailNotification',
                'params' => $params,
              );
              $sender->execute('userNotification', json_encode($queueParams));
            }
            if ($sendPhone == TRUE) {
              $queueParams = array(
                'type' => 'mobile',
                'template' => 'resetMobileNotification',
                'params' => $params,
              );
              $sender->execute('userNotification', json_encode($queueParams));
            }
          }

          $user = $this->app->user->get($username);
        }
        $user = $this->getItem($user, new UserTransformer());
        //$user = $this->response->formatDataOutput($this->fields, $user);
        $user = array($user);
      } else {
        $user = array();
        $message = 'User tidak ditemukan';
      }
      $this->response->formatJson($code, $message, $user);
    }

    public function invitor(){
      $sender = new WorkerSender();
      $userData = $this->app->request->post();

      $valid = TRUE;
      $code = 200;

      $invitationMessage = "Undangan berhasil dikirimkan ke " . $userData["rt_name"];
      foreach($userData as $key => $value){
        if($value == ""){
          $invitationMessage = "Kolom " . $key . " harus diisi";
        }
      }
      $phone = $userData['rt_phone'];
      if( substr($phone,0,1) == "0" ){
        $phone = "+62" . substr($phone,1);
      }
      /*$phoneCheck = $this->app->user->get($phone);
      if ($phoneCheck) {
        $invitationMessage = 'No. Telepon sudah terdaftar di Kentongan';
        $valid = FALSE;
      }*/
      if(!preg_match("/^(\+\d{1,3}[- ]?)?\d{11,13}$/", $phone)) {
        $invitationMessage = 'Nomor telepon tidak valid';
        $valid = FALSE;
      }

      if($valid){
        // Insert into invitor
        $insert = $this->app->user->insertInvitor($userData);
        if(!$insert){
          $code = 406;
          $userData = array();
        }
        else {
          // Send SMS
          $phoneParams = array(
            'phone' => $phone,
            'rt_name' => $userData["rt_name"],
            'name' => $userData['name'],
            //'message' => $message,
          );
          $queueParams = array(
            'type' => 'mobile',
            'template' => 'invitorMobileNotification',
            'params' => $phoneParams,
          );
          $sender->execute('userNotification', json_encode($queueParams));
        }
      }
      else {
        $code = 406;
        $userData = array();
      }
      $this->response->formatJson($code, $invitationMessage, $userData);
    }

    function invitor_share(){
      $sender = new WorkerSender();
      $loggedUser = $this->app->loggedUser;
      $phone = $this->app->request->post('phone');

      if(!isset($loggedUser['pid'])){
        $this->response->formatJson(406, 'Access denid.');
      }

      $code = 200;
      $status = TRUE;
      $partialError = FALSE;
      $invitationMessage = array();

      if ($phone) {
        $phones = explode(",",$phone);
        foreach($phones as $phone){
          $valid = TRUE;
          $name = '';
          
          if(!trim($phone)){
            continue;
          } 
          if( substr($phone,0,3) == "+62" ){
            $phone = "0" . substr($phone,3);
            $phone = str_replace(" ", "", $phone);
            $phone = str_replace("-", "", $phone);
          }
          $phone = preg_replace('/(\+62\ ?)/', '0', $phone);
          $phone = str_replace("-", "", $phone);
          $phone = str_replace("'", "", $phone);
          $phone = str_replace('"', "", $phone);
          $phoneOrigin = $phone;
          if(!is_numeric($phone)) {
            preg_match('/([a-zA-Z0-9 ]+) \((\ *\+?[-0-9]*)\ *\)/', $phone, $matches);
            if (count($matches) < 3) {
              $valid = FALSE;
              $invitationMessage[] = array(
                'phone' => $phone,
                'status' => FALSE,
                'message' => "Nomor tidak valid",
              );
              $partialError = TRUE;
            } else {
              $phone = $matches[2];
              $name = $matches[1];
            }
          }
          else {
            $phone = str_replace(" ", "", $phone);
            if(!preg_match("/^(\+\d{1,3}[- ]?)?\d{11,13}$/", $phone)) {
              $invitationMessage[] = array(
                'phone' => $phone,
                'status' => FALSE,
                'message' => "Nomor tidak valid",
              );
              $partialError = TRUE;
              $valid = FALSE;
            }
          }

          if( substr($phone,0,1) == "0" ){
            $phone = "+62" . substr($phone,1);
          }
          if(!preg_match("/^(\+\d{1,3}[- ]?)?\d{9,13}$/", $phone)) {
            $invitationMessage[] = array(
              'phone' => $phone,
              'status' => FALSE,
              'message' => "Nomor tidak valid",
            );
            $partialError = TRUE;
            $valid = FALSE;
          } else {
            $getUserData = $this->app->user->get($phone);
            if ($getUserData){
              $invitationMessage[] = array(
                'phone' => $phone,
                'status' => FALSE,
                'message' => "Nomor sudah terdaftar di Kentongan",
              );
              $partialError = TRUE;
              $valid = FALSE;
            }
          }

          if ($valid) {
            $neighborhood = $this->app->rt->get($loggedUser['neighbourhood_id']);
            $inviteParams['rt_phone'] = $phone;
            $inviteParams['rt_name'] = $name;
            $inviteParams['name'] = $loggedUser['name'];
            $inviteParams['province'] = $neighborhood['province_id'];
            $inviteParams['regency'] = $neighborhood['city_id'];

            $insert = $this->app->user->insertInvitor($inviteParams);
            if($insert){
              // Send SMS
              $phoneParams = array(
                'phone' => $phone,
                'rt_name' => $inviteParams["rt_name"],
                'name' => $inviteParams['name'],
              );
              $queueParams = array(
                'type' => 'mobile',
                'template' => 'invitorMobileNotification',
                'params' => $phoneParams,
              );
              $sender->execute('userNotification', json_encode($queueParams));
              $invitationMessage[] = array(
                'phone' => $phone,
                'status' => TRUE,
                'message' => "Undangan sukses dikirimkan",
              );
            }
          }
        }
      }
      $message = "";
      $this->response->formatJson($code, $message, $invitationMessage);
    }

    function notification() {
      $sender = new WorkerSender();
      $loggedUser = $this->app->loggedUser;

      $code = 406;
      $user = array();

      if (strtolower($loggedUser['role']) != 'admin') {
        $this->response->formatJson(406, 'Access denid.');
      }
      $params = $this->app->request->params();
      $users =  $this->app->user->index($params);
      foreach($users as $user) {
        $phone = $user['phone'];
        if (strpos('+62', $phone) !== FALSE) {
          $params = array();
          $queueParams = array(
            'type' => 'mobile',
            'template' => 'updateMobileNotification',
            'params' => $params,
          );
          $sender->execute('userNotification', json_encode($queueParams));
        }
      }
    }

    function logged() {
      $response = $this->response;
      $loggedUser = $this->app->loggedUser;

      $users = array($this->getItem($loggedUser, new UserTransformer()));
      $response->setTotal(1);

      $response->formatJson(200, 'User saat ini', $users);
    }

    public function exportExcel(){
      $loggedUser = $this->app->loggedUser;
      $userEmail = $loggedUser['email'];
      $dataRt = $this->app->rt->get($loggedUser['neighbourhood_id']);
      if (!$userEmail) {
          // return errror because empty email receiver
          $this->response->formatJson(406, 'Email tidak tersedia, silakan update email anda');
      }

      $response = $this->response;
      $params = $this->app->request->params();
      if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
      $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      $params['order'] = "family_id";

      $total = $this->app->user->total($params);
      $users =  $this->app->user->index($params);
      $peoples = $this->getCollection($users, new UserTransformer());
      $response->setTotal($total);

      $objPHPExcel = new \PHPExcel();
      $sheet = $objPHPExcel->getActiveSheet();
      $sheet->mergeCells('A1:J1');
      $sheet->setCellValue('A1', sprintf('Data Warga RT %s / RW %s Desa %s',$dataRt['rt'], $dataRt['rw'], $dataRt['village']));
      $sheet->setCellValue('A2', 'Tanggal');
      $sheet->setCellValue('B2', date('d/m/Y'));

      $sheet->getStyle('A1:J1')->applyFromArray([
        'font' => [
          'bold' => true,
          'size' => 15,
        ],
        'alignment' => [
          'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ],
      ]);

      $sheet->setCellValue('A3', 'No');
      $sheet->setCellValue('B3', 'Nama');
      // $sheet->setCellValue('C3', 'No KTP');
      // $sheet->setCellValue('D3', 'No KK');
      $sheet->setCellValue('C3', 'Alamat');
      $sheet->setCellValue('D3', 'Tanggal Lahir');
      $sheet->setCellValue('E3', 'Tempat Lahir');
      $sheet->setCellValue('F3', 'L/P');
      $sheet->setCellValue('G3', 'No Telepon');
      $sheet->setCellValue('H3', 'Email');
      $sheet->setCellValue('I3', 'Pendidikan');
      $sheet->setCellValue('J3', 'Ket');

      $sheet->getStyle('A3:J3')->applyFromArray([
        'font' => [
          'bold' => true,
        ],
      ]);
      foreach ($peoples as $index => $people) {
        $sheet->setCellValue('A'.($index + 4), $index + 1);
        $sheet->setCellValue('B'.($index + 4), $people['name']);
        // $sheet->setCellValue('C'.($index + 4), "'".$people['card_id']);
        // $sheet->setCellValue('D'.($index + 4), "'".$people['family_id']);
        $sheet->setCellValue('C'.($index + 4), $people['address']);
        $sheet->setCellValue('D'.($index + 4), date('d/m/Y', strtotime($people['birth_date'])));
        $sheet->setCellValue('E'.($index + 4), $people['birth_place']);
        $sheet->setCellValue('F'.($index + 4), $people['gender']);
        $sheet->setCellValue('G'.($index + 4), "'".$people['phone']);
        $sheet->setCellValue('H'.($index + 4), $people['email']);
        $sheet->setCellValue('I'.($index + 4), $people['education']);
        $sheet->setCellValue('J'.($index + 4), $people['family_type']);
      }

      // Set column size to auto
      foreach ( range('A', 'J') as $col) {
          $sheet->getColumnDimension($col);
          // $sheet->setAutoSize(true);
      }
      
      $sheet->getStyle('A3:J'.(count($peoples) + 3));

      $sheet->getStyle('A3:J'.(count($peoples) + 3))->applyFromArray([
        'borders' => [
          'allborders' => [
            'style' => \PHPExcel_Style_Border::BORDER_THIN,
          ],
        ],
      ]);

      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

      $excelTmpPath = tempnam(sys_get_temp_dir(), 'excel');
      $objWriter->save($excelTmpPath);
      
      // Send Email
      $emailParams = array(
        'to' => array(
            'email' => $userEmail
          ),
        'subject' => "Data Warga RT " . $dataRt['rt'] . "/" . $dataRt['rw'] . " Desa/Kel " . $dataRt['village'],
        'message' => "Terlampir Data Warga RT " . $dataRt['rt'] . "/" . $dataRt['rw'] . " Desa/Kel " . $dataRt['village'] . ". Silakan Download dari lampiran email ini.",
      );
      $attachment = array(
        'source' => $excelTmpPath,
        'fileName' => "warga.xls",
        'appType' => 'application/vnd.ms-excel'
      );
      $sendEmail = $this->notif->sendMail($emailParams,$attachment);
      unlink($excelTmpPath);
      $this->response->formatJson(200, 'OK');
    } 
  }
