<?php

namespace App\Controller;

use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;
use App\Transformer\ActivityTransformer;
use App\Transformer\ActivityCommentTransformer;

class ActivityController {

  use FractalTrait;

  protected $app;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $fields = $this->app->activity->fields;
    $this->fields = $fields;
  }

  public function index(){
    $loggedUser = $this->app->loggedUser;
    $neightbourhoodId = $loggedUser['neighbourhood_id'];

    $page = $this->app->request->get('page');
    if (!$page){$page = 1;}

    $total = $this->app->activity->total($neightbourhoodId);
    $activities = $this->app->activity->getByNeighbourhoodId($neightbourhoodId, $page);
    $activities = $this->getCollection($activities, new ActivityTransformer());
    $this->response->setTotal($total);

    $this->response->formatJson(200, 'Activities', $activities);
  }

  public function create(){
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
    // if($loggedUser['role'] != 'rt') $response->formatJson(406, 'Access denied');

    $activityData = $this->app->request->post();

    $code = 406;
    $activity = [];
    if ($activityData){
      $activityData['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      $activityData['type'] = 'status';
      $activityData['pid'] = $loggedUser['pid'];
      $activityData['content'] = '{"text" : "' . $activityData['content'] . '"}';

      $errors = $this->appMisc->inputValidate($activityData, $this->fields);

      if($errors){
        $response->formatJson(406, 'Validation Error', $errors);
      }

      // print_r($activityData);
      // return;
      $activityId = $this->app->activity->create($activityData);
      if($activityId){
        $base_url = $this->app->request->getUrl();
        $activity = $this->app->activity->get($activityId);
        $activity['image'] = $base_url . "/" . $activity['image'];
        $content = json_decode($activity['content']);
        $alertMessage = $loggedUser['name'] . " menulis status " . $content->text;
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $activity['neighbourhood_id'],
            'pid' => 'pid-'. $activity['pid']
          ),
          'message' => $alertMessage,
          'data' => array(
            'link' => "app/detail.php?activity_id=".$activity['id'],
            'activity' => $activity['id'],
            'activity_type' => 'status',
            'pid' => $activity['pid'],
            'sound' => 'default'
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));

        $code = 200;
        $message = 'Status berhasil dibuat';
        $response->setTotal(1);
      } else {
        $message = 'Status gagal dibuat';
      }

      $response->formatJson($code, $message, $activity);
    }
  }

  public function detail($id){
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $activity = $this->app->activity->get($id);
    if($activity) {
      if($activity['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
        $response->formatJson(406, 'Access denied');
      }

      $activity = [$this->getItem($activity, new ActivityTransformer())];
      $code = 200;
      $message = 'Activity ditemukan';
      $response->setTotal(1);
    } else {
      $activity = [];
      $response->setTotal(0);
      $code = 406;
      $message = 'Activity tidak ditemukan';
    }

    $response->formatJson($code, $message, $activity);
  }

  public function delete($id) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    $code = 406;
    $activity = $this->app->activity->get($id);
    if (!$activity) {$response->formatJson($code, 'Activity tidak ditemukan');}
    if (
      $activity['neighbourhood_id'] != $this->app->loggedUser['neighbourhood_id']
      && $loggedUser['role'] != 'rt'
    ) {
      $response->formatJson($code, 'Access denied');
    }

    $message = 'Activity gagal dihapus';
    if($this->app->activity->delete($id)){
       $code = 200;
       $message = 'Activity berhasil dihapus';
    }

    $response->formatJson($code, $message, [$activity]);
  }

  public function createComment($activityId) {
    $sender = new WorkerSender();
    $activity = $this->getActivityOr404($activityId);
    $loggedUser = $this->app->loggedUser;

    if ($activity['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
      return $this->response->formatJson(406, 'Access denied');
    }

    $commentData = $this->app->request->post();
    if ($commentData) {
      $commentData['activity_id'] = $activityId;
      $commentData['people_id'] = $loggedUser['pid'];

      $errors = $this->appMisc->inputValidate(
        $commentData,
        $this->app->activityComment->fields
      );

      if ($errors) {
        $this->response->formatJson(406, 'Validation error', $errors);
      }

      $commentId = $this->app->activityComment->create($commentData);
      if ($commentId) {
        
        $mycomment = $this->app->activityComment->get($commentId);

        $people = $this->app->user->get($mycomment['people_id']);
        $alertMessage = $people['name'] . " mengomentari : " . $mycomment['content'];
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $people['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid']
          ),
          'message' => $alertMessage,
          'data' => array(
            'link' => "app/detail.php?activity_id=".$mycomment['activity_id'],
            'pid' => $loggedUser['pid'],
            'sound' => 'default',
            'activity' => $mycomment['activity_id'],
            'activity_type' => 'status',

          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));

        $comment = $this->app->activityComment->get($commentId);
        $comment = $this->getItem($comment, new ActivityCommentTransformer());
        $this->response->formatJson(200, 'Komentar ditambahkan', [$comment]);
        
      }
    }

    $this->response->formatJson(406, 'Komentar gagal ditambahkan');
  }

  public function indexComment($activityId)
  {
    $activity = $this->getActivityOr404($activityId);

    $comments = $this->app->activityComment->findByActivity($activity['id']);
    $comments = $this->getCollection($comments, new ActivityCommentTransformer());

    $this->response->formatJson(200, 'Activity comments', $comments);
  }

  public function deleteComment($activityId, $commentId)
  {
    $activity = $this->getActivityOr404($activityId);
    $loggedUser = $this->app->loggedUser;

    $comment = $this->app->activityComment->get($commentId);

    if (!$comment) {
      $this->response->formatJson(404, 'Comment tidak ditemukan');
    }

    if ($loggedUser['pid'] != $comment['people_id']) {
      $this->response->formatJson(404, 'Comment tidak ditemukan');
    }

    if ($this->app->activityComment->delete($comment['id'])) {
      $this->response->formatJson(200, 'Komentar berhasil dihapus');
    } else {
      $this->response->formatJson(200, 'Komentar gagal dihapus');
    }
  }

  public function getActivityByRefAndType($ref,$type){
    $activity = $this->app->activity->getActivityByRefAndType($ref,$type);
    $total = count($activity);
    $activity = $this->getCollection($activity, new ActivityTransformer());
    $this->response->setTotal($total);

    $this->response->formatJson(200, 'Activities', $activities);
  }

  private function getActivityOr404($id) {
    $activity = $this->app->activity->get($id);

    if (!$activity) {
      return $this->response->formatJson(404, 'Activity tidak ditemukan');
    }

    return $activity;
  }


}
