<?php

namespace App\Controller;

use App\Fractal\FractalTrait;
use App\Transformer\ProductChatTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\AmqpWrapper\WorkerSender;

class ProductChatController
{
    use FractalTrait;

    protected $app;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->response = new Format();
        $this->appMisc = new Misc();
        $this->fields = $this->app->productChat->fields;
    }

    public function create()
    {
        $user = $this->app->loggedUser;

        if (!$user) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $chatParams = $this->app->request->post();

        if ($errors = $this->appMisc->inputValidate($chatParams, $this->fields)) {
            return $this->response->formatJson(406, 'Validation Error', $errors);
        }

        // Validasi relasi database
        if (!$this->app->product->get($chatParams['product_id'])) {
            return $this->response->formatJson(406, 'Produk tidak ditemukan');
        }

        if (!$this->app->user->get($chatParams['to_pid'])) {
            return $this->response->formatJson(406, 'Pengguna tidak ditemukan');
        }

        $chatParams['from_pid'] = $user['pid'];

        if ($chatId = $this->app->productChat->create($chatParams)) {
            $chat = $this->app->productChat->get($chatId);
            $chat = $this->getItem($chat, new ProductChatTransformer());

            return $this->response->formatJson(200, 'Pesan ditambahkan', [$chat]);
        } else {
            return $this->response->formatJson(406, 'Pesan gagal ditambahkan');
        }
    }

    public function index()
    {
        $user = $this->app->loggedUser;

        if (!$user) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $queryParams = $this->app->request->get();

        $result = $this->app->productChat->getConversation($user['pid'], $queryParams);
        $chats = $this->getCollection($result['data'], new ProductChatTransformer(['product']));

        $this->response->setTotal($result['totalCount']);

        return $this->response->formatJson(200, 'Pesan ditemukan', $chats);
    }

    public function conversation($productId)
    {
        $user = $this->app->loggedUser;

        if (!$user) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $queryParams = $this->app->request->get();

        $with = isset($queryParams['with']) ? $queryParams['with'] : false;
        unset($queryParams['with']);
        if (!$with) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $result = $this->app->productChat->getConversationByProductId($user['pid'], $with, $productId, $queryParams);
        $chats = $this->getCollection($result['data'], new ProductChatTransformer());

        $this->response->setTotal($result['totalCount']);

        return $this->response->formatJson(200, 'Pesan ditemukan', $chats);

    }

    public function delete($id)
    {
        $user = $this->app->loggedUser;

        if (!$user) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $chat = $this->app->productChat->get($id);

        if (!$chat || $chat['from_pid'] != $user['pid']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        if ($this->app->productChat->delete($id)) {
            return $this->response->formatJson(200, 'Pesan berhasil dihapus', [$chat]);
        }

        return $this->response->formatJson(200, 'Pesan gagal dihapus', [$chat]);
    }
}
