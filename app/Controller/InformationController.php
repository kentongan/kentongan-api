<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 10:10
 */

namespace App\Controller;

use App\Transformer\InformationTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;

class InformationController {
  use FractalTrait;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $this->fields = $this->app->information->fields;
  }

  public function detail($id) {
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
    $information = $this->app->information->get($id);
    if ($information) {
      if($loggedUser['role'] != 'admin') {
        $response->formatJson(406, 'Pengisian harus dilakukan oleh hak akses admin');
      }
      $information = $this->getItem($information, new InformationTransformer());
      $information = array($information);
      $response->setTotal(1);
      $code = 200;
      $message = 'Informasi ditemukan';
    } else {
      $information = array();
      $response->setTotal(0);
      $code = 406;
      $message = 'Informasi tidak ditemukan';
    }
    $response->formatJson($code, $message, $information);
  }

  public function index() {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    $params = $this->app->request->params();
    if($loggedUser['role'] != 'admin') {
      $response->formatJson(406, 'Pengisian harus dilakukan oleh hak akses admin');
    }

    $total = $this->app->information->total($params);
    $informations =  $this->app->information->index($params);
    $informations = $this->getCollection($informations, new InformationTransformer());

    $response->setTotal($total);
    $response->formatJson(200, 'Daftar Informasi', $informations);
  }

  public function create() {
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    if($loggedUser['role'] != 'admin') {
      $response->formatJson(406, 'Pengisian harus dilakukan oleh hak akses admin');
    }
    $informationData = $this->app->request->post();
    $fields = $this->fields;
    $code = 406;
    $information = array();
    if ($informationData) {
      $errors = $this->appMisc->inputValidate($informationData, $fields);
      if(count($errors)) {
        $message = array();
        foreach($errors as $key => $error) {
          foreach ($error as $m) {
            $message[$key][] = $m;
          }
        }
        $response->formatJson(406, 'Validation Error', $message);
      }
      $informationId = $this->app->information->create($informationData);

      if ($informationId) {
        $notification = $this->app->notif->getNotificationByRefAndType($informationId,'info');
        $information = $this->app->information->get($informationId);
        $alertMessage = "Informasi dari ".$information['institution'];
        $queueParams = array(
          'channel_name' => array(
            'pid' => "pid-". $loggedUser['pid'],
            'name' => "kentongan",
            'appVersion' => "2.2.6",
            'deviceType' => "android",
            // 'rt' => 'rt-1'
          ),
          'message' => $alertMessage,
          'data' => array(
            'notification' => $notification['id'],
            'activity_type' => 'notif',
            'pid' => $loggedUser['pid'],
            'sound' => 'default'
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));
        $information = array($this->getItem($information, new InformationTransformer()));
        $code = 200;
        $message = 'Informasi berhasil dibuat';
        $response->setTotal(1);
      } else {
        $response->setTotal(0);
        $message = 'Informasi gagal dibuat';
      }
    } else {
      $message = 'Tidak ada data yang dikirimkan';
    }
    $response->formatJson($code, $message, $information);
  }

  public function delete($id) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    $code = 406;
    $information = array();
    $currentData = $this->app->information->get($id);
    if (!$currentData) $response->formatJson(406, 'Informasi tidak ditemukan');

    if($loggedUser['role'] != 'admin') {
      $response->formatJson(406, 'Pengisian harus dilakukan oleh hak akses admin');
    }

    $message = 'Informasi gagal dihapus';
    $information = $this->app->information->get($id);
    $deleted = $this->app->information->delete($id);
    if ($deleted) {
      $code = 200;
      $message = 'Informasi berhasil dihapus';
    }
    $response->formatJson($code, $message, $information);
  }
}