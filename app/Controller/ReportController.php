<?php


namespace App\Controller;
use App\Transformer\ReportTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;

class ReportController {
  use FractalTrait;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $this->fields = $this->app->report->fields;
  }

  public function detail($reportId) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $report = $this->app->report->get($reportId);
    if ($report) {
      if($report['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
        $response->formatJson(406, 'Access denied');
      }
      //$report = $response->formatDataOutput($this->fields, $report);
      $report = $this->getItem($report, new ReportTransformer());
      $report = array($report);
      $response->setTotal(1);
      $code = 200;
      $message = 'Laporan ditemukan';
    } else {
      $report = array();
      $response->setTotal(0);
      $code = 406;
      $message = 'Laporan tidak ditemukan';
    }
    $response->formatJson($code, $message, $report);
  }

  public function index() {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $params = $this->app->request->params();
    $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];

    $total = $this->app->report->total($params);
    $reports =  $this->app->report->index($params);
    $reports = $this->getCollection($reports, new ReportTransformer());
    $response->setTotal($total);

    /*$data = array();
    foreach($reports as $v) {
      $data[] = $response->formatDataOutput($this->fields, $v);
    }*/

    $response->formatJson(200, 'Daftar Laporan', $reports);
  }

  public function create() {
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if(!$loggedUser['neighbourhood_id']) {
      $response->formatJson(406, 'Anda tidak terdaftar disuatu RT');
    }
    $reportData = $this->app->request->post();

    $fields = $this->fields;   
    $errors = $this->appMisc->inputValidate($reportData, $fields);
    if(count($errors)) {
      $message = array();
      foreach($errors as $error) {
        foreach ($error as $m) {
          $message[] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }

    $code = 406;
    $report = array();

    if ($reportData) {
      $reportData['sender_pid'] = $loggedUser['pid'];
      $reportData['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      $reportId = $this->app->report->create($reportData);
      if ($reportId) {
        $activity = $this->app->activity->getActivityByRefAndType($reportId,'report');
        $report = $this->app->report->get($reportId);
        $report = array($this->getItem($report, new ReportTransformer()));
        $code = 200;
        $message = 'Laporan berhasil dibuat';
        $response->setTotal(1);

        $get_ketua_rt = $this->app->user->index(array(
          'neighbourhood_id' => $loggedUser['neighbourhood_id'],
          'role' => 'rt',
        ));
        $target_ketua_rt = array();
        foreach($get_ketua_rt as $ketua_rt){
          if($ketua_rt['pid'] == $loggedUser['pid']){
            continue;
          }
          array_push($target_ketua_rt, 'pid-' . $ketua_rt['pid']);
        }

        $reportMessage = $reportData['description'];
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $loggedUser['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid'],
            'target_pid' => array_unique($target_ketua_rt),
          ),
          'message' => $loggedUser['name']. " mengirim laporan \"$reportMessage\"",
          'data' => array(
            'link' => "app/laporan_warga.php?page=edit&id=". $reportId,
            'pid' => $loggedUser['pid'],
            'activity' => $activity['id'],
            'activity_type' => 'report',
            'sound' => 'default'
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));
      } else {
        $response->setTotal(0);
        $message = 'Laporan gagal dibuat';
      }
    } else {
      $message = 'Tidak ada data yang dikirimkan';
    }
    $response->formatJson($code, $message, $report);
  }

  public function update($reportId) {
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if(!$loggedUser['neighbourhood_id']) {
      $response->formatJson(406, 'Anda tidak terdaftar disuatu RT');
    }

    $reportData = $this->app->request->post();
    //$reportData2 = array();
    //if(isset($reportData['response'])) $reportData2['response'] = $reportData['response'];

    $fields = $this->fields;
    $fields['response']['format'] = 'required';
    unset($fields['description']['format']);
    unset($fields['created']);
    $errors = $this->appMisc->inputValidate($reportData, $fields);

    if(count($errors)) {
      $message = array();
      foreach($errors as $key => $error) {
        foreach ($error as $m) {
          $message[$key][] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }
    $currentData = $this->app->report->get($reportId);
    if (!$currentData) $response->formatJson(406, 'Laporan tidak ditemukan');

    $code = 406;
    $report = array();
    $message = '';
    if ($reportData) {
      if($loggedUser['neighbourhood_id'] != $currentData['neighbourhood_id'] || strtolower($loggedUser['role']) != 'rt') {
        $response->formatJson(406, 'Access denied');
      }
      unset($reportData['neighbourhood_id']);
      $updated = $this->app->report->update($reportId, $reportData);
      if ($updated) {
        $get_ketua_rt = $this->app->user->index(array(
          'neighbourhood_id' => $loggedUser['neighbourhood_id'],
          'role' => 'rt',
        ));
        //$ketua_rt = $get_ketua_rt[0];

        $reportMessage = $reportData['response'];
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $loggedUser['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid'],
            'target_pid' => 'pid-'. $currentData['sender_pid'],
          ),
          'message' => 'Pengurus RT telah menanggapi laporan Anda',
          'data' => array(
            'link' => "app/laporan_warga.php?page=edit&id=". $reportId,
            'pid' => $loggedUser['pid'],
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));

        $response->setTotal(1);
        $report = $this->app->report->get($reportId);
        $report = array($this->getItem($report, new ReportTransformer()));
        $code = 200;
        $message = 'Laporan berhasil diubah';
      } else {
        $response->setTotal(0);
        $message = 'Gagal mengubah laporan';
      }
    } else {
      $message = 'No Data Posted';
    }

    $response->formatJson($code, $message, $report);
  }

  public function delete($reportId) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    $currentData = $this->app->report->get($reportId);
    if (!$currentData) $response->formatJson(406, 'Laporan tidak ditemukan');

    if (!($loggedUser['role'] == 'rt' || $loggedUser['pid'] == $currentData['sender_pid'])) $response->formatJson(406, 'Access denied');
    $code = 406;
    $report = array();
    $message = 'Laporan gagal dihapus';
    $deleted = $this->app->report->delete($reportId);
    if ($deleted) {
      $code = 200;
      $message = 'Laporan berhasil dihapus';
    }
    $response->formatJson($code, $message, $report);
  }
}