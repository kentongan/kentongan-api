<?php
namespace App\Controller;

use App\Transformer\NeighbourhoodTransformer;
use App\Transformer\UserTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;

class RtController {
  use FractalTrait;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $this->fields = $this->app->rt->fields;
  }

  public function create() {
    $response = $this->response;
    //$loggedUser = $this->app->loggedUser;
    $rtData = $this->app->request->post();

    unset($rtData['province']);
    unset($rtData['city']);
    unset($rtData['district']);

    $city_tmp = @$rtData['city_tmp'];
    $district_tmp = @$rtData['district_tmp'];
    unset($rtData['city_tmp']);
    unset($rtData['district_tmp']);

    $fields = $this->fields;
    $errors = $this->appMisc->inputValidate($rtData, $fields);
    if(count($errors)) {
      $message = array();
      foreach($errors as $key => $error) {
        foreach ($error as $m) {
          $message[$key][] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }
    if ($rtData) {
      
      // if($this->app->config('app.mode') != 'demo'){
      //   $validate = $this->app->rt->index($rtData);
      //   if (count($validate)) $response->formatJson(406, 'Data RT sudah pernah terdaftar');
      // }

      if($city_tmp != ""){
        $rtData['city_tmp']=$city_tmp;
      }
      if($district_tmp != ""){
        $rtData['district_tmp']=$district_tmp;
      }
      $nid = $this->app->rt->create($rtData);
      /*$userData['neighbourhood'] = $nid;
      $userData['type'] = 'kk';*/

      //$userId = $this->app->user->create($userData);

      $code = 406;
      $data = array();
      if ($nid) {
        $code = 200;
        //$user = $this->app->user->get($userId);
        $rt = $this->app->rt->get($nid);
        $rt = array($this->getItem($rt, new NeighbourhoodTransformer()));

        /*$data = array(
          $rt,
          $user,
        );*/
        $response->setTotal(1);
        $message = 'Pendaftaran RT berhasil';
      } else {
        $message = 'Pendaftaran RT gagal';
      }
    }

    $response->formatJson($code, $message, $rt);
  }

  public function detail($nid) {
    $loggedUser = $this->app->loggedUser;

    $response = $this->response;
    $rt = $this->app->rt->get($nid);

    if ($nid != $loggedUser['neighbourhood_id'] && $loggedUser['role'] != 'admin') $response->formatJson(406, 'Access denied');

    if ($rt) {
      $rt = array($this->getItem($rt, new NeighbourhoodTransformer()));
      $response->setTotal(1);
      $code = 200;
      $message = 'Data RT ditemukan';
    } else {
      $rt = array();
      $response->setTotal(0);
      $code = 406;
      $message = 'Data RT tidak ditemukan';
    }
    $response->formatJson($code, $message, $rt);
  }

  public function update($nid) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if($loggedUser['role'] != 'rt' && $loggedUser['role'] != 'admin') $response->formatJson(406, 'Access denied');

    $code = 406;
    $rtData = $this->app->request->post();

    unset($rtData['province']);
    unset($rtData['city']);
    unset($rtData['district']);
    
    // $validate = $this->app->rt->index($rtData);
    // if (count($validate)) $response->formatJson(406, 'Data RT sudah pernah terdaftar');
    
    $fields = $this->fields;
    foreach ($fields as $key => &$value) {
      unset($value['format']);
    }
    unset($fields['created']);
    $errors = $this->appMisc->inputValidate($rtData, $fields);
    if(count($errors)) {
      $message = array();
      foreach($errors as $key => $error) {
        foreach ($error as $m) {
          $message[$key][] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }

    $currentData = $this->app->rt->get($nid);
    if (!$currentData) $response->formatJson(406, 'Rt tidak ditemukan');

    $rt = array();
    if ($rtData) {
      if ($loggedUser['neighbourhood_id'] != $nid && $loggedUser['role'] != 'admin') {
        $response->formatJson(406, 'Access denied');
      }
      $updated = $this->app->rt->update($nid, $rtData);
      if ($updated) {
        $response->setTotal(1);
        $rt = $this->app->rt->get($nid);
        $rt = array($this->getItem($rt, new NeighbourhoodTransformer()));

        $code = 200;
        $message = 'Data RT berhasil diubah';
      } else {
        $response->setTotal(0);
        $message = 'Data RT gagal diubah';
      }
    } else {
      $message = 'No Data Posted';
    }

    $response->formatJson($code, $message, $rt);
  }

  public function index() {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if($loggedUser['role'] != 'admin') $response->formatJson(406, 'Access denied');

    $params = $this->app->request->params();
    $total = $this->app->rt->total($params);
    $rt =  $this->app->rt->index($params);

    $rts = array($this->getItem($rt, new NeighbourhoodTransformer()));
    $response->setTotal($total);
    $response->formatJson(200, 'Daftar RT', $rts);
  }

  public function block($nid) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    if($loggedUser['role'] != 'admin') $response->formatJson(406, 'Access denied');

    $blocked = $this->app->rt->block($nid);
    $message = 'RT tidak berhasil diblokir';
    $code = 406;
    $rt = array();
    if($blocked) {
      $rt = $this->app->rt->get($nid);
      $rt = array($this->getItem($rt, new NeighbourhoodTransformer()));

      $code = 200;
      $response->setTotal(1);
      $message = 'RT berhasil diblokir';
    }
    $response->formatJson($code, $message, $rt);
  }

  public function reinvite($nid) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    if($loggedUser['role'] != 'admin') $response->formatJson(406, 'Access denied');

    $filter = array('neighbourhood_id' => $nid);
    $peoples = $this->app->user->index($filter);
    $sender = new WorkerSender();
    $reinvited = array();
    $peoples = $this->getCollection($peoples, new UserTransformer());
    foreach($peoples as $people) {
      $phone = $people['phone'];
      if ($people['role'] == 'rt') {
        $inviteParams = array(
          'rt' => $people['neighbourhood']['rt'],
          'rw' => $people['neighbourhood']['rw'],
          'village' => $people['neighbourhood']['village'],
          'rt_name' => $people['name'],
        );
      }
      if (!$phone || $people['role'] == 'rt') continue;
      $updated = $people['updated'];
      $now = time();
      $datediff = $now - $updated;
      $diff = floor($datediff/60/60/24);
      //if user has active for last 3 days, skip it
      if ($diff < 4) {
        continue;
      }
      $reinvited[] = $people;
      $password = substr(md5($phone), 5, 6);
      if($this->app->config('app.mode') == 'demo') $password = 'demo_'. $password;
      $inviteParams['phone'] = $phone;
      $inviteParams['password'] = $password;

      $queueParams = array(
        'type' => 'mobile',
        'template' => 'reinviteMobileNotification',
        'params' => $inviteParams,
      );
      $sender->execute('userNotification', json_encode($queueParams));
    }
    $message = 'Warga diundang kembali ke Kentongan';
    if (!count($reinvited)) $message = 'Tidak ada warga yang diundang kembali ke Kentongan';
    //$reinvited = $this->getCollection($reinvited, new UserTransformer());
    $response->setTotal(count($reinvited));
    $response->formatJson(200, $message, $reinvited);
  }
}