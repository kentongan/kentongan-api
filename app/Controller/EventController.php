<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 10:10
 */

namespace App\Controller;

use App\Transformer\EventTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;

class EventController {
  use FractalTrait;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $this->fields = $this->app->event->fields;
  }

  public function detail($id) {
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $event = $this->app->event->get($id);
    if ($event) {
      if($event['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
        $response->formatJson(406, 'Access denied');
      }
      $event = $this->getItem($event, new EventTransformer());
      //$event = $response->formatDataOutput($this->fields, $event);
      $event = array($event);
      $response->setTotal(1);
      $code = 200;
      $message = 'Kegiatan ditemukan';
    } else {
      $event = array();
      $response->setTotal(0);
      $code = 406;
      $message = 'Kegiatan tidak ditemukan';
    }
    $response->formatJson($code, $message, $event);
  }

  public function index() {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    $params = $this->app->request->params();
    if($loggedUser['role'] != 'admin') {
      if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
      $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
    }

    $total = $this->app->event->total($params);
    $events =  $this->app->event->index($params);
    $events = $this->getCollection($events, new EventTransformer());

    $response->setTotal($total);

    /*$data = array();
    foreach($events as $v) {
      $data[] = $response->formatDataOutput($this->fields, $v);
    }*/

    $response->formatJson(200, 'Daftar Kegiatan', $events);
  }

  public function create() {
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
    if($loggedUser['role'] != 'rt') $response->formatJson(406, 'Access denied');
    $eventData = $this->app->request->post();

    $fields = $this->fields;

    $code = 406;
    $event = array();

    if ($eventData) {

      $errors = $this->appMisc->inputValidate($eventData, $fields);
      if($eventData['start_date'] > $eventData['end_date']) {
        $errors['end_date']['validate'] = 'Kolom end_date tidak boleh lebih kecil daripada start_date';
      }
      if(count($errors)) {
        $message = array();
        foreach($errors as $key => $error) {
          foreach ($error as $m) {
            $message[$key][] = $m;
          }
        }
        $response->formatJson(406, 'Validation Error', $message);
      }

      $eventData['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      $eventData['createdby'] = $loggedUser['pid'];
      $eventData['start_date'] = strtotime($eventData['start_date']);
      $eventData['end_date'] = strtotime($eventData['end_date']);
      $eventId = $this->app->event->create($eventData);
      if ($eventId) {
        $activity = $this->app->activity->getActivityByRefAndType($eventId,'event');
        $event = $this->app->event->get($eventId);
        $alertMessage = "Ada acara ".$event['name']." di ". $event['location']." pada ". date('Y-m-d H:i',$event['start_date']);
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $event['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid']
          ),
          'message' => $alertMessage,
          'data' => array(
            'link' => "app/info_kegiatan.php?page=view&id=".$eventId,
            'activity' => $activity['id'],
            'activity_type' => 'event',
            'pid' => $loggedUser['pid'],
            'sound' => 'default'
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));
        //$this->app->alert->pushNotif(array("alert" => $alertMessage , "link" => "app/info_kegiatan.php?page=view&id=".$eventId), array("rt-". $event['neighbourhood_id']));
        //$event = array($response->formatDataOutput($this->fields, $event));
        $event = array($this->getItem($event, new EventTransformer()));
        $code = 200;
        $message = 'Kegiatan berhasil dibuat';
        $response->setTotal(1);
      } else {
        $response->setTotal(0);
        $message = 'Kegiatan gagal dibuat';
      }
    } else {
      $message = 'Tidak ada data yang dikirimkan';
    }
    $response->formatJson($code, $message, $event);
  }

  public function update($id) {
    $sender = new WorkerSender();
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;

    if($loggedUser['role'] != 'rt') $response->formatJson(406, 'Access denied');

    $eventData = $this->app->request->post();

    $fields = $this->fields;
    unset($fields['created']);
    $errors = $this->appMisc->inputValidate($eventData, $fields);
    if(count($errors)) {
      $message = array();
      foreach($errors as $key => $error) {
        foreach ($error as $m) {
          $message[$key][] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }

    $code = 406;
    $event = array();
    $message = '';
    $currentData = $this->app->event->get($id);
    if (!$currentData) $response->formatJson(406, 'Kegiatan tidak ditemukan');
    if($currentData['neighbourhood_id'] != $loggedUser['neighbourhood_id']) $response->formatJson(406, 'Access denied');

    if ($eventData) {
      $eventData['start_date'] = strtotime($eventData['start_date']);
      $eventData['end_date'] = strtotime($eventData['end_date']);
      $eventData['createdby'] = $loggedUser['pid'];
      $updated = $this->app->event->update($id, $eventData);
      if ($updated) {

        $activity = $this->app->activity->getActivityByRefAndType($updated,'event');
        $response->setTotal(1);
        $event = $this->app->event->get($id);
        $alertMessage = "Perubahan Acara ". $event['name']." di ". $event['location']." pada ". date('d-m-Y H:i', $event['start_date']);
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $event['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid']
          ),
          'message' => $alertMessage,
          'data' => array(
            'link' => "app/info_kegiatan.php?page=view&id=".$id,
            'pid' => $loggedUser['pid'],
            'sound' => 'default',
            'activity' => $activity['id'],
            'activity_type' => 'event',
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));
        $event = array($this->getItem($event, new EventTransformer()));
        $code = 200;
        $message = 'Kegiatan berhasil diubah';
      } else {
        $response->setTotal(0);
        $message = 'Gagal mengubah Kegiatan';
      }
    } else {
      $message = 'No Data Posted';
    }

    $response->formatJson($code, $message, $event);
  }

  public function delete($id) {
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    $code = 406;
    $event = array();
    $currentData = $this->app->event->get($id);
    if (!$currentData) $response->formatJson(406, 'Kegiatan tidak ditemukan');

    if($currentData['neighbourhood_id'] != $loggedUser['neighbourhood_id']) $response->formatJson(406, 'Access denied');

    $message = 'Kegiatan gagal dihapus';
    $event = $this->app->event->get($id);
    $deleted = $this->app->event->delete($id);
    if ($deleted) {
      $activity = $this->app->activity->getActivityByRefAndType($deleted,'event');
      $alertMessage = "Pembatalan Acara ". $event['name']." di ". $event['location']." pada ". date('d-m-Y H:i', $event['start_date']);
      $queueParams = array(
        'channel_name' => array(
          'rt' => "rt-". $event['neighbourhood_id'],
          'pid' => 'pid-'. $loggedUser['pid']
        ),
        'message' => $alertMessage,
        'data' => array(
          'link' => "app/info_kegiatan.php",
          'pid' => $loggedUser['pid'],
          'sound' => 'default',
          'activity' => $activity['id'],
          'activity_type' => 'event',
        )
      );
      $queueParams['mode'] = $this->app->config('app.mode');
      $sender->execute('alertNotification', json_encode($queueParams));

      $code = 200;
      $message = 'Kegiatan berhasil dihapus';
    }
    $response->formatJson($code, $message, $event);
  }
}