<?php

namespace App\Controller;

use App\Transformer\FinanceTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;

class FinanceController {
  use FractalTrait;
    protected $app;
    private $format;
    private $params;
    private $response;

    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->response = new Format();
      $this->appMisc = new Misc();
//      $this->getparams();
      $fields = $this->app->finance->fields;
      $this->fields = $fields;
    }

    private function getparams(){
      $this->getfieldselect();
      $this->getfieldedit();
    }

    private function getfieldselect(){
      $this->fieldsSelect = array(
        'fid' => array(
          'type' => "primarykey"
        ),
        'type' => array(
          'type' => "string",
          'format' => "maxlength:1,required"
        ),
        'created' => array(
          'type' => "datetime",
          'format' => "required"
        ),
        'description' => array(
          'type' => "textarea",
          'format' => "required"
        ),
        'amount' => array(
          'type' => "number",
          'format' => "required,maxlength:11"
        ),
        'total_amount' => array(
          'type' => "number",
          'format' => "required,maxlength:11"
        ),
        'neighbourhood_id' => array(
          'format' => "required",
          'type' => "number"
        ),
        'people_id' => array(
          'format' => "required",
          'type' => "number"
        ),
        'tag' => array(
          'type' => "text",
          'format' => "required,maxlength:50"
        ),
      );
    }

    private function getfieldedit(){
      $this->fieldsEdit = array(
        'type' => array(
          'format' => "maxlength:1,required"
        ),
        'created' => array(
          'type' => "datetime",
          'format' => "required"
        ),
        'description' => array(
          'format' => "required"
        ),
        'amount' => array(
          'type' => "number",
          'format' => "required,maxlength:11"
        ),
        'neighbourhood_id' => array(
          'format' => "required",
          'type' => "number"
        ),
        'people_id' => array(
          'format' => "required",
          'type' => "number"
        ),
        'tag' => array(
          'format' => "required,maxlength:50"
        ),
      );
    }

    public function index() {
      $loggedUser = $this->app->loggedUser;
      if (!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Anda belum terdaftar disuatu RT');

      $params = $this->app->request->params();
      $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];

      $total = $this->app->finance->total($params);
      $finances = $this->app->finance->index($params);
      $finances = $this->getCollection($finances, new FinanceTransformer());
      /*$finances = array();
      foreach($data as $v) {
        $finances[] = $this->response->formatDataOutput($this->fields, $v);
      }*/
      $this->response->setTotal($total);
      $this->response->formatJson(200, 'Daftar laporan keuangan', $finances);
    }

    public function startAmount() {
      $loggedUser = $this->app->loggedUser;
      if (!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Anda belum terdaftar disuatu RT');

      $params = $this->app->request->params();
      $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];

      if(!isset($params['year']) || !isset($params['month']) ){
        $this->response->formatJson(406, "Invalid parameters", $message);
      }

      $total = 1;
      $finances = $this->app->finance->startAmount($params);
      $finances = $this->getCollection($finances, new FinanceTransformer());
      $this->response->setTotal($total);
      $this->response->formatJson(200, 'Saldo Awal', $finances);
    }

    public function lastAmount() {
      $loggedUser = $this->app->loggedUser;
      if (!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Anda belum terdaftar disuatu RT');

      $params = $this->app->request->params();
      $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];

      if(!isset($params['year']) || !isset($params['month']) ){
        $this->response->formatJson(406, "Invalid parameters", $message);
      }

      $total = 1;
      $finances = $this->app->finance->lastAmount($params);
      $finances = $this->getCollection($finances, new FinanceTransformer());
      $this->response->setTotal($total);
      $this->response->formatJson(200, 'Saldo Akhir', $finances);
    }

    public function create() {
      $sender = new WorkerSender();
      $loggedUser = $this->app->loggedUser;
      if ($loggedUser['role'] != 'rt')   $this->response->formatJson(406, 'Access denied');
      if (!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Anda belum terdaftar disuatu RT');

      $financeData = $this->app->request->post();
      unset($this->fields['total_amount']);
      $errors = $this->appMisc->inputValidate($financeData, $this->fields);

      if(count($errors)) {
        $message = array();
        foreach($errors as $key => $error) {
          foreach ($error as $m) {
            $message[$key][] = $m;
          }
        }
        $this->response->formatJson(406, "Validation Error", $message);
      }
      else {
        $code = 406;
        $finance = array();
        if ($financeData) {
          $financeData['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
          $financeData['people_id'] = $loggedUser['pid'];
          $fid = $this->app->finance->create($financeData);
          if ($fid) {
            $activity = $this->app->activity->getActivityByRefAndType($fid,'finance');
            $finance = $this->app->finance->get($fid);
            $finance = array($this->getItem($finance, new FinanceTransformer()));
            $code = 200;
            $message = 'Laporan keuangan berhasil dibuat';
            $this->response->setTotal(1);

            $fin = $this->app->finance->get($fid);
            $type = ($fin['type'] == "I") ? "Pemasukan" : "Pengeluaran" ;
            $alertMessage = "Ada " . $type . " Kas RT Sebesar " . number_format($fin['amount'],0) . " - ".$fin['description'] ;
            $queueParams = array(
              'channel_name' => array(
                'rt' => "rt-". $fin['neighbourhood_id'],
                'pid' => 'pid-'. $loggedUser['pid']
              ),
              'message' => $alertMessage,
              'data' => array(
                'link' => "app/laporan_kas.php",
                'pid' => $loggedUser['pid'],
                'activity' => $activity['id'],
                'activity_type' => 'finance',
                'sound' => 'default'
              )
            );
            $queueParams['mode'] = $this->app->config('app.mode');
            $sender->execute('alertNotification', json_encode($queueParams));
            

          } else {
            $this->response->setTotal(0);
            $message = 'Laporan keuangan gagal dibuat';
          }
        } else {
          $message = 'No Data Posted';
        }
        $this->response->formatJson($code, $message, $finance);
      }
    }

    public function detail($fid){
      $loggedUser = $this->app->loggedUser;
      if (!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Anda belum terdaftar disuatu RT');

      $code = 406;
      $message = 'Laporan keuangan tidak ditemukan';
      $finance = $this->app->finance->get($fid);
      if($finance) {
        if($finance['neighbourhood_id'] != $loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Access denied');
        $message = 'Laporan keuangan ditemukan';
        $finance = array($this->getItem($finance, new FinanceTransformer()));
        $code = 200;
      }
      $this->response->formatJson($code, $message, $finance);
    }

    public function update($fid){
      $sender = new WorkerSender();
      $loggedUser = $this->app->loggedUser;
      if ($loggedUser['role'] != 'rt')   $this->response->formatJson(406, 'Access denied');
      if (!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Anda belum terdaftar disuatu RT');

      $financeData = $this->app->request->post();
      $errors = $this->appMisc->inputValidate($financeData, $this->fields);

      if(count($errors)) {
        $message = array();
        foreach($errors as $key => $error) {
          foreach ($error as $m) {
            $message[$key][] = $m;
          }
        }
        $this->response->formatJson(406, "Validation Error", $message);
      }
      else {
        $code = 406;
        $finance = array();
        if ($financeData) {
          $financeData['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
          $financeData['people_id'] = $loggedUser['pid'];
          // Get Old Data
          $old_finance = $this->app->finance->get($fid);
          $finance_id = $this->app->finance->update($fid, $financeData);
          if ($finance_id) {
            // Update Last Amount
            $this->app->finance->updateLastAmount($fid,$financeData,$old_finance,"update");
            // Activity
            $activity = $this->app->activity->getActivityByRefAndType($finance_id,'finance');
            $finance = $this->app->finance->get($fid);
            $finance = array($this->getItem($finance, new FinanceTransformer()));
            $code = 200;
            $message = 'Laporan keuangan berhasil diubah';
            $this->response->setTotal(1);

            $fin = $this->app->finance->get($fid);
            $type = ($fin['type'] == "I") ? "Pemasukan" : "Pengeluaran" ;
            $alertMessage = "Perubahan kas RT sebesar " . number_format($fin['amount'],0) . " - ".$fin['description'] ;
            $queueParams = array(
              'channel_name' => array(
                'rt' => "rt-". $fin['neighbourhood_id'],
                'pid' => 'pid-'. $loggedUser['pid']
              ),
              'message' => $alertMessage,
              'data' => array(
                'link' => "app/laporan_kas.php",
                'pid' => $loggedUser['pid'],
                'activity' => $activity['id'],
                'activity_type' => 'finance',
                'sound' => 'default'
              )
            );
            $queueParams['mode'] = $this->app->config('app.mode');
            $sender->execute('alertNotification', json_encode($queueParams));

          } else {
            $this->response->setTotal(0);
            $message = 'Laporan keuangan gagal diubah';
          }
        } else {
          $message = 'No Data Posted';
        }
        $this->response->formatJson($code, $message, $finance);
      }
    }

    public function delete($fid){
      $loggedUser = $this->app->loggedUser;
      if ($loggedUser['role'] != 'rt')   $this->response->formatJson(406, 'Access denied');
      if (!$loggedUser['neighbourhood_id']) $this->response->formatJson(406, 'Anda belum terdaftar disuatu RT');

      $code = 406;
      $message = 'Laporan keuangan gagal dihapus';
      $finance = $this->app->finance->get($fid);
      if(!$finance){
        $this->response->formatJson(406, 'Laporan keuangan tidak ditemukan');
      }
      $finance_del = $this->app->finance->delete($fid);
      if($finance_del) {
        // Update Last Amount
        $this->app->finance->updateLastAmount($fid,array(),$finance,"delete");
        $message = 'Laporan keuangan berhasil dihapus';
        $finance = array($this->getItem($finance, new FinanceTransformer()));
        $code = 200;
      }
      $this->response->formatJson($code, $message, $finance);
    }
}
