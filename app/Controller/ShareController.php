<?php
  namespace App\Controller;

  use App\Transformer\ShareTransformer;
  use App\Wrapper\Format;
  use App\Wrapper\Misc;
  use \Firebase\JWT\JWT;
  use App\Fractal\FractalTrait;

  class ShareController {
    use FractalTrait;
    protected $app;
    private $format;


    public function __construct() {
      $this->app = \Slim\Slim::getInstance();
      $this->response = new Format();
      $this->appMisc = new Misc();
    }

    public function index() {
      $loggedUser = $this->app->loggedUser;
      $base_url = $this->app->request->getUrl();
      $image_url = $base_url . "/files/share_images/shared_image.jpeg";
      $response = $this->response;
      $total = 1;
      $users =  $this->app->user->get($loggedUser['pid']);
      unset($users['password']);
      $users['image_share'] = $image_url;
      $userData = $this->getCollection(array($users), new ShareTransformer());
      $response->setTotal($total);
      $response->formatJson(200, 'Share Kentongan', $userData);
    }

    public function share(){
      // $base_url = $this->app->request->getUrl();
      $image_url = "./files/share_images/shared_image.jpeg";
      $params = $this->app->request->params();
      if(!isset($params['pid']) || !isset($params['rt'])) {
        $this->response->formatJson(406, 'Invalid parameters');
      }
      $data = $this->app->user->getByPidAndNid($params['pid'],$params['rt']);
      if($data){
        // open the file in a binary mode
        $name = './img/ok.png';
        $fp = fopen($image_url, 'rb');

        // send the right headers
        header("Content-Type: image/png");
        header("Content-Length: " . filesize($image_url));

        fpassthru($fp);
        exit;
      }
      else {
        $this->response->formatJson(406, 'Failed to load Image');
      }
    }
  }
