<?php

namespace App\Controller;

use App\Fractal\FractalTrait;
use App\Model\Product;
use App\Transformer\ProductTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\AmqpWrapper\WorkerSender;

class ProductController
{
    use FractalTrait;

    protected $app;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->response = new Format();
        $this->appMisc = new Misc();
        $this->fields = $this->app->product->fields;
    }

    public function create()
    {   
        $sender = new WorkerSender();
        $loggedUser = $this->app->loggedUser;

        if (!$loggedUser) {
            return $this->response->formatJson(406, 'Access denied');
        }

        // $productParams = $this->app->request->getBody();
        $productParams = $this->app->request->post();

        $productParams['pid'] = $loggedUser['pid'];
        $productParams['nid'] = $loggedUser['neighbourhood_id'];

        // auto approve product if user is pak rt
        if ($loggedUser['role'] == 'rt') {
            $productParams['approval'] = Product::APPROVAL_ACCEPTED;
        } else {
            $productParams['approval'] = Product::APPROVAL_WAITING;
        }

        $productImages = isset($productParams['images']) ? $productParams['images'] : [];
        unset($productParams['images']);

        if ($errors = $this->appMisc->inputValidate($productParams, $this->fields)) {
            return $this->response->formatJson(406, 'Validation Error', $errors);
        }

        if ($productId = $this->app->product->create($productParams)) {
            if (count($productImages)) {
                $this->app->productImage->createImages($productId, $productImages);
            }

            $product = $this->app->product->get($productId);
            $product = $this->getItem($product, new ProductTransformer());

            $get_ketua_rt = $this->app->user->index(array(
              'neighbourhood_id' => $loggedUser['neighbourhood_id'],
              'role' => 'rt',
            ));
            $target_ketua_rt = array();
            foreach($get_ketua_rt as $ketua_rt){
              if($ketua_rt['pid'] == $loggedUser['pid']){
                continue;
              }
              array_push($target_ketua_rt, 'pid-' . $ketua_rt['pid']);
            }
            // Notification to RT
            $queueParams = array(
              'channel_name' => array(
                'rt' => "rt-". $loggedUser['neighbourhood_id'],
                'pid' => 'pid-'. $loggedUser['pid'],
                'target_pid' => array_unique($target_ketua_rt),
              ),
              'message' => $loggedUser['name'] . " menambah produk ".$productParams['name']." untuk dijual.",
              'data' => array(
                'pid' => $loggedUser['pid'],
                'activity_type' => 'productCreate',
                'sound' => 'default',
                'productId' => $productId
              )
            );
            $queueParams['mode'] = $this->app->config('app.mode');
            $sender->execute('alertNotification', json_encode($queueParams));
        
            return $this->response->formatJson(200, 'Produk berhasil', [$product]);
        } else {
            return $this->response->formatJson(406, 'Produk gagal');
        }
    }

    public function detail($id)
    {
        if (!$this->app->loggedUser) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $product = $this->app->product->get($id);

        if ($product) {
            // increment product view
            $product['view'] = (int) $product['view'] + 1;
            $this->app->product->update($id, $product);
            $product = $this->getItem($product, new ProductTransformer());

            return $this->response->formatJson(200, 'Produk ditemukan', [$product]);
        }

        return $this->response->formatJson(404, 'Produk tidak ditemukan');
    }

    public function update($id)
    {
        $loggedUser = $this->app->loggedUser;
        if (!$loggedUser) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $product = $this->app->product->get($id);
        if (!$product) {
            return $this->response->formatJson(404, 'Produk tidak ditemukan');
        }
        if ($product['pid'] != $loggedUser['pid']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $productParams = $this->app->request->post();
        if ($errors = $this->appMisc->inputValidate($productParams, $this->fields)) {
            return $this->response->formatJson(406, 'Validation Error', $errors);
        }

        if ($this->app->product->update($id, $productParams)) {
            $product = $this->app->product->get($id);
            $product = $this->getItem($product, new ProductTransformer());

            return $this->response->formatJson(200, 'Produk berhasil diubah', [$product]);
        }

        return $this->response->formatJson(406, 'Produk gagal diubah');
    }

    public function delete($id)
    {
        $loggedUser = $this->app->loggedUser;
        if (!$loggedUser) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $product = $this->app->product->get($id);
        if (!$product) {
            return $this->response->formatJson(404, 'Produk tidak ditemukan');
        }
        if ($product['pid'] != $loggedUser['pid']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        if ($this->app->product->delete($id)) {
            $product = $this->getItem($product, new ProductTransformer());

            return $this->response->formatJson(200, 'Produk berhasil dihapus', [$product]);
        }

        return $this->response->formatJson(406, 'Produk gagal dihapus');
    }

    public function index()
    {
        $queryParams = $this->app->request->get();

        if(isset($queryParams['approval'])) {
            $queryParams['approval'] = explode(',', $queryParams['approval']);
        }

        if(isset($queryParams['status'])) {
            $queryParams['status'] = explode(',', $queryParams['status']);
        }

        $result = $this->app->product->index($queryParams);
        $products = $result['data'];
        $products = $this->getCollection($products, new ProductTransformer());

        $totalCount = $result['totalCount'];
        $this->response->setTotal($totalCount);

        return $this->response->formatJson(200, 'Produk', $products);
    }

    public function accept($id)
    {
        $sender = new WorkerSender();
        $loggedUser = $this->app->loggedUser;
        // Get Product
        $product = $this->app->product->get($id);
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $loggedUser['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid'],
            'target_pid' => 'pid-'. $product['pid'],
          ),
          'message' => "Produk Anda ".$product['name']." telah disetujui.",
          'data' => array(
            'pid' => $loggedUser['pid'],
            'activity_type' => 'productAccept',
            'sound' => 'default',
            'productId' => $id
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));
        return $this->setApproval($id, Product::APPROVAL_ACCEPTED);
    }

    public function reject($id)
    {
        $sender = new WorkerSender();
        $loggedUser = $this->app->loggedUser;
        // Get Product
        $product = $this->app->product->get($id);
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $loggedUser['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid'],
            'target_pid' => 'pid-'. $product['pid'],
          ),
          'message' => "Produk Anda ".$product['name']." tidak disetujui / dibatalkan.",
          'data' => array(
            'pid' => $loggedUser['pid'],
            'activity_type' => 'productReject',
            'sound' => 'default',
            'productId' => $id
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));
        return $this->setApproval($id, Product::APPROVAL_REJECTED);
    }

    private function setApproval($productId, $approval)
    {
        $loggedUser = $this->app->loggedUser;

        if (!$loggedUser || $loggedUser['role'] != 'rt') {
            return $this->response->formatJson(406, 'Access denied');
        }

        $product = $this->app->product->get($productId);

        if (!$product) {
            return $this->response->formatJson(404, 'Produk tidak ditemukan');
        }

        $seller = $this->app->user->get($product['pid']);

        if ($seller['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $product['approval'] = $approval;

        if ($this->app->product->update($productId, $product)) {
            return $this->response->formatJson(
                200,
                ($approval == Product::APPROVAL_ACCEPTED) ? 'Produk diterima' : 'Produk ditolak',
                [$this->getItem($product, new ProductTransformer())]
            );
        }

        return $this->response->formatJson(406, 'Terjadi kesalahan');
    }
}
