<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 15:43
 */

namespace App\Controller;
use App\Transformer\AlertTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;
use App\AmqpWrapper\WorkerSender;


class AlertController {
  use FractalTrait;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $this->fields = $this->app->alert->fields;
  }

  public function detail($alertId) {
    $response = $this->response;

    $loggedUser = $this->app->loggedUser;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $alert = $this->app->alert->get($alertId);
    if ($alert) {
      if($alert['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
        $response->formatJson(406, 'Access denied');
      }

      //$alert = $response->formatDataOutput($this->fields, $alert);
      $alert = $this->getItem($alert, new AlertTransformer());
      $alert = array($alert);
      $response->setTotal(1);
      $code = 200;
      $message = 'Tanda bahaya ditemukan';
    } else {
      $alert = array();
      $response->setTotal(0);
      $code = 406;
      $message = 'Tanda bahaya tidak ditemukan';
    }
    $response->formatJson($code, $message, $alert);
  }

  public function index() {
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;
    $params = $this->app->request->params();
    
    if($loggedUser['role'] != 'admin') {
      if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
      $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
    }

    $total = $this->app->alert->total($params);
    $alerts =  $this->app->alert->index($params);
    $alerts = $this->getCollection($alerts, new AlertTransformer());
    $response->setTotal($total);

    /*$data = array();
    foreach($alerts as $v) {
      $data[] = $response->formatDataOutput($this->fields, $v);
    }*/

    $response->formatJson(200, 'Daftar tanda bahaya', $alerts);
  }

  public function create() {
    $sender = new WorkerSender();
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $alertData = $this->app->request->post();

    $fields = $this->fields;
    $errors = $this->appMisc->inputValidate($alertData, $fields);
    if(count($errors)) {
      $message = array();
      foreach($errors as $error) {
        $message[] = $error;
      }
      $response->formatJson(406, 'Validation Error', $message);
    }

    $code = 406;
    $alert = array();

    if ($alertData) {
      $alertData['sender_pid'] = $loggedUser['pid'];
      $alertData['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      $alertId = $this->app->alert->create($alertData);
      if ($alertId) {
        $activity = $this->app->activity->getActivityByRefAndType($alertId,'alert');
        $status = '';
        switch ($alertData['type']) {
          case "1" :
            $status = "Kebakaran!!";
            break;
          case "2" :
            $status = "Pencurian/Perampokan!!";
            break;
          case "3" :
            $status = "Bencana Alam!!";
            break;
          case "4" :
            $status = "Kecelakaan!!";
            break;
        }
        $message = $loggedUser['name'] . " mengirimkan tanda bahaya ". $status;
        $queueParams = array(
          'channel_name' => array(
            'rt' => "rt-". $alertData['neighbourhood_id'],
            'pid' => 'pid-'. $loggedUser['pid']
          ),
          'message' => $message,
          //'link' => "app/panic_alarm.php?page=detail&id=".$alertId. "&pid=". $loggedUser['pid'],
          'data' => array(
            'link' => "app/panic_alarm.php?page=detail&id=".$alertId,
            'pid' => $loggedUser['pid'],
            'activity' => $activity['id'],
            'activity_type' => 'alert',
            'sound' => 'Soundkentongan.aiff'
          )
        );
        $queueParams['mode'] = $this->app->config('app.mode');
        $sender->execute('alertNotification', json_encode($queueParams));

        $alert = $this->app->alert->get($alertId);
        $alert = array($this->getItem($alert, new AlertTransformer()));
        //$alert = array($response->formatDataOutput($this->fields, $alert));
        $code = 200;
        $message = 'Tanda bahaya berhasil dibuat';
        $response->setTotal(1);
      } else {
        $response->setTotal(0);
        $message = 'Tanda bahaya gagal dibuat';
      }
    } else {
      $message = 'Tidak ada data yang dikirimkan';
    }
    $response->formatJson($code, $message, $alert);
  }
}