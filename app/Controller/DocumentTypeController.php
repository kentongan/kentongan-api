<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/24/15
 * Time: 10:10
 */

namespace App\Controller;

use App\Transformer\DocumentTypeTransformer;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Fractal\FractalTrait;

class DocumentTypeController {
  use FractalTrait;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $this->fields = $this->app->documenttype->fields;
  }

  public function detail($id) {
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $documentType = $this->app->documenttype->get($id);
    if ($documentType) {
      if($documentType['neighbourhood_id'] != $loggedUser['neighbourhood_id']) {
        $response->formatJson(406, 'Access denied');
      }
      $documentType = $this->getItem($documentType, new DocumentTypeTransformer());
      //$documentType = $response->formatDataOutput($this->fields, $documentType);
      $documentType = array($documentType);
      $response->setTotal(1);
      $code = 200;
      $message = 'Tipe Dokumen ditemukan';
    } else {
      $documentType = array();
      $response->setTotal(0);
      $code = 406;
      $message = 'Tipe Dokumen tidak ditemukan';
    }
    $response->formatJson($code, $message, $documentType);
  }

  public function index() {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;

    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');

    $params = $this->app->request->params();
    $params['neighbourhood_id'] = $loggedUser['neighbourhood_id'];

    $total = $this->app->documenttype->total($params);
    $documentTypes =  $this->app->documenttype->index($params);
    $documentTypes = $this->getCollection($documentTypes, new DocumentTypeTransformer());

    $response->setTotal($total);

    $response->formatJson(200, 'Daftar Tipe Dokumen', $documentTypes);
  }

  public function create() {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    if(!$loggedUser['neighbourhood_id']) $response->formatJson(406, 'User belum terdaftar disuatu RT');
    if($loggedUser['role'] != 'rt') $response->formatJson(406, 'Access denied');
    $data = $this->app->request->post();

    $fields = $this->fields;

    $code = 406;
    $documentType = array();

    if ($data) {
      $errors = $this->appMisc->inputValidate($data, $fields);
      if(count($errors)) {
        $message = array();
        foreach($errors as $key => $error) {
          foreach ($error as $m) {
            $message[$key][] = $m;
          }
        }
        $response->formatJson(406, 'Validation Error', $message);
      }

      $data['neighbourhood_id'] = $loggedUser['neighbourhood_id'];
      
      $dtid = $this->app->documenttype->create($data);
      if ($dtid) {
        $documentType = $this->app->documenttype->get($dtid);
        $documentType = array($this->getItem($documentType, new DocumentTypeTransformer()));
        $code = 200;
        $message = 'Tipe Dokumen berhasil dibuat';
        $response->setTotal(1);
      } else {
        $response->setTotal(0);
        $message = 'Tipe Dokumen gagal dibuat';
      }
    } else {
      $message = 'Tidak ada data yang dikirimkan';
    }
    $response->formatJson($code, $message, $documentType);
  }

  public function update($id) {
    $response = $this->response;
    $loggedUser = $this->app->loggedUser;

    if($loggedUser['role'] != 'rt') $response->formatJson(406, 'Access denied');

    $data = $this->app->request->post();

    $fields = $this->fields;
    unset($fields['created']);
    $errors = $this->appMisc->inputValidate($data, $fields);
    if(count($errors)) {
      $message = array();
      foreach($errors as $key => $error) {
        foreach ($error as $m) {
          $message[$key][] = $m;
        }
      }
      $response->formatJson(406, 'Validation Error', $message);
    }

    $code = 406;
    $documentType = array();
    $message = '';
    $currentData = $this->app->documenttype->get($id);
    if (!$currentData) $response->formatJson(406, 'Tipe Dokumen tidak ditemukan');
    if($currentData['neighbourhood_id'] != $loggedUser['neighbourhood_id']) $response->formatJson(406, 'Access denied');

    if ($data) {
      $updated = $this->app->documenttype->update($id, $data);
      if ($updated) {
        $response->setTotal(1);
        $documentType = $this->app->documenttype->get($id);
        $documentType = array($this->getItem($documentType, new DocumentTypeTransformer()));
        $code = 200;
        $message = 'Tipe Dokumen berhasil diubah';
      } else {
        $response->setTotal(0);
        $message = 'Gagal mengubah Tipe Dokumen';
      }
    } else {
      $message = 'No Data Posted';
    }

    $response->formatJson($code, $message, $documentType);
  }

  public function delete($id) {
    $loggedUser = $this->app->loggedUser;
    $response = $this->response;
    $code = 406;
    $documentType = array();
    $currentData = $this->app->documenttype->get($id);
    if (!$currentData) $response->formatJson(406, 'Tipe Dokumen tidak ditemukan');

    if($currentData['neighbourhood_id'] != $loggedUser['neighbourhood_id']) $response->formatJson(406, 'Access denied');

    $message = 'Tipe Dokumen gagal dihapus';
    $deleted = $this->app->documenttype->delete($id);
    if ($deleted) {
      $code = 200;
      $message = 'Tipe Dokumen berhasil dihapus';
    }
    $response->formatJson($code, $message, $documentType);
  }
}