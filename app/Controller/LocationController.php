<?php

namespace App\Controller;
use App\Wrapper\Format;
use App\Wrapper\Misc;

class LocationController {

  protected $app;

  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
    $this->response = new Format();
    $this->appMisc = new Misc();
    $fields = $this->app->location->fields;
    $this->fields = $fields;
  }

  public function province(){
    $provinces = $this->app->location->getProvince();
    $total = $this->app->location->getTotalProvince();
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Daftar Provinsi', $provinces);
  }
  public function getProvince( $provinceId = null){
    $provinces = $this->app->location->getLocation( "provinces", $provinceId );
    $total = $this->app->location->getTotalProvince();
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Data Provinsi', $provinces);
  }

  public function city( $provinceId ){
    $cities = $this->app->location->getCity( $provinceId );
    $total = $this->app->location->getTotalCity();
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Daftar Kota', $cities);
  }

  public function getCity( $cityId = null ){
    $provinces = $this->app->location->getLocation( "cities", $cityId );
    $total = $this->app->location->getTotalCity();
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Data Kota', $provinces);
  }

  public function district( $provinceId, $cityId ){
    // echo "province = " . $provinceId . " "; 
    // echo "cityid = " . $cityId . " "; 
    $districts = $this->app->location->getDistrict( $cityId );
    $total = $this->app->location->getTotalDistrict();
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Daftar Kecamatan', $districts);
  }

  public function getDistrict( $districtId = null ){
    $provinces = $this->app->location->getLocation( "districts", $districtId );
    $total = $this->app->location->getTotalDisctrict();
    $this->response->setTotal($total);
    $this->response->formatJson(200, 'Data Kota', $districts);
  }
}
