<?php

namespace App\Controller;

use App\Fractal\FractalTrait;
use App\Wrapper\Format;
use App\Wrapper\Misc;
use App\Transformer\ProductCommentTransformer;
use App\AmqpWrapper\WorkerSender;

class ProductCommentController
{
    use FractalTrait;

    protected $app;

    public function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
        $this->response = new Format();
        $this->appMisc = new Misc();
        $this->fields = $this->app->productComment->fields;
    }

    private function getProductOr404($productId)
    {
        $productId = intval($productId);
        
        if (!$productId) {
            return $this->response->formatJson(404, 'Produk tidak ditemukan');
        }

        $product = $this->app->product->get($productId);

        if (!$product) {
            return $this->response->formatJson(404, 'Produk tidak ditemukan');
        }

        return $product;
    }

    public function create($productId)
    {
        $sender = new WorkerSender();
        $loggedUser = $this->app->loggedUser;

        if (!$loggedUser) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $product = $this->getProductOr404($productId);

        $commentParams = $this->app->request->post();
        if ($errors = $this->appMisc->inputValidate($commentParams, $this->fields)) {
            return $this->response->formatJson(406, 'Validation Error', $errors);
        }

        $commentParams['people_id'] = $loggedUser['pid'];
        $commentParams['product_id'] = $product['id'];

        if ($commentId = $this->app->productComment->create($commentParams)) {
            $comment = $this->app->productComment->get($commentId);
            $comment = $this->getItem($comment, new ProductCommentTransformer());

            $productParams = $this->app->product->get($product['id']);
            // Set Target PID
            $target_pid = array();
            // Get Product Owner
            $owner = $this->app->product->get($product['id']);
            if($owner['pid'] != $loggedUser['pid']){
              $target_pid[] = "pid-" . $owner['pid'];
            }
            // Get Product Commenter
            $commenters = $this->app->productComment->getCommentsByProductId($product['id']);
            foreach($commenters['data'] as $commenter){
              if($commenter['people_id'] == $loggedUser['pid']){
                continue;
              }
              $target_pid[] = "pid-" . $commenter['people_id'];
            }

            $queueParams = array(
              'channel_name' => array(
                'rt' => "rt-". $loggedUser['neighbourhood_id'],
                'pid' => 'pid-'. $loggedUser['pid'],
                'target_pid' => array_unique($target_pid),
              ),
              'message' => "Komentar baru pada produk ".$productParams['name']." untuk dijual.",
              'data' => array(
                'pid' => $loggedUser['pid'],
                'activity_type' => 'productComment',
                'sound' => 'default',
                'productId' => $productId
              )
            );
            $queueParams['mode'] = $this->app->config('app.mode');
            $sender->execute('alertNotification', json_encode($queueParams));

            return $this->response->formatJson(200, 'Komentar berhasil ditambahkan', [$comment]);
        }

        return $this->response->formatJson(406, 'Komentar gagal ditambahkan');
    }

    public function detail($id)
    {
        if (!$this->app->loggedUser) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $comment = $this->app->productComment->get($id);

        if ($comment) {
            $comment = $this->getItem($comment, new ProductCommentTransformer(['product']));

            return $this->response->formatJson(200, 'Komentar ditemukan', [$comment]);
        }

        return $this->response->formatJson(404, 'Komentar tidak ditemukan');
    }

    public function delete($id)
    {
        $loggedUser = $this->app->loggedUser;
        if (!$loggedUser) {
            return $this->response->formatJson(406, 'Access denied');
        }

        $comment = $this->app->productComment->get($id);
        if (!$comment) {
            return $this->response->formatJson(404, 'Komentar tidak ditemukan');
        }
        if ($comment['people_id'] != $loggedUser['pid']) {
            return $this->response->formatJson(406, 'Access denied');
        }

        if ($this->app->productComment->delete($id)) {
            $comment = $this->getItem($comment, new ProductCommentTransformer(['product']));

            return $this->response->formatJson(200, 'Komentar berhasil dihapus', [$comment]);
        }

        return $this->response->formatJson(406, 'Komentar gagal dihapus');
    }

    public function index($productId)
    {
        $queryParams = $this->app->request->get();

        $product = $this->getProductOr404($productId);

        $result = $this->app->productComment->getCommentsByProductId($productId, $queryParams);
        $comments = $result['data'];
        $comments = $this->getCollection($comments, new ProductCommentTransformer());

        $totalCount = $result['totalCount'];
        $this->response->setTotal($totalCount);

        return $this->response->formatJson(200, 'Produk', $comments);
    }
}
