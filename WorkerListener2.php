<?php
chdir(dirname(__DIR__));
define('APP_PATH', getcwd(). '/'. basename(__DIR__));

require_once('vendor/autoload.php');

use App\AmqpWrapper\WorkerReceiver;

$worker = new WorkerReceiver();

$worker->listen('alertNotification');