<?php

use App\AmqpWrapper\WorkerSender;

$config = array(
  'mode' => 'live',
  'db' => 'api.kentongan',
);

if (file_exists(APP_PATH.'/bootstrap/config.php')) {
    $config = require APP_PATH.'/bootstrap/config.php';
}

$app = new \Slim\Slim([
  'mode' => 'development',
  'debug' => true,
  'app.key' => 'skyshidevKentongan',
  'app.files' => 'files',
  'app.approval_files' => 'files/approval_images',
  'app.imageExt' => array('jpg', 'jpeg', 'png'),
  'app.mode' => $config['mode'],
]);

$mode = $app->request->headers->get('mode');
if( !is_null($mode) ){
  $app->config('app.mode', $mode);
  if($mode == "demo"){
    $config = array(
      'mode' => 'demo',
      'db' => 'api.demo.kentongan',
    );
  } elseif ($mode == "live"){
    $config = array(
      'mode' => 'live',
      'db' => 'api.kentongan',
    );
  } else {
    $config = array(
      'mode' => 'dev',
      'db' => 'dev.api.kentongan',
      'parse' => array(
        'app_id' => 'ii6GRIV1a7F4zWVg3Nzvoamx0ioEySAvvuQG0xRL',
        'master_key' => 'arQ69xJQpoDY1j5OHYbnziUizkYxDD1KeSCZJZAB',
        'rest_key' => 'duNPqNULjHYiU0LTYP75en0UpfDfbjNp9LGPg8gz'
      ),
    );
  }
}

// Setting Image library to use imagic driver instead of gd
\Intervention\Image\ImageManagerStatic::configure(['driver' => 'imagick']);

// register singleton
$app->container->singleton('mysql', function ($config) use ($app, $config) {
    $mysqli = new mysqli('localhost', 'kentongan', 'kentongan123%', $config['db']);
    // $mysqli = new mysqli(
    //   $config['database']['host'],
    //   $config['database']['user'],
    //   $config['database']['password'],
    //   $config['database']['name']
    // );
    $db = new MysqliDb($mysqli);
    return $db;
});

// Autoload the Models
$app->container->singleton('user', function () use ($app) {
    return new \App\Model\User();
});
$app->container->singleton('warga', function () use ($app) {
    return new \App\Model\Warga();
});
$app->container->singleton('rt', function () use ($app) {
    return new \App\Model\Rt();
});
$app->container->singleton('finance', function () use ($app) {
    return new \App\Model\Finance();
});
$app->container->singleton('location', function () use ($app) {
    return new \App\Model\Location();
});
$app->container->singleton('report', function () use ($app) {
    return new \App\Model\Report();
});
$app->container->singleton('event', function () use ($app) {
    return new \App\Model\Event();
});
$app->container->singleton('alert', function () use ($app) {
    return new \App\Model\Alert();
});
$app->container->singleton('familytype', function () use ($app) {
    return new \App\Model\Familytype();
});
$app->container->singleton('documenttype', function () use ($app) {
  return new \App\Model\DocumentType();
});
$app->container->singleton('documentrequest', function () use ($app) {
  return new \App\Model\DocumentRequest();
});
// Activity Singleton
$app->container->singleton('activity', function () use ($app) {
  return new \App\Model\Activity();
});
$app->container->singleton('activityComment', function () use ($app) {
  return new \App\Model\ActivityComment();
});
// Notification Singleton
$app->container->singleton('notif', function () use ($app) {
  return new \App\Model\Notif();
});
$app->container->singleton('notifComment', function () use ($app) {
  return new \App\Model\NotifComment();
});
$app->container->singleton('information', function () use ($app) {
  return new \App\Model\Information();
});
// Marketplace 
$app->container->singleton('product', function() use ($app) {
  return new \App\Model\Product();
});
$app->container->singleton('productComment', function() use ($app) {
  return new \App\Model\ProductComment();
});
$app->container->singleton('productImage', function() use ($app) {
  return new \App\Model\ProductImage();
});
$app->container->singleton('productChat', function() use ($app) {
  return new \App\Model\ProductChat();
});

$app->container->singleton('format', function () use ($app) {
    return new \App\Wrapper\Response();
});

$app->container->singleton('utilities', function () use ($app) {
    return new \App\Model\Utilities();
});
$app->container->singleton('application', function () use ($app) {
    return new \App\Model\App();
});

// register middleware
$app->add(new \Slim\Middleware\ContentTypes());
$app->add(new \SlimJson\Middleware([
  'json.override_error' => true,
  'json.debug' => true,
]));
$app->add(new \App\Middleware\AuthMiddleware());
$app->add(new \App\Middleware\LogMiddleware());

$env = $app->environment();
if (strpos($env['PATH_INFO'], '/neighbourhood') === 0) {
    $env['PATH_INFO'] = str_replace('neighbourhood', 'rt', $env['PATH_INFO']);
}

$app->error(function (\Exception $e) use ($app) {
  echo $e;

  return;
  $sender = new WorkerSender();

  $message = [
    'level' => $app->log->getLevel(),
    'host' => $app->request->getHost(),
    'method' => $app->request->getMethod(),
    'content-type' => $app->request->getContentType(),
    'user_agent' => $app->request->getUserAgent(),
    'request_body' => $app->request->getBody(),
    'endpoint' => $_SERVER['REQUEST_URI'],
    'message' => $e->getMessage(),
    'stack_trace' => $e->getTraceAsString(),
  ];

  $queueParams = array(
    'app_path' => APP_PATH,
    'message' => $message,
  );

  $sender->execute('errorNotification', json_encode($queueParams));

  $app->log->error([
    'host' => $app->request->getHost(),
    'method' => $app->request->getMethod(),
    'content-type' => $app->request->getContentType(),
    'user_agent' => $app->request->getUserAgent(),
    'request_body' => $app->request->getBody(),
    'endpoint' => $_SERVER['REQUEST_URI'],
    'message' => $e->getMessage(),
    'stack_trace' => $e->getTraceAsString(),
  ]);
  /*$response = new \App\Wrapper\Format();
  $response->formatJson(500, $e->getMessage());*/
    $app->render(500, [
        'errorMessage' => 'Internal Server Error',
    ]);
});

// load routes
require APP_PATH.'/config/routes.php';

return $app;
